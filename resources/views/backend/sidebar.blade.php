@php
  $user = auth()->user();
@endphp

<aside class="main-sidebar sidebar-light-info text-sm elevation-2" style="font-family: Poppins;">
    <!-- Brand Logo -->
    <a href="index3.html" class="brand-link">
      <img src="{{asset('/backend/dist/img/AdminLTELogo.png')}}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
      <span class="brand-text font-weight-light"><b>Kogas</b></span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column nav-child-indent" data-widget="treeview" role="menu" data-accordion="false">
          {{-- <li class="nav-header">USERS</li> --}}

          <li class="nav-item">

            <a href="#" class="nav-link">
              <i class="nav-icon far fa-user"></i>
              <p>
                Users
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>

            <ul class="nav nav-treeview">

              <li class="nav-item">
                <a href="{{route('new.user')}}" class="nav-link">
                  <i class="fa fa-plus-circle nav-icon"></i>
                  <p>Add New</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{route('list.user')}}" class="nav-link">
                  <i class="fa fa-list-ul nav-icon"></i>
                  <p>List Users</p>
                </a>
              </li>

            </ul>
          </li>
          <li class="nav-item">
                <a href="{{route('gov.list')}}" class="nav-link">
                  <i class="fa fa-list-ul nav-icon"></i>
                  <p>Governor</p>
                </a>
              </li>
          <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="nav-icon fa fa-newspaper"></i>
              <p>
                News
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{route('news')}}" class="nav-link">
                  <i class="fa fa-plus-circle nav-icon"></i>
                  <p>Add New</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{route('list.news')}}" class="nav-link">
                  <i class="fa fa-list-ul nav-icon"></i>
                  <p>List News</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="nav-icon fa fa-road"></i>
              <p>
                Project
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{route('project')}}" class="nav-link">
                  <i class="fa fa-plus-circle nav-icon"></i>
                  <p>Add New</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{route('list.project')}}" class="nav-link">
                  <i class="fa fa-list-ul nav-icon"></i>
                  <p>List Projects</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="nav-icon fa fa-briefcase"></i>
              <p>
                Ministry
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{route('ministry')}}" class="nav-link">
                  <i class="fa fa-plus-circle nav-icon"></i>
                  <p>Add New</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{route("list.ministries")}}" class="nav-link">
                  <i class="fa fa-list-ul nav-icon"></i>
                  <p>List Ministries</p>
                </a>
              </li>
            </ul>
          </li>
            <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="nav-icon fa fa-briefcase"></i>
              <p>
                Official
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{route('official')}}" class="nav-link">
                  <i class="fa fa-plus-circle nav-icon"></i>
                  <p>Add New</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{route('list.officials')}}" class="nav-link">
                  <i class="fa fa-list-ul nav-icon"></i>
                  <p>List Officials</p>
                </a>
              </li>
            </ul>
            <li class="nav-item">
                <a href="{{route('chat_home')}}" class="nav-link">
                    <i class="fa fa-envelope-open nav-icon"></i>
                    <p>Messages</p>
                </a>
            </li>

        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>
