<style type="text/css">
  .img-circle-profile{
    border-radius: 50%;
    height: 25px;
    width: 25px;
  }
  .profile-detail{
    height: auto;
    width: auto;
    /*border: 1px solid grey;*/
  }
  .user-profile{
    border-radius: 5px;
    border: none;
    color: grey;
  }
  .dropdown-item {
    display: block;
    width: 100%;
    padding: .25rem 1rem;
    clear: both;
    font-weight: 400;
    color: grey;
    text-align: inherit;
    white-space: nowrap;
    background-color: transparent;
    border: 0;
}
</style>
<nav class="main-header navbar navbar-expand navbar-white navbar-light text-sm elevation-0" style="font-family: Poppins;">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
      {{-- <li class="nav-item d-none d-sm-inline-block">
        <a href="index3.html" class="nav-link">Home</a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="#" class="nav-link">Contact</a>
      </li> --}}
    </ul>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
      <li class="nav-item">
        <a class="nav-link" data-widget="fullscreen" href="#" role="button">
          <i class="fas fa-expand-arrows-alt"></i>
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" data-widget="control-sidebar" data-slide="true" id="profile" href="#" role="button">
          <img src="{{asset('/backend/dist/img/user2-160x160.jpg')}}" class="img-circle-profile elevation-2" alt="User Image" id="image">
          <span class="ml-1">{{\Auth::user()-> name}}</span>
          <div class="dropdown-menu dropdown-menu-right user-profile">
            <a href="#" id="myprofile" class="dropdown-item"><i class="fa fa-user mr-1"></i>My Profile</a>
            <a href="#" id="mysettings" class="dropdown-item"><i class="fa fa-cog mr-1"></i>Settings</a>
            <a href="{{route('logout')}}" id="mylogout" class="dropdown-item"><i class="fa fa-power-off mr-1"></i>Logout</a>
          </div>
        </a>
      </li>
    </ul>
  </nav>

