<footer class="main-footer text-sm" style="font-family: Poppins;">
	<strong>Copyright &copy; 2014-2020 <a href="https://adminlte.io">KOGAS</a>.</strong>
	All rights reserved.
	<div class="float-right d-none d-sm-inline-block">
		<b>Version</b> 3.1.0-rc
	</div>
</footer>
