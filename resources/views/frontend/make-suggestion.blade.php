@extends('frontend.template.master')

@section('content')

    <header>
        <!---top-row--->
    @include('frontend.template.toppart')
    <!---top-row close--->
        <!---Middle-header--->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="backend/plugins/fontawesome-free/css/all.min.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="dist/css/adminlte.min.css">
        <!-- summernote -->
        <link rel="stylesheet" href="backend/plugins/summernote/summernote-bs4.min.css">
        <!-- CodeMirror -->
        <link rel="stylesheet" href="backend/plugins/codemirror/codemirror.css">
        <link rel="stylesheet" href="/backend/plugins/codemirror/theme/monokai.css">
        <!-- SimpleMDE -->
        <link rel="stylesheet" href="/backend/plugins/simplemde/simplemde.min.css">
        <link rel="stylesheet" href="/backend/plugins/daterangepicker/daterangepicker.css">
        <!-- iCheck for checkboxes and radio inputs -->
        <link rel="stylesheet" href="/backend/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
        <!-- Bootstrap Color Picker -->
        <link rel="stylesheet" href="/backend/plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css">
        <!-- Tempusdominus Bootstrap 4 -->
        <link rel="stylesheet" href="/backend/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
        <!-- Select2 -->
        <link rel="stylesheet" href="/backend/plugins/select2/css/select2.min.css">
        <link rel="stylesheet" href="/backend/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
        <!-- Bootstrap4 Duallistbox -->
        <link rel="stylesheet" href="/backend/plugins/bootstrap4-duallistbox/bootstrap-duallistbox.min.css">
        <!-- BS Stepper -->
        <link rel="stylesheet" href="/backend/plugins/bs-stepper/css/bs-stepper.min.css">
        <!-- dropzonejs -->
        <link rel="stylesheet" href="/backend/plugins/dropzone/min/dropzone.min.css">
        <!---Menu-Nav-item--->
        <div id="mySidenav" class="sidenav header-menus">
            <ul>
                <li class="closebtn" onclick="closeNav()">&times;</li>
                <li><a href="/">Home</a></li>
                <li><a href="{{route("view_officials")}}">Governor Office</a></li>
                <li><a href="{{route("local_government")}}">Local Governments</a></li>
                <li><a href="{{route("ministries")}}">Ministries</a></li>
                {{--          <li><a href="">Ajaokuta Local Government Area</a></li>--}}
            </ul>
        </div>
        <!---Menu-Nav-item Close--->
        </div>
        <!---Middle-header close--->
    </header>
    <!--Header-close-->

    <!--marquee-close-start-->
    <div class="marquee-sec">
        <div class="simple-marquee-container">
            <div class="marquee">
                <ul class="marquee-content-items">
                    <li>KOGAS Portal</li>
                </ul>
            </div>
        </div>
    </div>
    <!--marquee-close-->

    <!-- start-governor-office-sec -->
    <div class="governor-office-banner">
        <div class="container-fluid">
            <div class="governor-logo-content">
                <img src="{{asset('/frontend/image/office-logo1.png')}}">
                <div class="office-content">
                    <h2><span>New Ideas / Suggestions</span></h2>
                    <p>Sed egestas, ante et vulputate volutpat, eros pede semper est, vitae luctus metus libero eu augue. Morbi purus libero, faucibus adipiscing, commodo quis, gravida id, est. Sed lectus. Praesent elementum hendrerit tortor.</p>
                </div>
                <img src="{{asset('/frontend/image/office-logo2.png')}}">
            </div>
        </div>
    </div>

    <!-- close-governor-office-sec -->

    <!-- start-standard-icons-sec -->

    @include('frontend.template.middlepart')
    <!-- close-standard-icons-sec -->

    <!-- start-contact-form -->

    <div class="contact-form padding-sec">
        <div class="container-fluid">
            <div class="formBox">
                <form>


                    <div class="row">
                        <label id="suggestion_status" style="background-color: green"></label>
                        <div class="col-sm-12 col-lg-6">
                            <div class="inputBox">
                                <label>State <span>*</span></label>
                                <select name="state" id="state" form="stateform">
                                    <option value="1">Kogi state</option>

                                </select>
                            </div>
                        </div>
                        <div class="col-sm-12 col-lg-6">
                            <div class="inputBox">
                                <label>Town/City <span>*</span></label>
                                <select name="city" id="city" form="cityform">
                                    <option value="volvo">Okene</option>

                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 col-lg-6">
                            <div class="inputBox">
                                <label>Choose Office <span>*</span></label>
                                <select name="office" id="office" form="officeform">
                                    <option value="1">Office of the Governor</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-12 col-lg-6">
                            <div class="inputBox">
                                <label>Select Ministry <span>*</span></label>
                                <select name="ministry" id="ministry" form="ministryform">
                                    @foreach($ministries as $ministry)
                                        <option value="{{$ministry->id}}">{{$ministry->name}}</option>
                                    @endforeach

                                </select>
                            </div>
                        </div>
                        <div class="col-sm-12 col-lg-6">
                            <div class="inputBox">
                                <label>Select Official <span>*</span></label>
                                <select name="governor" id="governor" form="governorform">
                                    @foreach($officials as $official)
                                        <option value="{{$official->id}}">{{$official->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="inputBox">
                                <label>Your Idea/Suggestion <span>*</span></label>
                                <textarea id="subject" name="subject" placeholder="Your message goes here"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="inputBox">
                                <label>Attachments (Optional)</label>
                                <div class="search-form-sec">
                                    <input type="file" id="id_file" name="attachment" multiple placeholder="Select a file  (JPG, PNG, PDF, DOCX, MP3, MP4, etc.)">
                                    <button type="button">Select File</button>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <p>
                                <input type="checkbox" class="input_class_checkbox">
                                <label for="test1">my submission complies with the <a href="#">terms and conditions</a>, governing this site.</label>
                            </p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <input type="submit" id="submit_suggestion" name="" class="button-two" value="Submit Suggestion">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script src="/frontend/js/bootstrap.bundle.min.js"></script>
    <script src="/frontend/js/jquery.min.js"></script>
    <script src="/frontend/js/marquee.js"></script>
    <script src="/frontend/js/custom-script.js"></script>
    </script>



    <script src="/backend/plugins/jquery/jquery.min.js"></script>
    <!-- Bootstrap 4 -->
    <script src="/backend/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- AdminLTE App -->
    <script src="/backend/dist/js/adminlte.min.js"></script>
    <!-- Summernote -->
    <script src="/backend/plugins/summernote/summernote-bs4.min.js"></script>
    <!-- CodeMirror -->
    <script src="/backend/plugins/codemirror/codemirror.js"></script>
    <script src="/backend/plugins/codemirror/mode/css/css.js"></script>
    <script src="/backend/plugins/codemirror/mode/xml/xml.js"></script>
    <script src="/backend/plugins/codemirror/mode/htmlmixed/htmlmixed.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="/backend/dist/js/demo.js"></script>
    <!-- Page specific script -->

    <!-- jQuery -->
    <script src="/backend/plugins/jquery/jquery.min.js"></script>
    <!-- Bootstrap 4 -->
    <script src="/backend/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- Select2 -->
    <script src="/backend/plugins/select2/js/select2.full.min.js"></script>
    <!-- Bootstrap4 Duallistbox -->
    <script src="/backend/plugins/bootstrap4-duallistbox/jquery.bootstrap-duallistbox.min.js"></script>
    <!-- InputMask -->
    <script src="/backend/plugins/moment/moment.min.js"></script>
    <script src="/backend/plugins/inputmask/jquery.inputmask.min.js"></script>
    <!-- date-range-picker -->
    <script src="/backend/plugins/daterangepicker/daterangepicker.js"></script>
    <!-- bootstrap color picker -->
    <script src="/backend/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js"></script>
    <!-- Tempusdominus Bootstrap 4 -->
    <script src="/backend/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
    <!-- Bootstrap Switch -->
    <script src="/backend/plugins/bootstrap-switch/js/bootstrap-switch.min.js"></script>
    <!-- BS-Stepper -->
    <script src="/backend/plugins/bs-stepper/js/bs-stepper.min.js"></script>
    <!-- dropzonejs -->
    <script src="/backend/plugins/dropzone/min/dropzone.min.js"></script>
    <!-- AdminLTE App -->
    <script src="/backend/dist/js/adminlte.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="/backend/dist/js/demo.js"></script>
    <!-- Page specific script -->
    <script>

        $("#submit_suggestion").click(function(event){
            event.preventDefault();
            let suggestion_message = $("#subject").val();
            let ministry = $("#ministry").val();
            let official = $("#governor").val();



            $.ajax({
                url: "/user/save_idea_suggestion",
                type:"POST",
                data:{
                    suggestion_message:suggestion_message,
                    ministry:ministry,
                    official:official

                },
                success:function(response){
                    console.log(response);
                    if(response) {

                        $('#suggestion_status').text(response['message']);

                        // window.location.href = 'user/dashboard';

                    }
                },
            });
        });
    </script>

@stop
