<footer class="padding-sec">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-3">
            <a href="#"><img src="{{asset('/frontend/image/footer-logo.png')}}"></a>
            <div class="footer-social">
              <a href="#"><img src="{{asset('/frontend/image/social-1.png')}}"></a>
              <a href="#"><img src="{{asset('/frontend/image/social-2.png')}}"></a>
              <a href="#"><img src="{{asset('/frontend/image/social-3.png')}}"></a>
            </div>
          </div>
          <div class="col-lg-5">
            <h3>Quick Links</h3>
            <ul class="footer-links footer-links-two">
              <li><a href="#">Governor’s Office</a></li>
              <li><a href="#">Directory</a></li>
              <li><a href="#">Projects</a></li>
              <li><a href="#">Ministries</a></li>
              <li><a href="#">Bereaus</a></li>
              <li><a href="#">Appointments</a></li>
              <li><a href="#">About KOGAS</a></li>
              <li><a href="#">Discussion Forum</a></li>
              <li><a href="#">2020 Budget</a></li>
              <li><a href="#">Report</a></li>
            </ul>
          </div>
          <div class="col-lg-2">
            <h3>Ministries</h3>
            <ul class="footer-links">
              <li><a href="#">Popular Questions</a></li>
              <li><a href="#">Popular MDAs</a></li>
              <li><a href="#">Popular Sector</a></li>
              <li><a href="#">Popular Questions</a></li>
              <li><a href="#">Popular MDAs</a></li>
            </ul>
          </div>
          <div class="col-lg-2">
            <h3>Contact Us</h3>
            <span>Call Now</span>
            <ul class="footer-links footer-links-three">
              <li><a href="#">+2348198765432</a></li>
              <li><a href="#">+2348198765432</a></li>
            </ul>
            <span>Email Address</span>
            <ul class="footer-links">
              <li><a href="#">info@kogas.com</a></li>
            </ul>

          </div>
        </div>
        <div class="row footer-end">
          <div class="col-lg-6">
            <p>COPYRIGHT 2020 ALL RIGHT RESERVED</p>
          </div>
          <div class="col-lg-6">
            <p>KOGI STATE GOVERNMENT</p>
          </div>
        </div>
      </div>
    </footer>
