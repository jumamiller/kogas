<div class="top-row">
      <div class="container-fluid">
        <div class="row">
          <!--col-1--->
          <div class="col-lg-4">
            <div class="logo-top">
              <img src="{{asset('/frontend/image/top-logo.png')}}"><span>KOGI STATE GOVERNMENT</span>
            </div>
          </div>
          <!--col-2--->
          <div class="col-lg-4">
            <div class="social-top text-center"> <a href="#"><i class="fab fa-facebook-f"></i></a>
              <a href="#"><i class="fab fa-twitter"></i></a>
              <a href="#"><i class="fab fa-instagram"></i></a>
            </div>
          </div>
          <!--col-3--->
            @if (Auth::check())
                <div class="col-lg-4">
                    <div class="top-right-bar text-end"> <a class="btntop " href="#"></a>
                        <a class="btntop" href="{{url('user/user_account_settings')}}">{{\Auth::user()-> name}}</a>
                    </div>
                </div>

            @else
                <div class="col-lg-4">
                    <div class="top-right-bar text-end"> <a class="btntop " href="{{url('/user-sign-in')}}">{{__("Sign In")}}</a>
                        <a class="btntop" href="{{url('/registerType')}}">{{__("Register")}}</a>
                    </div>
                </div>

            @endif

        </div>
      </div>
    </div>

    <div class="middle-header">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-6">
            <div class="logo">
              <img src="{{asset('/frontend/image/logo.png')}}">
            </div>
          </div>
          <div class="col-lg-6">
            <div class="text-end bar-middle-right">
              <ul>
                <li><a href="#"><i class="far fa-bell"></i></a>
                </li>
                <li><a href="#"><i class="fas fa-search"></i></a>
                </li>
                <li><a class="toggle-bar" onclick="openNav()" id="topbarToggle" href="#"><i class="fas fa-bars"></i></a>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div id="mySidenav" class="sidenav header-menus">
          <ul>
          <li class="closebtn" onclick="closeNav()">&times;</li>
              <li><a href="/">Home</a></li>
              <li><a href="{{route("view_officials")}}">Governor Office</a></li>
              <li><a href="{{route("local_government")}}">Local Governments</a></li>
              <li><a href="{{route("ministries")}}">Ministries</a></li>
              {{--          <li><a href="">Ajaokuta Local Government Area</a></li>--}}
          </ul>
    </div>

