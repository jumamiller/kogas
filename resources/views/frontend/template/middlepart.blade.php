<div class="standard-icons-sec">
  <div class="container-fluid">
    <div class="row">
      <div class="col">
        <a href="{{route('view_officials')}}">
        <div class="standard-icon-inner clr-1">
          <img src="{{asset('/frontend/image/buildings1.png')}}">
          <h5>Governor</h5>
        </div>
    	</a>
      </div>
      <div class="col">
      	<a href="{{route('local_government')}}">
        <div class="standard-icon-inner clr-2">
          <img src="{{asset('/frontend/image/buildings2.png')}}">
          <h5>Local Governments</h5>
        </div>
    	</a>
      </div>
      <div class="col">
      	<a href="{{route('ministries')}}">
        <div class="standard-icon-inner clr-3">
          <img src="{{asset('/frontend/image/buildings3.png')}}">
          <h5>Ministries</h5>
        </div>
    	</a>
      </div>
      <div class="col">
      	<a href="#">
        <div class="standard-icon-inner clr-4">
          <img src="{{asset('/frontend/image/buildings4.png')}}">
          <h5>Bureaus</h5>
        </div>
    	</a>
      </div>
      <div class="col">
      	<a href="{{route('projects')}}">
        <div class="standard-icon-inner clr-5">
          <img src="{{asset('/frontend/image/buildings5.png')}}">
          <h5>Projects</h5>
        </div>
    	</a>
      </div>
      <div class="col">
      	<a href="{{route('directories_home')}}">
        <div class="standard-icon-inner clr-6">
          <img src="{{asset('/frontend/image/buildings6.png')}}">
          <h5>Directory</h5>
        </div>
    	</a>
      </div>
      <div class="col">
      	<a href="{{route('appontment')}}">
        <div class="standard-icon-inner clr-7">
          <img src="{{asset('/frontend/image/buildings7.png')}}">
          <h5>Appointments</h5>
        </div>
    	</a>
      </div>
    </div>
  </div>
</div>
