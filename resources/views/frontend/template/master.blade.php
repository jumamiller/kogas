<!DOCTYPE html>
<html lang="">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Bootstrap CSS -->
  <link href="{{asset('/frontend/css/bootstrap.min.css')}}" rel="stylesheet">
  <link rel="stylesheet" href="{{asset('/frontend/css/marquee.css')}}" />
  <link href="{{asset('/frontend/fonts/popins.css')}}" rel="stylesheet">
  <link href="{{asset('/frontend/css/style.css')}}" rel="stylesheet">
  <link href="{{asset('/frontend/css/style-two.css')}}" rel="stylesheet">
  <link href="{{asset('/frontend/css/responsive.css')}}" rel="stylesheet">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css">
  <link rel="stylesheet" href="{{asset('/frontend/css/slick-theme.min.css')}}">
  <link rel="stylesheet" href="{{asset('/frontend/css/slick.css')}}">
  <script src="{{asset('/frontend/js/jquery.min.js')}}"></script>
  <title>{{__("Home")}}</title>
</head>
<body>

@yield('content')
</body>
@include('frontend.template.footer')
  <script src="{{asset('/frontend/js/bootstrap.bundle.min.js')}}"></script>
  <script src="{{asset('/frontend/js/custom-script.js')}}"></script>
  <script src="{{asset('/frontend/js/marquee.js')}}"></script>
  <script src="{{asset('/frontend/js/slick.min.js')}}"></script>
  <script>
    $(function (){
            $('.simple-marquee-container').SimpleMarquee({
                autostart: true,
                property: 'value',
                onComplete: null,
                duration: 30000,
                padding: 10,
                marquee_class: '.marquee',
                container_class: '.simple-marquee-container',
                sibling_class: 0,
                hover: true,
                velocity: 0.1,
                direction: 'left'
            });
          });
  </script>
 <script>
    $('.slick-carousel').slick({
    infinite: true,
    slidesToShow: 3,
    slidesToScroll: 1,
    arrows: true,
    dots: true,
  });
</script>
</html>
