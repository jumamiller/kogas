<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="/frontend/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="/frontend/css/marquee.css" />
    <link href="/frontend/fonts/popins.css" rel="stylesheet">
    <link href="/frontend/css/style.css" rel="stylesheet">
      \
    <link href="/frontend/css/style-two.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css">
    <link rel="stylesheet" href="/frontend/css/slick-theme.min.css">
    <link rel="stylesheet" href="/frontend/css/slick.css">
    <title>KOGAS</title>
  </head>
  <body>

   <!--Header-start-->
@include('frontend.template.toppart')
  <!--Header-close-->

<!--marquee-close-start-->
  <div class="marquee-sec">
    <div class="simple-marquee-container">
      <div class="marquee">
        <ul class="marquee-content-items">
          <li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros..</li>
          <li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros..</li>
        </ul>
      </div>
    </div>
  </div>
<!--marquee-close-->

<!-- start-governor-office-sec -->
<div class="governor-office-banner">
  <div class="container-fluid">
    <div class="governor-logo-content">
      <img src="/frontend/image/office-logo1.png">
      <div class="office-content">
        <h2><span>{{$ministry->name}}</span></h2>
        <p>The Kogi Open Governance and Accountability System actively encourages citizens participation in governance. Join thousands of Nigerians asking their leaders questiosn related to policy, projects, and more. Unhinged.</p>
      </div>
      <img src="/frontend/image/office-logo2.png">
    </div>
  </div>
</div>

<!-- close-governor-office-sec -->

<!-- start-standard-icons-sec -->

@include('frontend.template.middlepart')

<!-- close-standard-icons-sec -->


<!-- start-chairman-office-sec -->

<div class="chairman-office commisioner-office padding-sec">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-5">
        <div class="chairman-img">
          <img src="/frontend/image/drjames.jpg">
          <div class="chairman-info">
            <h4>Dr. Richard James</h4>
            <p>Chairman, Ajaokuta Local Goernment Area</p>
          </div>
        </div>
      </div>
      <div class="col-lg-7">
        <div class="chairman-content ministry-title">
          <h3 class="heading">{{$ministry->name}}</h3>
        <p>Sed egestas, ante et vulputate volutpat, eros pede semper est, vitae luctus metus libero eu augue. Morbi purus libero, faucibus adipiscing, commodo quis, gravida id, est. Sed lectus. Praesent elementum hendrerit tortor. Sed semper lorem at felis. Vestibulum volutpat, lacus a ultrices sagittis, mi neque euismod.</p>
        <p>Sed egestas, ante et vulputate volutpat, eros pede semper est, vitae luctus metus libero eu augue. Morbi purus libero, faucibus adipiscing, commodo quis, gravida id, est. Sed lectus.</p>
        <div class="governor-buttons">
          <!-- popup -->
        <a class="clr-1" href="#">Contact the Chairman</a>
        <a class="clr-4" href="#">Submit Report/Complaint</a>
        <a class="clr-3" href="#">Idea / Suggestions</a>
        <a class="clr-6" href="#">Book an Appointment</a>
    </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- close-chairman-office-sec -->

<!-- quick-upcoming-sec start -->
<div class="quick-upcoming-sec">
  <div class="row m-0">

    <div class="col-lg-4 quick-links-bx p-0">
      <div class="padding-quick">
      <h3>Quicklinks</h3>
      <ul>
        <li><a class="budget" href="#">Budget</a></li>
         <li><a class="projects" href="#">Projects</a></li>
          <li><a class="activities" href="#">Activities</a></li>
           <li><a class="news" href="#">News</a></li>
             <li><a class="notice-board" href="#">Notice Board</a></li>
      </ul>
    </div>
    </div>
    <div class="col-lg-8 p-0">
      <div class="upcoming-project-slide">
        <h2>Upcoming Projects</h2>

      <div id="projects-upcoming" class="slick-carousel upcoming-slide">
          @foreach($projects as $pr)
          <div class="upcoming-inner">
            <div class="upcoming-text">
                <img src="{{asset('/uploads/project_image/thumb_image/'.$pr-> thumb_image)}}">
           <a href="{{route("single_Project_view",$pr->id)}}"> <h5>{{$pr->project_name}}</h5></a>
          </div>
            <h6>{{$pr->project_detail}}</h6>
          </div>
          @endforeach


        </div>
        <div class="slide-projectbtn">
          <a href="{{route("single_ministry_projects",$ministry->id)}}" class="button project-all">VIEW ALL PROJECTS</a>
        </div>

      </div>

    </div>

  </div>
</div>
<!-- close-quick-upcoming-sec -->

<!-- start-executive-officials-sec -->
    <div class="upcoming-project-sec executive-officials padding-sec">
      <div class="container-fluid">
        <h3 class="heading">Other Officials</h3>
        <div id="other-official-two" class="slick-carousel upcoming-slide">
            @foreach($ministry->official_derectories as $official)
          <div class="upcoming-inner">
            <div class="up-sec">
            <img src="{{asset("uploads/profile_images/$official->official_image")}}">
            <h5>{{$official->name}}</h5>
            <h6>Vice - Chairman</h6>
          </div>
        </div>
            @endforeach

        </div>
        </div>
      </div>
    </div>

    <!-- close-executive-officials-sec -->



        <!-- start-footer -->
@include('frontend.template.footer')
    <!-- close-footer -->




  <!-- Option 1: Bootstrap Bundle with Popper -->
  <script src="/frontend/js/bootstrap.bundle.min.js"></script>
  <script src="/frontend/js/jquery.min.js"></script>
  <script src="/frontend/js/marquee.js"></script>
  <script src="/frontend/js/slick.min.js"></script>
  <script src="/frontend/js/custom-script.js"></script>


<script>
  $('#other-official-two').slick({
  dots: true,
  infinite: false,
  speed: 300,
  slidesToShow: 4,
  slidesToScroll: 1,
  responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 1,
        infinite: true,
        dots: true
      }
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
  ]
});
</script>
<script>
  $('#projects-upcoming').slick({
  dots: false,
  infinite: false,
  speed: 400,
  slidesToShow: 2,
  centerMode: false,
  variableWidth: true
});
</script>


  </body>
</html>
