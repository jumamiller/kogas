<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="/frontend/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="/frontend/css/marquee.css"/>
    <link href="/frontend/fonts/popins.css" rel="stylesheet">
    <link href="/frontend/css/style.css" rel="stylesheet">
    <link href="/frontend/css/style-two.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css">
    <title>Kogas</title>
</head>
<body>

<!--Header-start-->
@include('frontend.template.toppart')
<!--Header-close-->

<!--marquee-close-start-->
<div class="marquee-sec">
    <div class="simple-marquee-container">
        <div class="marquee">
            <ul class="marquee-content-items">
                <li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis
                    eros..
                </li>
                <li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis
                    eros..
                </li>
            </ul>
        </div>
    </div>
</div>
<!--marquee-close-->

<!-- start-governor-office-sec -->
<div class="governor-office-banner">
    <div class="container-fluid">
        <div class="governor-logo-content">
            <img src="/frontend/image/office-logo1.png">
            <div class="office-content">
                <h2><span>Submit Report</span></h2>
                <p>Sed egestas, ante et vulputate volutpat, eros pede semper est, vitae luctus metus libero eu augue.
                    Morbi purus libero, faucibus adipiscing, commodo quis, gravida id, est. Sed lectus. Praesent
                    elementum hendrerit tortor.</p>
            </div>
            <img src="/frontend/image/office-logo2.png">
        </div>
    </div>
</div>

<!-- close-governor-office-sec -->

<!-- start-standard-icons-sec -->

@include('frontend.template.middlepart')

<!-- close-standard-icons-sec -->

<!-- start-contact-form -->

<div class="contact-form padding-sec">
    <div class="container-fluid">
        <div class="formBox">
            <form>
                <div class="row">
                    <div class="col-sm-12 col-lg-6">
                        <div class="inputBox ">
                            <label>First Name <span>*</span></label>
                            <input type="text" id="first_name" name="First Name" placeholder="First Name">
                        </div>
                    </div>
                    <div class="col-sm-12 col-lg-6">
                        <div class="inputBox">
                            <label>Last Name <span>*</span></label>
                            <input type="text" id="last_name" name="Last Name" placeholder="Last Name">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 col-lg-6">
                        <div class="inputBox">
                            <label>Phone <span>*</span></label>
                            <input type="text" id="phone" name="phone" placeholder="Phone Number">
                        </div>
                    </div>
                    <div class="col-sm-12 col-lg-6">
                        <div class="inputBox">
                            <label>Email (Optional)</label>
                            <input type="email" id="email" name="email" placeholder="Email Address">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 col-lg-6">
                        <div class="inputBox">
                            <label>State <span>*</span></label>
                            <select name="state" id="state" form="stateform">
                                <option value="volvo">Kogi state</option>
                                <option value="saab">Kogi state</option>
                                <option value="opel">Kogi state</option>
                                <option value="audi">Kogi state</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-12 col-lg-6">
                        <div class="inputBox">
                            <label>Town/City <span>*</span></label>
                            <select name="city" id="city" form="cityform">
                                <option value="volvo">Okene</option>
                                <option value="saab">Okene</option>
                                <option value="opel">Okene</option>
                                <option value="audi">Okene</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 col-lg-6">
                        <div class="inputBox">
                            <label>Choose Office <span>*</span></label>
                            <select name="office" id="office" form="officeform">
                                <option value="volvo">Office of the Governor</option>
                                <option value="saab">Office of the Governor</option>
                                <option value="opel">Office of the Governor</option>
                                <option value="audi">Office of the Governor</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-12 col-lg-6">
                        <div class="inputBox">
                            <label>Select Official <span>*</span></label>
                            <select name="governor" id="governor" form="governorform">
                                <option value="1">Please choose an option</option>
                                <option value="2">Please choose an option</option>
                                <option value="3">Please choose an option</option>
                                <option value="4">Please choose an option</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="inputBox">
                            <label>Report Type <span>*</span></label>
                            <select name="governor" id="governor" form="governorform">
                                <option value="1">The Governor</option>
                                <option value="2">The Governor</option>
                                <option value="3">The Governor</option>
                                <option value="4">The Governor</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="inputBox">
                            <label>Your Report <span>*</span></label>
                            <textarea id="subject" name="subject" placeholder="Your message goes here"></textarea>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="inputBox">
                            <label>Attachments (Optional)</label>
                            <div class="search-form-sec">
                                <input type="text" id="fname" name="ezekiel"
                                       placeholder="Select a file  (JPG, PNG, PDF, DOCX, MP3, MP4, etc.)">
                                <button>Select File</button>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <p>
                            <input type="checkbox" class="input_class_checkbox">
                            <label for="test1">my submission complies with the <a href="#">terms and conditions</a>,
                                governing this site.</label>
                        </p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <input type="submit" id="submit_report" name="" class="button-two"
                               value="Submit Report/Complaint">
                    </div>
                </div>
            </form>
            <label id="submit_report_status" style="animation: backwards; background-color: green"></label>
        </div>
    </div>
</div>

<!-- close-contact-form -->

<!-- start-footer -->
@include('frontend.template.footer')
<!-- close-footer -->

<!-- Option 1: Bootstrap Bundle with Popper -->
<script src="/frontend/js/bootstrap.bundle.min.js"></script>
<script src="/frontend/js/jquery.min.js"></script>
<script src="/frontend/js/marquee.js"></script>
<script src="/frontend/js/custom-script.js"></script>

<script>

    $("#submit_report").click(function (event) {
        event.preventDefault();
        let report_message = $("#subject").val();
        let ministry = $("#office").val();
        let assigned_to = $("#governor").val();


        $.ajax({
            url: "{{route('submit_report')}}",
            type: "POST",
            data: {
                report_message: report_message,
                ministry: ministry,
                assigned_to: assigned_to

            },
            success: function (response) {
                console.log(response);
                if (response) {

                    $('#submit_report_status').text(response['message']);

                }
            },
        });
    });
</script>

</body>
</html>
