@extends('frontend.template.master')

@section('content')

    <!---top-row--->
    @include('frontend.template.toppart')

    <!--Header-close-->

    <!--marquee-close-start-->
    <div class="marquee-sec">
        <div class="simple-marquee-container">
            <div class="marquee">
                <ul class="marquee-content-items">
                    <li>KOGAS Web Portal</li>
                    <li>KOGAS Web Portal</li>
                </ul>
            </div>
        </div>
    </div>
    <!--marquee-close-->

    <!-- start-governor-office-sec -->
    <div class="governor-office-banner">
        <div class="container-fluid">
            <div class="governor-logo-content">
                <img src="{{asset('/frontend/image/office-logo1')}}.png">
                <div class="office-content">
                    <h2>{{$ministry->name}} Projects</h2>
                    <p>Driven by the need to harness the huge natural endowments of our state, we are committed to efficient management of our resources to the advantage for our people. Projects of the administration in different stages are hereby showcased.  </p>
                </div>
                <img src="{{asset('/frontend/image/office-logo2.png')}}">
            </div>
        </div>
    </div>

    <!-- close-governor-office-sec -->


    <!-- start-standard-icons-sec -->

    @include('frontend.template.middlepart')

    <!-- close-standard-icons-sec -->

    <!-- projects-tab-sec -->
    <div class="projects-tab-sec padding-sec">
        <div class="container-fluid">

            <ul class="nav nav-pills mb-3  justify-content-center d-flex" id="pills-tab" role="tablist">
                <li class="nav-item" role="presentation">
                    <a class="nav-link active" id="pills-upcoming-tab" data-bs-toggle="pill" href="#pills-upcoming" role="tab" aria-controls="pills-upcoming" aria-selected="true">Upcoming</a>
                </li>
                <li class="nav-item" role="presentation">
                    <a class="nav-link" id="pills-progress-tab" data-bs-toggle="pill" href="#pills-progress" role="tab" aria-controls="pills-progress" aria-selected="false">In Progress</a>
                </li>
                <li class="nav-item" role="presentation">
                    <a class="nav-link" id="pills-completed-tab" data-bs-toggle="pill" href="#pills-completed" role="tab" aria-controls="pills-completed" aria-selected="false">Completed</a>
                </li>
            </ul>

            <div class="filter-showing-box">
                <div class="showing-box">

                </div>
                <div class="fillter-box">
                    <label>Fillter:</label>
                    <select>
                        <option>All</option>
                        <option>Upcoming</option>`
                        <option>In Progress</option>
                        <option>Completed</option>
                    </select>
                </div>
            </div>

            <div class="tab-content" id="pills-tabContent">
                <div class="tab-pane fade show active" id="pills-upcoming" role="tabpanel" aria-labelledby="pills-upcoming-tab">
                    <div class="tab-content-sec">
                        <div class="row">
                            <!---1--->
                            @foreach($upcoming as $project)
                                <div class="col-lg-4 col-md-6">
                                    <div class="col-showingbx">
                                        <div class="image-text">
                                            <img  src="{{asset('uploads/project_image/thumb_image/'.$project-> thumb_image)}}">
                                            <h4>{{$project-> project_name}}</h4>
                                            <a href="{{route('single_Project_view',$project->id)}}">{{$project->project_detail}}</a>
                                        </div>
                                    </div>
                                </div>
                            @endforeach

                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="pills-progress" role="tabpanel" aria-labelledby="pills-progress-tab">
                    <div class="tab-content-sec">
                        <div class="row">
                            <!---1--->
                            @foreach($in_progress as $project)
                                <div class="col-lg-4 col-md-6">
                                    <div class="col-showingbx">
                                        <div class="image-text">
                                            <img src="{{asset('uploads/project_image/thumb_image/'.$project-> thumb_image)}}">
                                            <h4>{{$project-> project_name}}</h4>
                                            <a href="{{route('single_Project_view',$project->id)}}">{{$project->project_detail}}</a>
                                        </div>
                                    </div>
                                </div>
                            @endforeach


                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="pills-completed" role="tabpanel" aria-labelledby="pills-completed-tab">
                    <div class="tab-content-sec">
                        <div class="row">
                            <!---1--->
                            @foreach($completed as $project)
                                <div class="col-lg-4 col-md-6">
                                    <div class="col-showingbx">
                                        <div class="image-text">
                                            <img src="{{asset('uploads/project_image/thumb_image/'.$project-> thumb_image)}}">
                                            <h4>{{$project-> project_name}}</h4>
{{--                                            {{$project->images}}--}}
                                            <a href="{{route('single_Project_view',$project->id)}}">{{$project->project_detail}}</a>
                                        </div>
                                    </div>
                                </div>
                        @endforeach
                        <!---2--->


                        </div>
                    </div>

                </div>
            </div>

            <!---Pagination--->

            <div class="pagination-sec">
                <div class="row">
                    <div class="col-lg-6">
                        {{--            <p>page 1 of 3 </p>--}}
                    </div>
                    <div class="col-lg-6 text-end">
                        {{--<ul class="page-num">
                          <li><a class="active" href="#">1</a></li>
                            <li><a class="" href="#">2</a></li>
                              <li><a class="" href="#">3</a></li>
                        </ul>--}}
                    </div>
                </div>
            </div>

            <!---Pagination close--->

        </div>
    </div>

@stop
