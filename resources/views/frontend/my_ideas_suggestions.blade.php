@include('frontend.template.main-menu')

<!-- left column -->
<div class="container">
    <div class="row justify-content-end">
        <div class="col-md-10">
            <div class="card">
                @if($my_suggestions)
                    <table id="appointments_table" class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            <th> Title</th>
                            <th> Body</th>
                            <th> Ministry</th>
                            <th> Citizen</th>
                            <th> Assigned To</th>
                            <th> Status</th>
                            <th> Date</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($my_suggestions as $suggestion)
                            <tr>
                                <td> {{$suggestion->suggestion_title}} </td>
                                <td> {{$suggestion->suggestion_body}} </td>
                                <td> {{$suggestion->ministry_id}} </td>
                                <td> {{$suggestion->citizen_id}} </td>
                                <td> {{$suggestion->assigned_to}} </td>
                                <td> {{$suggestion->status}} </td>
                                <td> {{$suggestion->created_at}} </td>
                            </tr>
                        @endforeach
                        @endif
                        </tbody>
                    </table>
            </div>
        </div>
    </div>
</div>
