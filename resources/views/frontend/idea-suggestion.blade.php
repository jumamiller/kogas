<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="/css/marquee.css" />
    <link href="/fonts/popins.css" rel="stylesheet">
    <link href="/css/style.css" rel="stylesheet">
    <link href="/css/style-two.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css">
    <title>Kogas</title>
</head>
<body>

<!--Header-start-->
<header>
    <!---top-row--->
    <div class="top-row">
        <div class="container-fluid">
            <div class="row">
                <!--col-1--->
                <div class="col-lg-4">
                    <div class="logo-top">
                        <img src="/frontend/image/top-logo.png"><span>KOGI STATE GOVERNMENT</span>
                    </div>
                </div>
                <!--col-2--->
                <div class="col-lg-4">
                    <div class="social-top text-center"> <a href="#"><i class="fab fa-facebook-f"></i></a>
                        <a href="#"><i class="fab fa-twitter"></i></a>
                        <a href="#"><i class="fab fa-instagram"></i></a>
                    </div>
                </div>
                <!--col-3--->
                <div class="col-lg-4">
                    <div class="top-right-bar text-end"> <a class="btntop " href="#">login</a>
                        <a class="btntop" href="#">Register</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!---top-row close--->
    <!---Middle-header--->
    <div class="middle-header">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-6">
                    <div class="logo">
                        <a href="index.html"><img src="/frontend/image/logo.png"></a>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="text-end bar-middle-right">
                        <ul>
                            <li><a href="#"><i class="far fa-bell"></i></a>
                            </li>
                            <li><a href="#"><i class="fas fa-search"></i></a>
                            </li>
                            <li><a class="toggle-bar" onclick="openNav()" href="javascript:void(0)"><i class="fas fa-bars"></i></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!---Menu-Nav-item--->
        <div id="mySidenav" class="sidenav header-menus">
            <ul>
                <li class="closebtn" onclick="closeNav()">&times;</li>
                <li><a href="index.html">Home</a></li>
                <li><a href="governor_office.html">Governor Office</a></li>
                <li><a href="local-government.html">Local Governments</a></li>
                <li><a href="ministries.html">Ministries</a></li>
                <li><a href="single-local-government.html">Ajaokuta Local Government Area</a></li>
            </ul>
        </div>
        <!---Menu-Nav-item Close--->
    </div>
    <!---Middle-header close--->
</header>
<!--Header-close-->

<!--marquee-close-start-->
<div class="marquee-sec">
    <div class="simple-marquee-container">
        <div class="marquee">
            <ul class="marquee-content-items">
                <li>Kogas Web Portal</li>
                <li>Kogas Web Portal</li>
            </ul>
        </div>
    </div>
</div>
<!--marquee-close-->

<!-- start-governor-office-sec -->
<div class="governor-office-banner">
    <div class="container-fluid">
        <div class="governor-logo-content">
            <img src="/frontend/image/office-logo1.png">
            <div class="office-content">
                <h2><span>Ideas/Suggestions</span></h2>
                <p>Sed egestas, ante et vulputate volutpat, eros pede semper est, vitae luctus metus libero eu augue. Morbi purus libero, faucibus adipiscing, commodo quis, gravida id, est. Sed lectus. Praesent elementum hendrerit tortor.</p>
            </div>
            <img src="/frontend/image/office-logo2.png">
        </div>
    </div>
</div>

<!-- close-governor-office-sec -->

<!-- start-standard-icons-sec -->

<div class="standard-icons-sec">
    <div class="container-fluid">
        <div class="row">
            <div class="col">
                <div class="standard-icon-inner clr-1">
                    <img src="/frontend/image/buildings1.png">
                    <h5>Governor’s Office</h5>
                </div>
            </div>
            <div class="col">
                <div class="standard-icon-inner clr-2">
                    <img src="/frontend/image/buildings2.png">
                    <h5>Local Governments</h5>
                </div>
            </div>
            <div class="col">
                <div class="standard-icon-inner clr-3">
                    <img src="/frontend/image/buildings3.png">
                    <h5>Ministries</h5>
                </div>
            </div>
            <div class="col">
                <div class="standard-icon-inner clr-4">
                    <img src="/frontend/image/buildings4.png">
                    <h5>Bureaus</h5>
                </div>
            </div>
            <div class="col">
                <div class="standard-icon-inner clr-5">
                    <img src="/frontend/image/buildings5.png">
                    <h5>Projects</h5>
                </div>
            </div>
            <div class="col">
                <div class="standard-icon-inner clr-6">
                    <img src="/frontend/image/buildings6.png">
                    <h5>Directory</h5>
                </div>
            </div>
            <div class="col">
                <div class="standard-icon-inner clr-7">
                    <img src="/frontend/image/buildings7.png">
                    <h5>Appointments</h5>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- close-standard-icons-sec -->

<!-- start-contact-form -->

<div class="contact-form padding-sec">
    <div class="container-fluid">
        <div class="formBox">
            <form>


                <div class="row">
                    <label id="suggestion_status" style="background-color: green"></label>
                    <div class="col-sm-12 col-lg-6">
                        <div class="inputBox">
                            <label>State <span>*</span></label>
                            <select name="state" id="state" form="stateform">
                                <option value="volvo">Kogi state</option>

                            </select>
                        </div>
                    </div>
                    <div class="col-sm-12 col-lg-6">
                        <div class="inputBox">
                            <label>Town/City <span>*</span></label>
                            <select name="city" id="city" form="cityform">
                                <option value="volvo">Okene</option>

                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 col-lg-6">
                        <div class="inputBox">
                            <label>Choose Office <span>*</span></label>
                            <select name="office" id="office" form="officeform">
                                <option value="volvo">Office of the Governor</option>

                            </select>
                        </div>
                    </div>
                    <div class="col-sm-12 col-lg-6">
                        <div class="inputBox">
                            <label>Select Official <span>*</span></label>
                            <select name="governor" id="governor" form="governorform">
                                <option value="governor">The Governor</option>

                            </select>
                        </div>
                    </div>
                    <div class="col-sm-12 col-lg-6">
                        <div class="inputBox">
                            <label>Select Ministry <span>*</span></label>
                            <select name="ministry" id="ministry" form="ministryform">
                                <option value="1">Ministry of Water</option>
                                <option value="2">Ministry of Energy</option>

                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="inputBox">
                            <label>Your Idea/Suggestion <span>*</span></label>
                            <textarea id="subject" name="subject" placeholder="Your message goes here"></textarea>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="inputBox">
                            <label>Attachments (Optional)</label>
                            <div class="search-form-sec">
                                <input type="text" id="fname" name="ezekiel" placeholder="Select a file  (JPG, PNG, PDF, DOCX, MP3, MP4, etc.)">
                                <button>Select File</button>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <p>
                            <input type="checkbox" class="input_class_checkbox">
                            <label for="test1">my submission complies with the <a href="#">terms and conditions</a>, governing this site.</label>
                        </p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <input type="submit" id="submit_suggestion" name="" class="button-two" value="Submit Suggestion">
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- close-contact-form -->





<!-- start-footer -->
<footer class="padding-sec">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-3">
                <a href="#"><img src="/frontend/image/footer-logo.png"></a>
                <div class="footer-social">
                    <a href="#"><img src="/frontend/image/social-1.png"></a>
                    <a href="#"><img src="/frontend/image/social-2.png"></a>
                    <a href="#"><img src="/frontend/image/social-3.png"></a>
                </div>
            </div>
            <div class="col-lg-5">
                <h3>Quick Links</h3>
                <ul class="footer-links footer-links-two">
                    <li><a href="#">Governor’s Office</a></li>
                    <li><a href="#">Directory</a></li>
                    <li><a href="#">Projects</a></li>
                    <li><a href="#">Ministries</a></li>
                    <li><a href="#">Bereaus</a></li>
                    <li><a href="#">Appointments</a></li>
                    <li><a href="#">About KOGAS</a></li>
                    <li><a href="#">Discussion Forum</a></li>
                    <li><a href="#">2020 Budget</a></li>
                    <li><a href="#">Report</a></li>
                </ul>
            </div>
            <div class="col-lg-2">
                <h3>Ministries</h3>
                <ul class="footer-links">
                    <li><a href="#">Popular Questions</a></li>
                    <li><a href="#">Popular MDAs</a></li>
                    <li><a href="#">Popular Sector</a></li>
                    <li><a href="#">Popular Questions</a></li>
                    <li><a href="#">Popular MDAs</a></li>
                </ul>
            </div>
            <div class="col-lg-2">
                <h3>Contact Us</h3>
                <span>Call Now</span>
                <ul class="footer-links footer-links-three">
                    <li><a href="#">+2348198765432</a></li>
                    <li><a href="#">+2348198765432</a></li>
                </ul>
                <span>Email Address</span>
                <ul class="footer-links">
                    <li><a href="#">info@kogas.com</a></li>
                </ul>

            </div>
        </div>
        <div class="row footer-end">
            <div class="col-lg-6">
                <p>COPYRIGHT 2020 ALL RIGHT RESERVED</p>
            </div>
            <div class="col-lg-6">
                <p>KOGI STATE GOVERNMENT</p>
            </div>
        </div>
    </div>
</footer>
<!-- close-footer -->




<!-- Option 1: Bootstrap Bundle with Popper -->
<script src="/js/bootstrap.bundle.min.js"></script>
<script src="/js/jquery.min.js"></script>
<script src="/js/marquee.js"></script>
<script src="/js/custom-script.js"></script>

<script>

    $("#submit_suggestion").click(function(event){
        event.preventDefault();
        let suggestion_message = $("#subject").val();
        let ministry = $("#ministry").val();
        let official = $("#governor").val();



        $.ajax({
            url: "/user/save_idea_suggestion",
            type:"POST",
            data:{
                suggestion_message:suggestion_message,
                ministry:ministry,
                official:official

            },
            success:function(response){
                console.log(response);
                if(response) {

                    $('#suggestion_status').text(response['message']);

                   // window.location.href = 'user/dashboard';

                }
            },
        });
    });
</script>
</body>
</html>
