<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="/frontend/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="/frontend/css/marquee.css"/>
    <link href="/frontend/fonts/popins.css" rel="stylesheet">
    <link href="/frontend/css/style.css" rel="stylesheet">
    <link href="/frontend/css/style-two.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css">
    <title>Kogas</title>
</head>
<body>

<!--Header-start-->
@include('frontend.template.toppart')
<!--Header-close-->

<!--marquee-close-start-->
<div class="marquee-sec">
    <div class="simple-marquee-container">
        <div class="marquee">
            <ul class="marquee-content-items">
                <li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis
                    eros..
                </li>
                <li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis
                    eros..
                </li>
            </ul>
        </div>
    </div>
</div>
<!--marquee-close-->

<!-- start-governor-office-sec -->
<div class="governor-office-banner">
    <div class="container-fluid">
        <div class="governor-logo-content">
            <img src="/frontend/image/office-logo1.png">
            <div class="office-content">
                <h2><span>Ministries</span></h2>
                <p>The Kogi Open Governance and Accountability System actively encourages citizens participation in
                    governance. Join thousands of Nigerians asking their leaders questiosn related to policy, projects,
                    and more. Unhinged.</p>
            </div>
            <img src="/frontend/image/office-logo2.png">
        </div>
    </div>
</div>

<!-- close-governor-office-sec -->

<!-- start-standard-icons-sec -->

@include('frontend.template.middlepart')

<!-- close-standard-icons-sec -->

<!-- start-ministries-sec -->

<div class="ministries-sec padding-sec">
    <div class="container-fluid">
        <div class="row ministry-agriculture">
            @foreach($ministries as $ministry)
                <div class="col-lg-4">
                    <div class="ministry-inner">
                        <img id="ministry_name" onclick="viewSingleMinistry(event)" src="{{asset('/uploads/ministry_image/'.$ministry->ministry_image)}}">
                        <label id="ministry_id">{{$ministry->id}}</label>
{{--                        <label onclick="viewSingleMinistry(event)">{{$ministry-> name}}</label>--}}
                        <a href="{{route("single_ministry_home",$ministry->id)}}">{{$ministry-> name}}</a>

                    </div>
                </div>
            @endforeach

            {{-- <div class="col-lg-4">
                 <div class="ministry-inner">
                     <img src="/frontend/image/ministries-2.jpg">
                     <a href="#">Ministry of Justice</a>
                 </div>
             </div>
             <div class="col-lg-4">
                 <div class="ministry-inner">
                     <img src="/frontend/image/ministries-3.jpg">
                     <a href="#">Ministry of Finance, Budget & Economic Planning</a>
                 </div>
             </div>
         </div>
         <div class="row ministry-agriculture">
             <div class="col-lg-4">
                 <div class="ministry-inner">
                     <img src="/frontend/image/ministries-1.jpg">
                     <a href="#">Ministry of Agriculture</a>
                 </div>
             </div>
             <div class="col-lg-4">
                 <div class="ministry-inner">
                     <img src="/frontend/image/ministries-2.jpg">
                     <a href="#">Ministry of Justice</a>
                 </div>
             </div>
             <div class="col-lg-4">
                 <div class="ministry-inner">
                     <img src="/frontend/mage/ministries-3.jpg">
                     <a href="#">Ministry of Finance, Budget & Economic Planning</a>
                 </div>
             </div>
         </div>
         <div class="row ministry-agriculture">
             <div class="col-lg-4">
                 <div class="ministry-inner">
                     <img src="/frontend/image/ministries-1.jpg">
                     <a href="#">Ministry of Agriculture</a>
                 </div>
             </div>
             <div class="col-lg-4">
                 <div class="ministry-inner">
                     <img src="/frontend/image/ministries-2.jpg">
                     <a href="#">Ministry of Justice</a>
                 </div>
             </div>
             <div class="col-lg-4">
                 <div class="ministry-inner">
                     <img src="/frontend/image/ministries-3.jpg">
                     <a href="#">Ministry of Finance, Budget & Economic Planning</a>
                 </div>
             </div>
         </div>
         <div class="row ministry-agriculture">
             <div class="col-lg-4">
                 <div class="ministry-inner">
                     <img src="/frontend/image/ministries-1.jpg">
                     <a href="#">Ministry of Agriculture</a>
                 </div>
             </div>
             <div class="col-lg-4">
                 <div class="ministry-inner">
                     <img src="/frontend/image/ministries-2.jpg">
                     <a href="#">Ministry of Justice</a>
                 </div>
             </div>
             <div class="col-lg-4">
                 <div class="ministry-inner">
                     <img src="/frontend/image/ministries-3.jpg">
                     <a href="#">Ministry of Finance, Budget & Economic Planning</a>
                 </div>
             </div>
         </div>
         <div class="row ministry-agriculture">
             <div class="col-lg-4">
                 <div class="ministry-inner">
                     <img src="/frontend/image/ministries-1.jpg">
                     <a href="#">Ministry of Agriculture</a>
                 </div>
             </div>
             <div class="col-lg-4">
                 <div class="ministry-inner">
                     <img src="/frontend/image/ministries-2.jpg">
                     <a href="#">Ministry of Justice</a>
                 </div>
             </div>--}}
        </div>
    </div>
</div>

<!-- close-ministries-sec -->


<!-- start-footer -->
<footer class="padding-sec">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-3">
                <a href="#"><img src="/frontend/image/footer-logo.png"></a>
                <div class="footer-social">
                    <a href="#"><img src="/frontend/image/social-1.png"></a>
                    <a href="#"><img src="/frontend/image/social-2.png"></a>
                    <a href="#"><img src="/frontend/image/social-3.png"></a>
                </div>
            </div>
            <div class="col-lg-5">
                <h3>Quick Links</h3>
                <ul class="footer-links footer-links-two">
                    <li><a href="#">Governor’s Office</a></li>
                    <li><a href="#">Directory</a></li>
                    <li><a href="#">Projects</a></li>
                    <li><a href="#">Ministries</a></li>
                    <li><a href="#">Bereaus</a></li>
                    <li><a href="#">Appointments</a></li>
                    <li><a href="#">About KOGAS</a></li>
                    <li><a href="#">Discussion Forum</a></li>
                    <li><a href="#">2020 Budget</a></li>
                    <li><a href="#">Report</a></li>
                </ul>
            </div>
            <div class="col-lg-2">
                <h3>Ministries</h3>
                <ul class="footer-links">
                    <li><a href="#">Popular Questions</a></li>
                    <li><a href="#">Popular MDAs</a></li>
                    <li><a href="#">Popular Sector</a></li>
                    <li><a href="#">Popular Questions</a></li>
                    <li><a href="#">Popular MDAs</a></li>
                </ul>
            </div>
            <div class="col-lg-2">
                <h3>Contact Us</h3>
                <span>Call Now</span>
                <ul class="footer-links footer-links-three">
                    <li><a href="#">+2348198765432</a></li>
                    <li><a href="#">+2348198765432</a></li>
                </ul>
                <span>Email Address</span>
                <ul class="footer-links">
                    <li><a href="#">info@kogas.com</a></li>
                </ul>

            </div>
        </div>
        <div class="row footer-end">
            <div class="col-lg-6">
                <p>COPYRIGHT 2020 ALL RIGHT RESERVED</p>
            </div>
            <div class="col-lg-6">
                <p>KOGI STATE GOVERNMENT</p>
            </div>
        </div>
    </div>
</footer>
<!-- close-footer -->


<!-- Option 1: Bootstrap Bundle with Popper -->
<script src="/frontend/js/bootstrap.bundle.min.js"></script>
<script src="/frontend/js/jquery.min.js"></script>
<script src="/frontend/js/marquee.js"></script>
<script src="/frontend/js/custom-script.js"></script>
<script>

    function  viewSingleMinistry(event) {
        $("#ministry_name").click(function (event) {
            event.preventDefault();
            var id = document.getElementById("ministry_id").textContent;
            if(id == null){
                id = 1;
            }
            console.log('ministry_id ' + id.toString());
            $.ajax({
                url: '/user/single_ministry_home'+'/'+id,
                type: "GET",

                success: function (response) {
                    console.log(response);

                        window.location.replace('/user/single_ministry_home/'+id);


                },
            });
        });
    }
</script>
</body>
</html>
