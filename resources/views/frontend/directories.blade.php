<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="/frontend/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="/frontend/css/marquee.css" />
    <link href="/frontend/fonts/popins.css" rel="stylesheet">
    <link href="/frontend/css/style.css" rel="stylesheet">
    <link href="/frontend/css/style-two.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css">
    <title>Kogas</title>
</head>
<body>

<!--Header-start-->
@include('frontend.template.toppart')
<!--Header-close-->

<!--marquee-close-start-->
<div class="marquee-sec">
    <div class="simple-marquee-container">
        <div class="marquee">
            <ul class="marquee-content-items">
                <li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros..</li>
                <li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros..</li>
            </ul>
        </div>
    </div>
</div>
<!--marquee-close-->

<!-- start-governor-office-sec -->
<div class="governor-office-banner">
    <div class="container-fluid">
        <div class="governor-logo-content">
            <img src="/frontend/image/office-logo1.png">
            <div class="office-content">
                <h2>Directory</h2>
                <p>Sed egestas, ante et vulputate volutpat, eros pede semper est, vitae luctus metus libero eu augue. Morbi purus libero, faucibus adipiscing, commodo quis, gravida id, est. Sed lectus. Praesent elementum hendrerit tortor.</p>
            </div>
            <img src="/frontend/image/office-logo2.png">
        </div>
    </div>
</div>

<!-- close-governor-office-sec -->


<!-- start-standard-icons-sec -->

@include('frontend.template.middlepart')

<!-- close-standard-icons-sec -->

<!-- projects-tab-sec -->
<div class="projects-tab-sec padding-sec">
    <div class="container-fluid">

        <div class="filter-showing-box">
            <div class="showing-box showing-box-two">
                <form>
                    <input type="text" placeholder="Search for a name or office" name="search">
                    <button type="submit"><i class="fa fa-search"></i></button>
                </form>
            </div>
            <div class="fillter-box">
                <label>Fillter:</label>
                <select>
                    <option>All</option>
                    <option>Upcoming</option>
                    <option>In Progress</option>
                    <option>Completed</option>
                </select>
            </div>
        </div>

        <!-- start-directory-table -->

        <div class="directory-table">
            <table>
                <tr>
                    <th>S/N</th>
                    <th>Name</th>
                    <th>Office</th>
                    <th>Email</th>
                    <th>Phone</th>
                    <th>Socials</th>
                </tr>

                @foreach($officials as $official)

                <tr>
                    <td>{{$official->user_id}}</td>
                    <td>{{$official->name}}</td>
                    <td>{{$official->office}}</td>
                    <td>{{$official->email}}</td>
                    <td>{{$official->mobile}}</td>
                    <td>
                        <a href="{{$official->facebook}}"><i class="fab fa-facebook-f"></i></a>
                        <a href="{{$official->twitter}}"><i class="fab fa-twitter"></i></a>
                        <a href="{{$official->instagram}}"><i class="fab fa-instagram"></i></a>
                    </td>
                </tr>
                @endforeach

            </table>
        </div>

        <!-- close-directory-table -->

        <!---Pagination--->
        <div class="pagination-sec">
            <div class="row">
                <div class="col-lg-6">
                    <p>page 1 of 5 </p>
                </div>
                <div class="col-lg-6 text-end">
                    <ul class="page-num">
                        <li><a class="active" href="#">1</a></li>
                        <li><a class="" href="#">2</a></li>
                        <li><a class="" href="#">3</a></li>
                        <li><a class="" href="#">4</a></li>
                        <li><a class="" href="#">5</a></li>
                    </ul>
                </div>
            </div>
        </div>

        <!---Pagination close--->

    </div>
</div>
<!--projects-tab-sec close-->

<!-- start-footer -->
<footer class="padding-sec">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-3">
                <a href="#"><img src="/frontend/image/footer-logo.png"></a>
                <div class="footer-social">
                    <a href="#"><img src="/frontend/image/social-1.png"></a>
                    <a href="#"><img src="/frontend/image/social-2.png"></a>
                    <a href="#"><img src="/frontend/image/social-3.png"></a>
                </div>
            </div>
            <div class="col-lg-5">
                <h3>Quick Links</h3>
                <ul class="footer-links footer-links-two">
                    <li><a href="#">Governor’s Office</a></li>
                    <li><a href="#">Directory</a></li>
                    <li><a href="#">Projects</a></li>
                    <li><a href="#">Ministries</a></li>
                    <li><a href="#">Bereaus</a></li>
                    <li><a href="#">Appointments</a></li>
                    <li><a href="#">About KOGAS</a></li>
                    <li><a href="#">Discussion Forum</a></li>
                    <li><a href="#">2020 Budget</a></li>
                    <li><a href="#">Report</a></li>
                </ul>
            </div>
            <div class="col-lg-2">
                <h3>Ministries</h3>
                <ul class="footer-links">
                    <li><a href="#">Popular Questions</a></li>
                    <li><a href="#">Popular MDAs</a></li>
                    <li><a href="#">Popular Sector</a></li>
                    <li><a href="#">Popular Questions</a></li>
                    <li><a href="#">Popular MDAs</a></li>
                </ul>
            </div>
            <div class="col-lg-2">
                <h3>Contact Us</h3>
                <span>Call Now</span>
                <ul class="footer-links footer-links-three">
                    <li><a href="#">+2348198765432</a></li>
                    <li><a href="#">+2348198765432</a></li>
                </ul>
                <span>Email Address</span>
                <ul class="footer-links">
                    <li><a href="#">info@kogas.com</a></li>
                </ul>

            </div>
        </div>
        <div class="row footer-end">
            <div class="col-lg-6">
                <p>COPYRIGHT 2020 ALL RIGHT RESERVED</p>
            </div>
            <div class="col-lg-6">
                <p>KOGI STATE GOVERNMENT</p>
            </div>
        </div>
    </div>
</footer>
<!-- close-footer -->




<!-- Option 1: Bootstrap Bundle with Popper -->
<script src="/frontend/js/bootstrap.bundle.min.js"></script>
<script src="/frontend/js/jquery.min.js"></script>
<script src="/frontend/js/marquee.js"></script>
<script src="/frontend/js/custom-script.js"></script>
</body>
</html>
