<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="/frontend/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="/frontend/css/marquee.css" />
    <link href="/frontend/fonts/popins.css" rel="stylesheet">
    <link href="/frontend/css/style.css" rel="stylesheet">
    <link href="/frontend/css/style-two.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css">
    <title>Kogas</title>
</head>
<body>

<!--Header-start-->
<header>
    <!---top-row--->
    <div class="top-row">
        <div class="container-fluid">
            <div class="row">
                <!--col-1--->
                <div class="col-lg-4">
                    <div class="logo-top">
                        <img src="/frontend/image/top-logo.png"><span>KOGI STATE GOVERNMENT</span>
                    </div>
                </div>
                <!--col-2--->
                <div class="col-lg-4">
                    <div class="social-top text-center"> <a href="#"><i class="fab fa-facebook-f"></i></a>
                        <a href="#"><i class="fab fa-twitter"></i></a>
                        <a href="#"><i class="fab fa-instagram"></i></a>
                    </div>
                </div>
                <!--col-3--->
                <div class="col-lg-4">
                    <div class="top-right-bar text-end"> <a class="btntop " href="#">login</a>
                        <a class="btntop" href="#">Register</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!---top-row close--->
    <!---Middle-header--->
    <div class="middle-header">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-6">
                    <div class="logo">
                        <a href="index.html"><img src="/frontend/image/logo.png"></a>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="text-end bar-middle-right">
                        <ul>
                            <li><a href="#"><i class="far fa-bell"></i></a>
                            </li>
                            <li><a href="#"><i class="fas fa-search"></i></a>
                            </li>
                            <li><a class="toggle-bar" onclick="openNav()" href="javascript:void(0)"><i class="fas fa-bars"></i></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!---Menu-Nav-item--->
        <div id="mySidenav" class="sidenav header-menus">
            <ul>
                <li class="closebtn" onclick="closeNav()">&times;</li>
                <li><a href="/">Home</a></li>
                <li><a href="{{route("view_officials")}}">Governor Office</a></li>
                <li><a href="{{route("local_government")}}">Local Governments</a></li>
                <li><a href="{{route("ministries")}}">Ministries</a></li>
                {{--          <li><a href="">Ajaokuta Local Government Area</a></li>--}}
            </ul>
        </div>
        <!---Menu-Nav-item Close--->
    </div>
    <!---Middle-header close--->
</header>
<!--Header-close-->

<!--marquee-close-start-->
<div class="marquee-sec">
    <div class="simple-marquee-container">
        <div class="marquee">
            <ul class="marquee-content-items">
                <li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros..</li>
                <li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros..</li>
            </ul>
        </div>
    </div>
</div>
<!--marquee-close-->

<!-- start-governor-office-sec -->
<div class="governor-office-banner">
    <div class="container-fluid">
        <div class="governor-logo-content">
            <img src="/frontend/image/office-logo1.png">
            <div class="office-content">
                <h2><span>Local Governments</span></h2>
                <p>In keeping to promises made to the good people of Kogi State, and in demonstration of full commmitment to the ideals of open governance, this Governor’s Office Portal is hereby opened for direct interaction with His Excellency, Governor Yahaya Bello.</p>
            </div>
            <img src="/frontend/image/office-logo2.png">
        </div>
    </div>
</div>

<!-- close-governor-office-sec -->

<!-- start-standard-icons-sec -->

@include('frontend.template.middlepart')

<!-- close-standard-icons-sec -->

<!-- start-government-area-sec -->

<div class="government-area-sec padding-sec">
    <div class="container-fluid">
        <h3 class="heading">Please select your Local Government Area</h3>
        <div class="row">
            <div class="col-lg-3 col-md-4">
                <div class="local-area">
                    <img src="/frontend/image/local-area1.jpg">
                    <div class="local-area-content">
                        <a href="#">Adavi</a>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-4">
                <div class="local-area">
                    <img src="/frontend/image/local-area1.jpg">
                    <div class="local-area-content">
                        <a href="#">Ajaokuta</a>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-4">
                <div class="local-area">
                    <img src="/frontend/image/local-area1.jpg">
                    <div class="local-area-content">
                        <a href="#">Ankpa</a>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-4">
                <div class="local-area">
                    <img src="/frontend/image/local-area1.jpg">
                    <div class="local-area-content">
                        <a href="#">Bassa</a>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-4">
                <div class="local-area">
                    <img src="/frontend/image/local-area1.jpg">
                    <div class="local-area-content">
                        <a href="#">Dekina</a>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-4">
                <div class="local-area">
                    <img src="/frontend/image/local-area1.jpg">
                    <div class="local-area-content">
                        <a href="#">Ibaji</a>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-4">
                <div class="local-area">
                    <img src="/frontend/image/local-area1.jpg">
                    <div class="local-area-content">
                        <a href="#">Idah</a>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-4">
                <div class="local-area">
                    <img src="/frontend/image/local-area1.jpg">
                    <div class="local-area-content">
                        <a href="#">Igalamela-Odolu</a>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-4">
                <div class="local-area">
                    <img src="/frontend/image/local-area1.jpg">
                    <div class="local-area-content">
                        <a href="#">Kabba/Bunu</a>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-4">
                <div class="local-area">
                    <img src="/frontend/image/local-area1.jpg">
                    <div class="local-area-content">
                        <a href="#">Koton Karfe</a>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-4">
                <div class="local-area">
                    <img src="/frontend/image/local-area1.jpg">
                    <div class="local-area-content">
                        <a href="#">Lokoja</a>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-4">
                <div class="local-area">
                    <img src="/frontend/image/local-area1.jpg">
                    <div class="local-area-content">
                        <a href="#">Mopa-Muro</a>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-4">
                <div class="local-area">
                    <img src="/frontend/image/local-area1.jpg">
                    <div class="local-area-content">
                        <a href="#">Ofu</a>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-4">
                <div class="local-area">
                    <img src="/frontend/image/local-area1.jpg">
                    <div class="local-area-content">
                        <a href="#">AnkOgori/Magongopa</a>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-4">
                <div class="local-area">
                    <img src="/frontend/image/local-area1.jpg">
                    <div class="local-area-content">
                        <a href="#">Okehi</a>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-4">
                <div class="local-area">
                    <img src="/frontend/image/local-area1.jpg">
                    <div class="local-area-content">
                        <a href="#">Okene</a>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-4">
                <div class="local-area">
                    <img src="/frontend/image/local-area1.jpg">
                    <div class="local-area-content">
                        <a href="#">Olamaboro</a>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-4">
                <div class="local-area">
                    <img src="/frontend/image/local-area1.jpg">
                    <div class="local-area-content">
                        <a href="#">Omala</a>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-4">
                <div class="local-area">
                    <img src="/frontend/image/local-area1.jpg">
                    <div class="local-area-content">
                        <a href="#">Yagba East</a>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-4">
                <div class="local-area">
                    <img src="/frontend/image/local-area1.jpg">
                    <div class="local-area-content">
                        <a href="#">Yagba West</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- close-government-area-sec -->



<!-- start-footer -->
<footer class="padding-sec">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-3">
                <a href="#"><img src="/frontend/image/footer-logo.png"></a>
                <div class="footer-social">
                    <a href="#"><img src="/frontend/image/social-1.png"></a>
                    <a href="#"><img src="/frontend/image/social-2.png"></a>
                    <a href="#"><img src="/frontend/image/social-3.png"></a>
                </div>
            </div>
            <div class="col-lg-5">
                <h3>Quick Links</h3>
                <ul class="footer-links footer-links-two">
                    <li><a href="#">Governor’s Office</a></li>
                    <li><a href="#">Directory</a></li>
                    <li><a href="#">Projects</a></li>
                    <li><a href="#">Ministries</a></li>
                    <li><a href="#">Bereaus</a></li>
                    <li><a href="#">Appointments</a></li>
                    <li><a href="#">About KOGAS</a></li>
                    <li><a href="#">Discussion Forum</a></li>
                    <li><a href="#">2020 Budget</a></li>
                    <li><a href="#">Report</a></li>
                </ul>
            </div>
            <div class="col-lg-2">
                <h3>Ministries</h3>
                <ul class="footer-links">
                    <li><a href="#">Popular Questions</a></li>
                    <li><a href="#">Popular MDAs</a></li>
                    <li><a href="#">Popular Sector</a></li>
                    <li><a href="#">Popular Questions</a></li>
                    <li><a href="#">Popular MDAs</a></li>
                </ul>
            </div>
            <div class="col-lg-2">
                <h3>Contact Us</h3>
                <span>Call Now</span>
                <ul class="footer-links footer-links-three">
                    <li><a href="#">+2348198765432</a></li>
                    <li><a href="#">+2348198765432</a></li>
                </ul>
                <span>Email Address</span>
                <ul class="footer-links">
                    <li><a href="#">info@kogas.com</a></li>
                </ul>

            </div>
        </div>
        <div class="row footer-end">
            <div class="col-lg-6">
                <p>COPYRIGHT 2020 ALL RIGHT RESERVED</p>
            </div>
            <div class="col-lg-6">
                <p>KOGI STATE GOVERNMENT</p>
            </div>
        </div>
    </div>
</footer>
<!-- close-footer -->




<!-- Option 1: Bootstrap Bundle with Popper -->
<script src="/frontend/js/bootstrap.bundle.min.js"></script>
<script src="/frontend/js/jquery.min.js"></script>
<script src="/frontend/js/marquee.js"></script>
<script src="/frontend/js/custom-script.js"></script>

</body>
</html>
