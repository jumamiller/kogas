@extends('frontend.template.master')
@section('content')

    <header>
        <!---top-row--->
    @include('frontend.template.toppart')
    <!---top-row close--->

        <div id="mySidenav" class="sidenav header-menus">
            <ul>
                <li class="closebtn" onclick="closeNav()">&times;</li>
                <li><a href="/">Home</a></li>
                <li><a href="{{route("view_officials")}}">Governor Office</a></li>
                <li><a href="{{route("local_government")}}">Local Governments</a></li>
                <li><a href="{{route("ministries")}}">Ministries</a></li>
                {{--          <li><a href="">Ajaokuta Local Government Area</a></li>--}}
            </ul>
        </div>
        <!---Menu-Nav-item Close--->

        <!---Middle-header close--->
    </header>
    <!--Header-close-->

    <!--marquee-close-start-->
    <div class="marquee-sec">
        <div class="simple-marquee-container">
            <div class="marquee">
                <ul class="marquee-content-items">
                    <li>KOGAS Portal
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <!--marquee-close-->

    <!-- start-governor-office-sec -->
    <div class="governor-office-banner">
        <div class="container-fluid">
            <div class="governor-logo-content">
                <img src="{{asset('/frontend/image/office-logo1.png')}}">
                <div class="office-content">
                    <h2>Appointments</h2>
                    <p>Book Appointment </p>
                </div>
                <img src="{{asset('/frontend/image/office-logo2.png')}}">
            </div>
        </div>
    </div>

    <!-- close-governor-office-sec -->

    <!-- start-standard-icons-sec -->
    @include('frontend.template.middlepart')

    <div class="card-body">
            @if($my_appointments)
                <table id="appointments_table" class="table table-bordered table-hover">
                    <thead>
                    <tr>
                        <th> Name</th>
                        <th> Email</th>
                        <th> State</th>
                        <th> City</th>
{{--                        <th> Type</th>--}}
                        <th> Ministry</th>
{{--                        <th> Department</th>--}}
                        <th> Official</th>
                        <th> Message</th>
                        <th> Date Created</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($my_appointments as $appointment)
                        <tr>
                            <td> {{$appointment->name}} </td>
                            <td> {{$appointment->email}} </td>
                            <td> {{$appointment->state}} </td>
                            <td> {{$appointment->city}} </td>
{{--                            <td> {{$appointment->type}} </td>--}}
                            <td> {{$appointment->ministry()->first()->name}} </td>
{{--                            <td> {{$appointment->department}} </td>--}}
                            <td> {{$appointment->official()->first()->name}} </td>
                            <td> {{$appointment->message}} </td>
                            <td> {{$appointment->created_at}} </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
        </div>
    @else
        <div class="appointments-sec padding-sec  minus-margin-topbg section-bg1">
            <div class="container-fluid">
                <div class="appointment-box">
                    <div class="icon-icon"><i class="fas fa-calendar-alt"></i></div>
                    <p>You’ve not book any appointment. Click<span>the Button below to <a href="my-appointments.html"> Book Now</a></span>
                    </p>
                    <a href="{{route('book.appointment')}}" class="book-aapointment button">Book Appointment</a>
                </div>
            </div>
        </div>
    @endif
    <!-- close-standard-icons-sec -->


   <div class="appointments-sec padding-sec  minus-margin-topbg section-bg1">
        <div class="container-fluid">
            <div class="appointment-box">
                <div class="icon-icon"><i class="fas fa-calendar-alt"></i></div>
                <a href="{{route('book.appointment')}}" class="book-aapointment button">Book Appointment</a>
            </div>
        </div>
    </div>

@stop
