<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="/frontend/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="/frontend/css/marquee.css" />
    <link href="/frontend/fonts/popins.css" rel="stylesheet">
    <link href="/frontend/css/style.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css">
    <link rel="stylesheet" href="/frontend/css/slick-theme.min.css">
    <link rel="stylesheet" href="/frontend/css/slick.css">
    <title>Kogas</title>
</head>
<body>

<!--Header-start-->
@include('frontend.template.toppart')
<!--Header-close-->
<!--marquee-close-start-->
<div class="marquee-sec">
    <div class="simple-marquee-container">
        <div class="marquee">
            <ul class="marquee-content-items">
                <li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros..</li>
                <li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros..</li>
            </ul>
        </div>
    </div>
</div>
<!--marquee-close-->

<!-- start-governor-office-sec -->
<div class="governor-office-banner">
    <div class="container-fluid">
        <div class="governor-logo-content">
            <img src="/frontend/image/office-logo1.png">
            <div class="office-content">
                <h2>Welcome to the <span>GOVERNOR’S OFFICE</span></h2>
                <p>In keeping to promises made to the good people of Kogi State, and in demonstration of full commmitment to the ideals of open governance, this Governor’s Office Portal is hereby opened for direct interaction with His Excellency, Governor Yahaya Bello.</p>
            </div>
            <img src="/frontend/image/office-logo2.png">
        </div>
    </div>
</div>

<!-- close-governor-office-sec -->

<!-- start-standard-icons-sec -->
@include('frontend.template.middlepart')

<!-- close-standard-icons-sec -->

<!-- start-message-governor-sec -->
<div class="message-governor-sec padding-sec">
    <div class="container-fluid">
        <h3 class="heading">Message from the Governor</h3>
        <div class="row from-governor">
            <div class="col-lg-7">
                <p>Dear Kogites,</p>
                <p>Sed egestas, ante et vulputate volutpat, eros pede semper est, vitae luctus metus libero eu augue. Morbi purus libero, faucibus adipiscing, commodo quis, gravida id, est. Sed lectus. Praesent elementum hendrerit tortor. Sed semper lorem at felis. Vestibulum volutpat, lacus a ultrices sagittis, mi neque euismod.</p>
                <p>Sed egestas, ante et vulputate volutpat, eros pede semper est, vitae luctus metus libero eu augue. Morbi purus libero, faucibus adipiscing, commodo quis, gravida id, est. Sed lectus.</p>
                <p>Sed egestas, ante et vulputate volutpat, eros pede semper est, vitae luctus metus libero eu augue. Morbi purus libero, faucibus adipiscing, commodo quis, gravida id, est. Sed lectus. </p>
                <p>You are most welcome.</p>
                <p>Yours sincerely,
                    <strong>Alh. Yahaya Bello</strong>
                    <strong>Executive Governor</strong></p>
            </div>
            <div class="col-lg-5">
                <img src="/frontend/image/governor-img.jpg">
            </div>
        </div>
        <div class="row governor-buttons">
            <div class="col-lg-6">
                <a class="clr-1" href="tel:+62896706255135">Contact the Governor</a>
            </div>
            <div class="col-lg-6">
                <a class="clr-4" href="{{route('idea_suggestion')}}">Idea / Suggestions</a>
            </div>
            <div class="col-lg-6">
                <a class="clr-3" href="{{route('submit_report')}}">Submit Report</a>
            </div>
            <div class="col-lg-6">
                <a class="clr-6" href="#">Request Services</a>
            </div>
        </div>
    </div>
</div>
<!-- close-message-governor-sec -->

<!-- start-executive-officials-sec -->
<div class="upcoming-project-sec executive-officials padding-sec">
    <div class="container-fluid">
        <h3 class="heading">Executive Officials</h3>
        <div class="slick-carousel upcoming-slide">
            @foreach($officials as $official)
            <div class="upcoming-inner">
                <div class="up-sec">
                    <img src="{{asset("uploads/profile_images/".$official->official_image)}}">
                    <h5>{{$official->name}}</h5>
                    <h6>Office {{$official->office}}</h6>
                    @if($official->ministry)
                    <h6>Ministry {{$official->ministry()->first()->name}}
                    </h6>
                    @endif
                </div>
            </div>
            @endforeach

        </div>
    </div>
</div>

<!-- close-executive-officials-sec -->


<!-- start-footer -->
<footer class="padding-sec">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-3">
                <a href="#"><img src="/frontend/image/footer-logo.png"></a>
                <div class="footer-social">
                    <a href="#"><img src="/frontend/image/social-1.png"></a>
                    <a href="#"><img src="/frontend/image/social-2.png"></a>
                    <a href="#"><img src="/frontend/image/social-3.png"></a>
                </div>
            </div>
            <div class="col-lg-5">
                <h3>Quick Links</h3>
                <ul class="footer-links footer-links-two">
                    <li><a href="#">Governor’s Office</a></li>
                    <li><a href="#">Directory</a></li>
                    <li><a href="#">Projects</a></li>
                    <li><a href="#">Ministries</a></li>
                    <li><a href="#">Bereaus</a></li>
                    <li><a href="#">Appointments</a></li>
                    <li><a href="#">About KOGAS</a></li>
                    <li><a href="#">Discussion Forum</a></li>
                    <li><a href="#">2020 Budget</a></li>
                    <li><a href="#">Report</a></li>
                </ul>
            </div>
            <div class="col-lg-2">
                <h3>Ministries</h3>
                <ul class="footer-links">
                    <li><a href="#">Popular Questions</a></li>
                    <li><a href="#">Popular MDAs</a></li>
                    <li><a href="#">Popular Sector</a></li>
                    <li><a href="#">Popular Questions</a></li>
                    <li><a href="#">Popular MDAs</a></li>
                </ul>
            </div>
            <div class="col-lg-2">
                <h3>Contact Us</h3>
                <span>Call Now</span>
                <ul class="footer-links footer-links-three">
                    <li><a href="#">+2348198765432</a></li>
                    <li><a href="#">+2348198765432</a></li>
                </ul>
                <span>Email Address</span>
                <ul class="footer-links">
                    <li><a href="#">info@kogas.com</a></li>
                </ul>

            </div>
        </div>
        <div class="row footer-end">
            <div class="col-lg-6">
                <p>COPYRIGHT 2020 ALL RIGHT RESERVED</p>
            </div>
            <div class="col-lg-6">
                <p>KOGI STATE GOVERNMENT</p>
            </div>
        </div>
    </div>
</footer>
<!-- close-footer -->




<!-- Option 1: Bootstrap Bundle with Popper -->
<script src="/frontend/js/bootstrap.bundle.min.js"></script>
<script src="/frontend/js/jquery.min.js"></script>
<script src="/frontend/js/marquee.js"></script>
<script src="/frontend/js/slick.min.js"></script>
<script src="/frontend/js/custom-script.js"></script>
<script>
    $('.upcoming-slide').slick({
        dots: true,
        infinite: false,
        speed: 300,
        slidesToShow: 4,
        slidesToScroll: 1,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1,
                    infinite: true,
                    dots: true
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    });
</script>

</body>
</html>
