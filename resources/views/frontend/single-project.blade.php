<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="/frontend/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="/frontend/css/marquee.css" />
    <link href="/frontend/fonts/popins.css" rel="stylesheet">
    <link href="/frontend/css/style.css" rel="stylesheet">
    <link href="/frontend/css/style-two.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css">
    <title>Kogas</title>
  </head>
  <body>

    <!--Header-start-->
@include('frontend.template.toppart')
  <!--Header-close-->

<!--marquee-close-start-->
  <div class="marquee-sec">
    <div class="simple-marquee-container">
      <div class="marquee">
        <ul class="marquee-content-items">
          <li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros..</li>
          <li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros..</li>
        </ul>
      </div>
    </div>
  </div>
<!--marquee-close-->

<!-- start-governor-office-sec -->
<div class="governor-office-banner">
  <div class="container-fluid">
    <div class="governor-logo-content">
      <img src="/frontend/image/office-logo1.png">
      <div class="office-content">
        <h2><span>{{$project->project_name}}</span></h2>
      </div>
      <img src="/frontend/image/office-logo2.png">
    </div>
  </div>
</div>

<!-- close-governor-office-sec -->

<!-- start-standard-icons-sec -->

@include('frontend.template.middlepart')

<!-- close-standard-icons-sec -->

<!-- start-project-table -->

<div class="project-table red-bg padding-sec">
  <div class="container-fluid">
  <ul>
    <li>Client:</li>
    <li>{{$project->client}}</li>
  </ul>
    <ul>
    <li>Contractor:</li>
    <li>{{$project->contractor}}</li>
  </ul>
    <ul>
    <li>Facilitator:</li>
    <li>{{$project->facilator}}</li>
  </ul>
    <ul>
    <li>Ministry:</li>
    <li>{{$project->ministry}}</li>
  </ul>
    <ul>
    <li>Duration:</li>
    <li>{{$project->duration}}</li>
  </ul>

  </div>
</div>

<!-- close-project-table -->

<!-- start-project-discription -->
<div class="project-discription padding-sec">
  <div class="container-fluid">
    <div class="project-full-banner">
        <img style="max-height: 500px" src="{{asset('uploads/project_image/thumb_image/'.$project-> thumb_image)}}">
    </div>
    <p>{{$project->project_detail}}</p>


<div class="row project-images">
@foreach($project->images as $image)

    <div class="col-lg-3 col-md-4">
        <img src="{{asset("uploads/project_image/banner_image/".$image-> image)}}">
    </div>
@endforeach

</div>

<div class="governor-buttons row">
  <div class="col-lg-4">
    <a class="clr-6" href="#">Contact Facilitator</a>
  </div>
    <div class="col-lg-4">
    <a class="clr-8" href="#">Submit Complaints</a>
  </div>
    <div class="col-lg-4">
     <a class="clr-4" href="#">Idea/Suggestions</a>
  </div>
    </div>

  </div>
</div>
<!-- close-project-discription -->





        <!-- start-footer -->
  @include('frontend.template.footer')
    <!-- close-footer -->




  <!-- Option 1: Bootstrap Bundle with Popper -->
  <script src="/frontend/js/bootstrap.bundle.min.js"></script>
  <script src="/frontend/js/jquery.min.js"></script>
  <script src="/frontend/js/marquee.js"></script>
  <script src="/frontend/js/custom-script.js"></script>

  </body>
</html>
