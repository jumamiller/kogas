@extends('frontend.template.master')

@section('content')

    <header>
        <!---top-row--->
    @include('frontend.template.toppart')
    <!---top-row close--->
        <!---Middle-header--->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="backend/plugins/fontawesome-free/css/all.min.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="dist/css/adminlte.min.css">
        <!-- summernote -->
        <link rel="stylesheet" href="backend/plugins/summernote/summernote-bs4.min.css">
        <!-- CodeMirror -->
        <link rel="stylesheet" href="backend/plugins/codemirror/codemirror.css">
        <link rel="stylesheet" href="/backend/plugins/codemirror/theme/monokai.css">
        <!-- SimpleMDE -->
        <link rel="stylesheet" href="/backend/plugins/simplemde/simplemde.min.css">
        <link rel="stylesheet" href="/backend/plugins/daterangepicker/daterangepicker.css">
        <!-- iCheck for checkboxes and radio inputs -->
        <link rel="stylesheet" href="/backend/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
        <!-- Bootstrap Color Picker -->
        <link rel="stylesheet" href="/backend/plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css">
        <!-- Tempusdominus Bootstrap 4 -->
        <link rel="stylesheet" href="/backend/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
        <!-- Select2 -->
        <link rel="stylesheet" href="/backend/plugins/select2/css/select2.min.css">
        <link rel="stylesheet" href="/backend/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
        <!-- Bootstrap4 Duallistbox -->
        <link rel="stylesheet" href="/backend/plugins/bootstrap4-duallistbox/bootstrap-duallistbox.min.css">
        <!-- BS Stepper -->
        <link rel="stylesheet" href="/backend/plugins/bs-stepper/css/bs-stepper.min.css">
        <!-- dropzonejs -->
        <link rel="stylesheet" href="/backend/plugins/dropzone/min/dropzone.min.css">
        <!---Menu-Nav-item--->
        <div id="mySidenav" class="sidenav header-menus">
            <ul>
                <li class="closebtn" onclick="closeNav()">&times;</li>
                <li><a href="/">Home</a></li>
                <li><a href="{{route("view_officials")}}">Governor Office</a></li>
                <li><a href="{{route("local_government")}}">Local Governments</a></li>
                <li><a href="{{route("ministries")}}">Ministries</a></li>
                {{--          <li><a href="">Ajaokuta Local Government Area</a></li>--}}
            </ul>
        </div>
        <!---Menu-Nav-item Close--->
        </div>
        <!---Middle-header close--->
    </header>
    <!--Header-close-->

    <!--marquee-close-start-->
    <div class="marquee-sec">
        <div class="simple-marquee-container">
            <div class="marquee">
                <ul class="marquee-content-items">
                    <li>KOGAS Portal</li>
                </ul>
            </div>
        </div>
    </div>
    <!--marquee-close-->

    <!-- start-governor-office-sec -->
    <div class="governor-office-banner">
        <div class="container-fluid">
            <div class="governor-logo-content">
                <img src="{{asset('/frontend/image/office-logo1.png')}}">
                <div class="office-content">
                    <h2><span>New Appointment</span></h2>
                    <p>Sed egestas, ante et vulputate volutpat, eros pede semper est, vitae luctus metus libero eu augue. Morbi purus libero, faucibus adipiscing, commodo quis, gravida id, est. Sed lectus. Praesent elementum hendrerit tortor.</p>
                </div>
                <img src="{{asset('/frontend/image/office-logo2.png')}}">
            </div>
        </div>
    </div>

    <!-- close-governor-office-sec -->

    <!-- start-standard-icons-sec -->

    @include('frontend.template.middlepart')
    <!-- close-standard-icons-sec -->

    <!-- start-contact-form -->

    <div class="contact-form padding-sec">
        <div class="container-fluid">
            <label id="appointment_status" style="background-color: green"></label>

            <div class="formBox">
                <form>
                    <div class="row">
                        <div class="col-sm-12 col-lg-9">
                            <div class="inputBox ">
                                <label>First Name <span>*</span></label>
                                <input type="text" id="first_name" name="first_name" placeholder="First Name">
                            </div>
                            <div class="inputBox">
                                <label>Last Name <span>*</span></label>
                                <input type="text" id="last_name" name="last_name" placeholder="Last Name">
                            </div>
                        </div>
                        <div class="col-sm-12 col-lg-3">
                            <img src="{{asset('/frontend/image/appointment-img.jpg')}}">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 col-lg-3">
                            <div class="inputBox">
                                <label>Phone <span>*</span></label>
                                <input type="text" id="phone" name="phone" placeholder="Phone Number">
                            </div>
                        </div>
                        <div class="col-sm-12 col-lg-6">
                            <div class="inputBox">
                                <label>Email (Optional)</label>
                                <input type="email" id="email" name="email" placeholder="Email Address">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 col-lg-6">
                            <div class="inputBox">
                                <label>State <span>*</span></label>
                                <select name="state" id="state" form="stateform">
                                    <option value="Kogi">Kogi state</option>

                                </select>
                            </div>
                        </div>
                        <div class="col-sm-12 col-lg-6">
                            <div class="inputBox">
                                <label>Town/City <span>*</span></label>
                                <select name="city" id="city" form="cityform">
                                    <option value="Kogi">Okene</option>

                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 col-lg-6">
                            <div class="inputBox">
                                <label>Ministry/Agency/Bureau, etc? <span>*</span></label>
                                <select name="office" id="office" form="officeform">
                                    <option value="1">Office of the Governor</option>

                                </select>
                            </div>
                        </div>
                        <div class="col-sm-12 col-lg-6">
                            <div class="inputBox">
                                <label>Name of Ministry <span>*</span></label>
                                <select name="governor" id="governor" form="governorform">
                                    @foreach($ministries as $ministry)
                                        <option value="{{$ministry->id}}">{{$ministry->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 col-lg-6">
                            <div class="inputBox">
                                <label>Department/Unit <span>*</span></label>
                                <select name="office" id="office" form="officeform">
                                    <option value="volvo">Office of the Governor</option>

                                </select>
                            </div>
                        </div>
                        <div class="col-sm-12 col-lg-6">
                            <div class="inputBox">
                                <label>Select Official <span>*</span></label>
                                <select name="governor" id="governor" form="governorform">
                                    @foreach($officials as $official)
                                        <option value="{{$official->id}}">{{$official->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="inputBox">
                                <label>Attachments (Optional)</label>
                                <div class="search-form-sec">
                                    <input type="file" multiple id="attachment" name="attachment" placeholder="Select a file  (JPG, PNG, PDF, DOCX, MP3, MP4, etc.)">
                                    <button>Select File</button>
                                </div>

                            </div>
                        </div>
                    </div>
                    <!-- Main content -->
                    {{-- <section class="content">--}}
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card card-outline card-info">
                                <div class="card-header">
                                    <h3 class="card-title">
                                        Message
                                    </h3>
                                </div>
                                <!-- /.card-header -->
                                <div class="card-body">
              <textarea id="message">

              </textarea>
                                </div>

                            </div>
                        </div>
                        <!-- /.col-->
                    </div>
                    <!-- ./row -->
                    <!--TEXT EDITOR-->


                    {{--
                                      <div class="row">
                                          <div class="form-group">
                                              <label> Appointment Date and Time:</label>
                                              <div class="input-group date" id="reservationdatetime" data-target-input="nearest">
                                                  <input type="text" id="datetime" class="form-control datetimepicker-input" data-target="#reservationdatetime"/>
                                                  <div class="input-group-append" data-target="#reservationdatetime" data-toggle="datetimepicker">
                                                      <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                                  </div>
                                              </div>
                                          </div>

                                      </div>--}}



                    <div class="row">
                        <div class="col-sm-12">
                            <input type="submit" name="" id="save_appointment" class="button-two" value="Book Appointment">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <script src="/frontend/js/bootstrap.bundle.min.js"></script>
    <script src="/frontend/js/jquery.min.js"></script>
    <script src="/frontend/js/marquee.js"></script>
    <script src="/frontend/js/custom-script.js"></script>
    </script>



    <script src="/backend/plugins/jquery/jquery.min.js"></script>
    <!-- Bootstrap 4 -->
    <script src="/backend/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- AdminLTE App -->
    <script src="/backend/dist/js/adminlte.min.js"></script>
    <!-- Summernote -->
    <script src="/backend/plugins/summernote/summernote-bs4.min.js"></script>
    <!-- CodeMirror -->
    <script src="/backend/plugins/codemirror/codemirror.js"></script>
    <script src="/backend/plugins/codemirror/mode/css/css.js"></script>
    <script src="/backend/plugins/codemirror/mode/xml/xml.js"></script>
    <script src="/backend/plugins/codemirror/mode/htmlmixed/htmlmixed.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="/backend/dist/js/demo.js"></script>
    <!-- Page specific script -->
    <script>
        $(function () {
            // Summernote
            $('#message').summernote()

            // CodeMirror
            CodeMirror.fromTextArea(document.getElementById("codeMirrorDemo"), {
                mode: "htmlmixed",
                theme: "monokai"
            });
        })
    </script>

    <!-- jQuery -->
    <script src="/backend/plugins/jquery/jquery.min.js"></script>
    <!-- Bootstrap 4 -->
    <script src="/backend/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- Select2 -->
    <script src="/backend/plugins/select2/js/select2.full.min.js"></script>
    <!-- Bootstrap4 Duallistbox -->
    <script src="/backend/plugins/bootstrap4-duallistbox/jquery.bootstrap-duallistbox.min.js"></script>
    <!-- InputMask -->
    <script src="/backend/plugins/moment/moment.min.js"></script>
    <script src="/backend/plugins/inputmask/jquery.inputmask.min.js"></script>
    <!-- date-range-picker -->
    <script src="/backend/plugins/daterangepicker/daterangepicker.js"></script>
    <!-- bootstrap color picker -->
    <script src="/backend/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js"></script>
    <!-- Tempusdominus Bootstrap 4 -->
    <script src="/backend/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
    <!-- Bootstrap Switch -->
    <script src="/backend/plugins/bootstrap-switch/js/bootstrap-switch.min.js"></script>
    <!-- BS-Stepper -->
    <script src="/backend/plugins/bs-stepper/js/bs-stepper.min.js"></script>
    <!-- dropzonejs -->
    <script src="/backend/plugins/dropzone/min/dropzone.min.js"></script>
    <!-- AdminLTE App -->
    <script src="/backend/dist/js/adminlte.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="/backend/dist/js/demo.js"></script>
    <!-- Page specific script -->
    <script>
        $(function () {
            //Initialize Select2 Elements
            $('.select2').select2()

            //Initialize Select2 Elements
            $('.select2bs4').select2({
                theme: 'bootstrap4'
            })

            //Datemask dd/mm/yyyy
            $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
            //Datemask2 mm/dd/yyyy
            $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
            //Money Euro
            $('[data-mask]').inputmask()

            //Date picker
            $('#reservationdate').datetimepicker({
                format: 'L'
            });

            //Date and time picker
            $('#reservationdatetime').datetimepicker({ icons: { time: 'far fa-clock' } });

            //Date range picker
            $('#reservation').daterangepicker()
            //Date range picker with time picker
            $('#reservationtime').daterangepicker({
                timePicker: true,
                timePickerIncrement: 30,
                locale: {
                    format: 'MM/DD/YYYY hh:mm A'
                }
            })
            //Date range as a button
            $('#daterange-btn').daterangepicker(
                {
                    ranges   : {
                        'Today'       : [moment(), moment()],
                        'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                        'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
                        'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                        'This Month'  : [moment().startOf('month'), moment().endOf('month')],
                        'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                    },
                    startDate: moment().subtract(29, 'days'),
                    endDate  : moment()
                },
                function (start, end) {
                    $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
                }
            )

            //Timepicker
            $('#timepicker').datetimepicker({
                format: 'LT'
            })

            //Bootstrap Duallistbox
            $('.duallistbox').bootstrapDualListbox()

            //Colorpicker
            $('.my-colorpicker1').colorpicker()
            //color picker with addon
            $('.my-colorpicker2').colorpicker()

            $('.my-colorpicker2').on('colorpickerChange', function(event) {
                $('.my-colorpicker2 .fa-square').css('color', event.color.toString());
            })

            $("input[data-bootstrap-switch]").each(function(){
                $(this).bootstrapSwitch('state', $(this).prop('checked'));
            })

        })
        // BS-Stepper Init
        document.addEventListener('DOMContentLoaded', function () {
            window.stepper = new Stepper(document.querySelector('.bs-stepper'))
        })

        // DropzoneJS Demo Code Start
        Dropzone.autoDiscover = false

        // Get the template HTML and remove it from the doumenthe template HTML and remove it from the doument
        var previewNode = document.querySelector("#template")
        previewNode.id = ""
        var previewTemplate = previewNode.parentNode.innerHTML
        previewNode.parentNode.removeChild(previewNode)

        var myDropzone = new Dropzone(document.body, { // Make the whole body a dropzone
            url: "/target-url", // Set the url
            thumbnailWidth: 80,
            thumbnailHeight: 80,
            parallelUploads: 20,
            previewTemplate: previewTemplate,
            autoQueue: false, // Make sure the files aren't queued until manually added
            previewsContainer: "#previews", // Define the container to display the previews
            clickable: ".fileinput-button" // Define the element that should be used as click trigger to select files.
        })

        myDropzone.on("addedfile", function(file) {
            // Hookup the start button
            file.previewElement.querySelector(".start").onclick = function() { myDropzone.enqueueFile(file) }
        })

        // Update the total progress bar
        myDropzone.on("totaluploadprogress", function(progress) {
            document.querySelector("#total-progress .progress-bar").style.width = progress + "%"
        })

        myDropzone.on("sending", function(file) {
            // Show the total progress bar when upload starts
            document.querySelector("#total-progress").style.opacity = "1"
            // And disable the start button
            file.previewElement.querySelector(".start").setAttribute("disabled", "disabled")
        })

        // Hide the total progress bar when nothing's uploading anymore
        myDropzone.on("queuecomplete", function(progress) {
            document.querySelector("#total-progress").style.opacity = "0"
        })

        // Setup the buttons for all transfers
        // The "add files" button doesn't need to be setup because the config
        // `clickable` has already been specified.
        document.querySelector("#actions .start").onclick = function() {
            myDropzone.enqueueFiles(myDropzone.getFilesWithStatus(Dropzone.ADDED))
        }
        document.querySelector("#actions .cancel").onclick = function() {
            myDropzone.removeAllFiles(true)
        }
        // DropzoneJS Demo Code End
    </script>

    <script>

        $("#save_appointment").click(function(event){
            event.preventDefault();
            let firstName = $("#first_name").val();
            let lastName = $("#last_name").val();
            let email = $("#email").val();
            let state = $("#state").val();
            let phone = $("#phone").val();
            let city = $("#city").val();
            let office = $("#office").val();
            let message = $("#message").val();
            let appointment_date = $("#datetime").val();


            $.ajax({
                url: "user/save_appointment",
                type:"POST",
                data:{
                    firstName:firstName,
                    lastName:lastName,
                    email:email,
                    state:state,
                    city:city,
                    phone:phone,
                    office:office,
                    message:message,
                    appointment_date:appointment_date

                },
                success:function(response){
                    console.log(response);
                    if(response) {

                        $('#appointment_status').text(response['message']);


                    }
                },
            });
        });
    </script>
@stop
