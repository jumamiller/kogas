@extends('frontend.template.master')
@section('content')

    <header>
        <!---top-row--->
    @include('frontend.template.toppart')
    <!---top-row close--->
        <!---Middle-header--->

        <!---Middle-header close--->
    </header>
    <!--Header-close-->

    <!--marquee-close-start-->
    <div class="marquee-sec">
        <div class="simple-marquee-container">
            <div class="marquee">
                <ul class="marquee-content-items">
                    <li>KOGAS Project In Progress</li>
                    <li>KOGAS Project In Progress</li>
                </ul>
            </div>
        </div>
    </div>
    <!--marquee-close-->

    <!--banner-sec-->
    <div class="banner-sec">
        <div id="banner-slider" class="carousel slide carousel-fade" data-bs-ride="carousel">
            <div class="carousel-inner">
                <!---slide1--->
                <div class="carousel-item active">
                    <div class="row m-0 flex-row-reverse">
                        <div class="banner-bg">
                            <img src="{{asset('/frontend/image/1bg.png')}}" class="bg-img">
                            <img src="{{asset('/frontend/image/baner-bottom.png')}}" class="bottom-bg">
                        </div>
                        <div class="container-fluid carousel-fluid">
                            <div class="row">
                                <div class="col-lg-5">
                                    <div class="text">
                                        <h1>VOLUNTEER</h1>
                                        <p>We know that you are passionate about your work and skills. We also know that you would love to impact your communities. The KOGI OPEN GOVERNANCE AND ACCOUNTABILITY SYSTEM (KOGAS) provides you a platform to showcase your Volunteer and Community Service. You wil be able to find like minded collaborators and enjoy recognition for your good work.</p> <a href="#" class="button">start a project</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 p-0 col-right-image">
                            <div class="banner-col-right">
                                <img src="{{asset('/frontend/image/banner1.png')}}">
                            </div>
                        </div>
                    </div>
                </div>
                <!---slide2--->
                <div class="carousel-item ">
                    <div class="row m-0 flex-row-reverse">
                        <div class="banner-bg">
                            <img src="{{asset('/frontend/image/2bg.png')}}" class="bg-img">
                            <img src="{{asset('/frontend/image/baner-bottom.png')}}" class="bottom-bg">
                        </div>
                        <div class="container-fluid carousel-fluid">
                            <div class="row">
                                <div class="col-lg-5">
                                    <div class="text">
                                        <h1>Engage Your Leaders</h1>
                                        <p>You are welcome to KOGI OPEN GOVERNANCE AND ACCOUNTABIITY SYSTEM (KOGAS) where you are free to engage your elected and political office holders on subjects of importance to you. You are empowered, through KOGAS to probe, question and interrogate every office holder in the state. Your contributions not only promote accountability but also enhance policy formulation, implementation, and evaluation.</p>
                                        <a href="#" class="btn-pro button white-btn">Engage a Leader</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 p-0 col-right-image">
                            <div class="banner-col-right">
                                <img src="{{asset('/frontend/image/banner2.png')}}">
                            </div>
                        </div>
                    </div>
                </div>

                <!---slide3--->
                <div class="carousel-item ">
                    <div class="row m-0 flex-row-reverse">
                        <div class="banner-bg">
                            <img src="{{asset('/frontend/image/3bg.png')}}" class="bg-img">
                            <img src="{{asset('/frontend/image/baner-bottom.png')}}" class="bottom-bg">
                        </div>
                        <div class="container-fluid carousel-fluid">
                            <div class="row">
                                <div class="col-lg-5">
                                    <div class="text">
                                        <h1>COLLABORATE with government</h1>
                                        <p>What is that brilliant idea that you nurse? Do you have a proposal for consideration? The KOGI OPEN GOVERNANCE AND ACCOUNTABILITY SYSTEM (KOGAS) welcomes your brilliant ideas and proposals. We have structured collaboration as a key enbaler for our state’s growth and development and thus invite you in to work with us - this is your government after all.</p>
                                        <a href="#" class="blue-btn button">submit proposal</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 p-0 col-right-image">
                            <div class="banner-col-right">
                                <img src="{{asset('/frontend/image/banner3.png')}}">
                            </div>
                        </div>
                    </div>
                </div>

                <!---slide4--->
                <div class="carousel-item ">
                    <div class="row m-0 flex-row-reverse">
                        <div class="banner-bg">
                            <img src="{{asset('/frontend/image/4bg.png')}}" class="bg-img">
                            <img src="{{asset('/frontend/image/baner-bottom.png')}}" class="bottom-bg">
                        </div>
                        <div class="container-fluid carousel-fluid">
                            <div class="row">
                                <div class="col-lg-5">
                                    <div class="text">
                                        <h1>OBTAIN MEETING ACCESS  </h1>
                                        <p>To underscore the representative mandate of politicial leadership, KOGAS enables meeting access to citizens and businesses in Kog Stater. Residents and citizens of Kogi State can now, from the comfort of their homes, schools, farmlands and offices request access for physical or virtual meetings with their leaders. Simply fill the access form and await notification of approval via SMS, Email or Phone. </p>
                                        <a href="#" class="blue-btn button">ACCESS GOVERNOR</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 p-0 col-right-image">
                            <div class="banner-col-right">
                                <img src="{{asset('/frontend/image/banner4.png')}}">
                            </div>
                        </div>
                    </div>
                </div>

                <!---slide5--->
                <div class="carousel-item ">
                    <div class="row m-0 flex-row-reverse">
                        <div class="banner-bg">
                            <img src="{{asset('/frontend/image/5bg.png')}}" class="bg-img">
                            <img src="{{asset('/frontend/image/baner-bottom.png')}}" class="bottom-bg">
                        </div>
                        <div class="container-fluid carousel-fluid">
                            <div class="row">
                                <div class="col-lg-5">
                                    <div class="text">
                                        <h1>Governor</h1>
                                        <p>“The promise of KOGAS is today a promise kept and testimony of our administration’s commitment to accountability. Our emphasis on open governance and accountability is timely, well considered and primed for our quest for more recordable growth and progress of our dear state. It is our utmost belief that collaboration and structured enagagement with you our citizens and residents, expands our capacity to do more. “</p>
                                        <p class="text-end">
                                            - Yahaya Adoza Bello
                                            <span>Executive Governor, Kogi State</span>
                                        </p>
                                        <a href="#" class="button">BOOK APPOINTMENT</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 p-0 col-right-image">
                            <div class="banner-col-right">
                                <img src="{{asset('/frontend/image/banner5.png')}}">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <a class="carousel-control-prev" href="#banner-slider" role="button" data-bs-slide="prev"> <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Previous</span>
            </a>
            <a class="carousel-control-next" href="#banner-slider" role="button" data-bs-slide="next"> <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Next</span>
            </a>
        </div>
    </div>
    <!--banner-sec close-->

    <!-- start-standard-icons-sec -->
    @include('frontend.template.middlepart')
    <!-- close-standard-icons-sec -->
    <!-- startupcoming-project-sec -->
    <div class="upcoming-project-sec mt-5" style="cursor: pointer;">
        <!-- startupcoming-project-sec -->
        <div class="upcoming-project-sec">
            <div class="container-fluid">
                <h3 class="heading">{{__("Upcoming Projects")}}</h3>
                <div class="slick-carousel">
                    @foreach($projects as $project)
                        <div class="row">
                            <div class="card m-2 border shadow bg-white rounded"
                                 style="width: 350px;height:270px; padding: 4px; background-image: url({{asset('/uploads/project_image/thumb_image/'.$project-> thumb_image)}})">
                                <div class="upcoming-inner justify-content-end row">
                                    <div class="col-md-8"></div>
                                    <div class="col-md-4">
                                        <span class="text-white text-left font-weight-bolder" style="background: #1C4271;">{{__("Title")}}</span>
                                    </div>
                                </div>
                            </div>
                            <p class="ml-10">
                                {{$project->project_detail}}
                            </p>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
<div class="" style="background:url({{asset('images/background.jpg')}})">
    <!-- start-news-event-sec .news-event-sec-->
    <div class="text-white">
        <div class="container-fluid">
            <h3 class="text-center font-weight-bolder"style="margin-top: 10px;padding: 20px">{{__("News / Events")}}</h3>
            <div class="row mt-2">
                <div class="col-md-6">
                    <div class="left-side-news">
                        <img src="{{asset('/frontend/image/news-1.jpg')}}" alt="">
                        <p>Sed egestas, ante et vulputate volutpat, eros pede semper est.</p>
                    </div>
                </div>
                <div class="col-md-6 justify-content-between">
                    @foreach($news as $new)
                        <div class="right-side-news">
                            <div class="inner-img-event">
                                <img src="{{asset('/uploads/news_image/'.$new-> news_image)}}" alt="">
                                <p class="p-2 m-2">{{$new-> news_title}}</p>
                            </div>
                            @endforeach
                        </div>
                </div>
            </div>
        </div>
    </div>
    <!-- close-news-event-sec -->
</div>

    <!-- start-testimonials-sec -->
    <div class="testimonails-sec padding-sec mh-100">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-8">
                    <div id="carouselExampleInterval" class="carousel slide" data-bs-ride="carousel">
                        <div class="carousel-inner">
                            <div class="carousel-item active" data-bs-interval="10000">
                                <div class="client-content">
                                    <i class="fas fa-quote-left"></i>
                                    <div class="tstimonial-text">
                                        <p>Sed egestas, ante et vulputate volutpat, eros pede semper est, vitae luctus metus libero eu augue. Morbi purus libero, faucibus adipiscing, commodo quis, gravida id, est. Sed lectus. Praesent elementum hendrerit tortor. Sed semper lorem at felis. Vestibulum volutpat.</p>
                                        <p>Sed egestas, ante et vulputate volutpat, eros pede semper est, vitae luctus metus libero eu augue. Morbi purus libero, faucibus adipiscing, commodo quis, gravida id, est. Sed lectus.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="carousel-item" data-bs-interval="2000">
                                <div class="client-content">
                                    <i class="fas fa-quote-left"></i>
                                    <div class="tstimonial-text">
                                        <p>Sed egestas, ante et vulputate volutpat, eros pede semper est, vitae luctus metus libero eu augue. Morbi purus libero, faucibus adipiscing, commodo quis, gravida id, est. Sed lectus. Praesent elementum hendrerit tortor. Sed semper lorem at felis. Vestibulum volutpat.</p>
                                        <p>Sed egestas, ante et vulputate volutpat, eros pede semper est, vitae luctus metus libero eu augue. Morbi purus libero, faucibus adipiscing, commodo quis, gravida id, est. Sed lectus.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="carousel-item">
                                <div class="client-content">
                                    <i class="fas fa-quote-left"></i>
                                    <div class="tstimonial-text">
                                        <p>Sed egestas, ante et vulputate volutpat, eros pede semper est, vitae luctus metus libero eu augue. Morbi purus libero, faucibus adipiscing, commodo quis, gravida id, est. Sed lectus. Praesent elementum hendrerit tortor. Sed semper lorem at felis. Vestibulum volutpat.</p>
                                        <p>Sed egestas, ante et vulputate volutpat, eros pede semper est, vitae luctus metus libero eu augue. Morbi purus libero, faucibus adipiscing, commodo quis, gravida id, est. Sed lectus.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <a class="carousel-control-prev" href="#carouselExampleInterval" role="button" data-bs-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="visually-hidden">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carouselExampleInterval" role="button" data-bs-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="visually-hidden">Next</span>
                        </a>
                    </div>
                </div>
                <div class="col-lg-4">
                    <img src="{{asset('/frontend/image/client-testimonial-1.png')}}" alt="">
                </div>
            </div>
        </div>
    </div>

    <!-- BEGIN Work in Progress-->
    <div class="upcoming-project-sec padding-sec bg-white mt-10">
        <h3 class="text-center" style=" font-style: normal;font-weight: bold;font-size: 40px;line-height: 60px;color: #1C416D;margin-bottom: 30px;">{{__("Work In Progress")}}</h3>
        <div id="carouselExampleCaptions" class="carousel slide" data-bs-ride="carousel">
            <div class="carousel-indicators">
                <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
                <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="1" aria-label="Slide 2"></button>
            </div>
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <img src="{{asset('images/car.png')}}" class="d-block w-100 h-25" alt="...">
                    <div class="carousel-caption d-none d-md-block">
                        <h5>First slide label</h5>
                        <p>Some representative placeholder content for the first slide.</p>
                    </div>
                </div>
                <div class="carousel-item">
                    <img src="{{asset('images/car.png')}}" class="d-block w-100 h-25" alt="...">
                    <div class="carousel-caption d-none d-md-block">
                        <h5>Second slide label</h5>
                        <p>Some representative placeholder content for the second slide.</p>
                    </div>
                </div>
            </div>
            <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Previous</span>
            </button>
            <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Next</span>
            </button>
        </div>
    </div>
    <!-- END Work in Progress-->
@stop
