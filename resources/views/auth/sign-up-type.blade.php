<!doctype html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Bootstrap CSS -->
  <link href="/frontend/css/bootstrap.min.css" rel="stylesheet">
  <link rel="stylesheet" href="/frontend/css/marquee.css" />
  <link href="/frontend/fonts/popins.css" rel="stylesheet">
  <link href="/frontend/css/style.css" rel="stylesheet">
  <link href="/frontend/css/style-two.css" rel="stylesheet">
  <link href="/frontend/css/responsive.css" rel="stylesheet">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css">
  <title>Kogas</title>
</head>

<body>

<!-- start-registration-sec -->

<div class="registration-sec">
  <div class="container-fluid">
      <div class="row bg-white">
          <div class="col-md-4 text-left"><img src="frontend/image/logo.png" alt=""></div>
          <div class="col-md-4 text-center"><img src="frontend/image/logo-2.png"></div>
          <div class="col-md-4" style="text-align: end"><img src="frontend/image/logo-3.png" alt=""></div>
      </div>
    <div class="register-form">
      <h3 class="heading">Register As</h3>
      <p class="heading">Please choose a user category to register</p>
      <div class="container">
        <div class="row register-citizen justify-content-center">
        <div class="col-md-5">
          <div class="citizen-user-sec active">
            <div class="citizen-user-inner">
              <img class="dotcs-none" src="/frontend/image/user-check.png">
              <img class="check" src="/frontend/image/user-check-active.png">
            <h3>Citizen</h3>
            <p>The general citizenry under the authority of the Kogi State Government.</p>
            </div>
            <div class="dot-citizen-btn">
              <a href="{{url('/register')}}"><img  class="dotcs-none" src="/frontend/image/dot-citizen.png"><img class="check" src="/frontend/image/dot-citizen-check.png"></a>
            </div>
          </div>
        </div>


        <div class="col-md-5 col-md-offset-2">
          <div class="citizen-user-sec">
            <div class="citizen-user-inner">
              <img class="dotcs-none"  src="/frontend/image/user-check.png">
              <img class="check" src="/frontend/image/user-check-active.png">
            <h3>Officials</h3>
            <p>The government officials under the authority of the Kogi State Government</p>
            </div>
            <div class="dot-citizen-btn">
              <a href="javascript:void(0)"><img class="dotcs-none" src="/frontend/image/dot-citizen.png"><img class="check" src="/frontend/image/dot-citizen-check.png"></a>
            </div>
          </div>
        </div>
      </div>
      </div>
    </div>
  </div>
</div>

<!-- close-registration-sec -->
  <!-- Option 1: Bootstrap Bundle with Popper -->
  <script src="/frontend/js/bootstrap.bundle.min.js"></script>
  <script src="/frontend/js/jquery.min.js"></script>
  <script src="/frontend/js/marquee.js"></script>
  <script src="/frontend/js/custom-script.js"></script>
  <script>
   $('body').on('click', '.citizen-user-sec', function() {
      $('.citizen-user-sec.active').removeClass('active');
      $(this).addClass('active');
  });
  </script>
</body>
<footer>
    <div class="footer-register-sec">
        <p class="mt-3">{{__("COPYRIGHT 2021, ALL RIGHT RESERVED | KOGI STATE GOVERNMENT")}}</p>
    </div>
</footer>
</html>
