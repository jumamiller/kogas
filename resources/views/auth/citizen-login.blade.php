<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Bootstrap CSS -->
    <link href="/frontend/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="/frontend/css/marquee.css" />
    <link href="/frontend/fonts/popins.css" rel="stylesheet">
    <link href="/frontend/css/style.css" rel="stylesheet">
    <link href="/frontend/css/style-two.css" rel="stylesheet">
    <link href="/frontend/css/responsive.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css">
    <title>Kogas</title>
    @toastr_css
</head>

<body>

<!-- start-registration-sec -->

<div class="registration-sec">
    <div class="container-fluid">
        <div class="row bg-white">
            <div class="col-md-4 text-left"><img src="frontend/image/logo.png" alt=""></div>
            <div class="col-md-4 text-center"><img src="frontend/image/logo-2.png"></div>
            <div class="col-md-4" style="text-align: end"><img src="frontend/image/logo-3.png" alt=""></div>
        </div>
        <div class="register-user-form">
            <div class="register-inner-sec">
                <h6 class="font-weight-bold text-center text-white">{{__("Citizen Login")}}</h6>
                <form action="{{route('user_login')}}" autocomplete="off" method="POST">
                    @csrf
                    <div class="inputBox user-icon-req">
                        <label style="font-size: 16px;margin-bottom: 0">{{__("Username")}}</label>
                        <input type="text" id="email" class="form-control" name="email" required>
                        <img src="frontend/image/user-icn.png" alt="">
                        @if ($errors->has('email'))
                            <span class="text-warning">{{ $errors->first('email') }}</span>
                        @endif
                    </div>
                    <div class="inputBox user-icon-req form-group">
                        <label style="font-size: 16px;margin-bottom: 0">{{__("Password")}}</label>
                        <input type="password" id="password" name="password" class="form-control" required>
                        <img src="frontend/image/lock-icn.png" alt="">
                        @if ($errors->has('password'))
                            <span class="text-warning">{{ $errors->first('password') }}</span>
                        @endif
                    </div>
                    <div class="submit-btns">
                        <input type="submit" name="" class="button-two" id="login_button" value="Sign In">
                        <a href="{{url('/register') }}">Create an account</a>
                    </div>
                    <div class="resert-pass-btn">
                        <a href="#">Reset Password</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- close-registration-sec -->



<!-- Option 1: Bootstrap Bundle with Popper -->
<script src="/frontend/js/bootstrap.bundle.min.js"></script>
<script src="/frontend/js/jquery.min.js"></script>
<script src="/frontend/js/marquee.js"></script>
<script src="/frontend/js/custom-script.js"></script>
</body>
<footer class="footer-register-sec">
    <p class="mt-2">COPYRIGHT 2021, ALL RIGHT RESERVED | KOGI STATE GOVERNMENT</p>
    @jquery
    @toastr_js
    @toastr_render
</footer>
</html>
