<!doctype html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Bootstrap CSS -->
  <link href="/frontend/css/bootstrap.min.css" rel="stylesheet">
  <link rel="stylesheet" href="/frontend/css/marquee.css" />
  <link href="/frontend/fonts/popins.css" rel="stylesheet">
  <link href="/frontend/css/style.css" rel="stylesheet">
  <link href="/frontend/css/style-two.css" rel="stylesheet">
  <link href="/frontend/css/responsive.css" rel="stylesheet">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css">
  <title>Kogas</title>
    <style>
        label{
            font-weight: lighter;
        }
    </style>
    @toastr_css
</head>

<body>

<!-- start-registration-sec -->

<div class="registration-sec">
  <div class="container-fluid">
      <div class="row bg-white">
          <div class="col-md-4 text-left"><img src="frontend/image/logo.png" alt=""></div>
          <div class="col-md-4 text-center"><img src="frontend/image/logo-2.png"></div>
          <div class="col-md-4" style="text-align: end"><img src="frontend/image/logo-3.png" alt=""></div>
      </div>

    <div class="register-user-form">
        @if(session()->get('errors'))
            toastr.error("{{ session()->get('errors')->first() }}");
        @endif
      <div class="register-inner-sec">
          <h6 class="font-weight-bold text-center text-white">{{__("Register")}}</h6>
        <form action="{{route('save_user')}}" method="POST">
            @csrf
            <div class="form-group">
                <label class="font-weight-light" style="font-size: 16px;margin-bottom: 0">Full Name<span class="text-warning">*</span></label>
                <input type="text" class="form-control" id="name" name="name" placeholder="" style="padding: 0;margin: 0" required>
            </div>
            <div class="form-group">
                <label  style="font-size: 16px;margin-bottom: 0">Town/City<span class="text-warning">*</span></label>
                <input type="text" class="form-control" id="town" name="town" placeholder="" style="padding: 0;margin: 0" required>
            </div>

            <div class="form-group">
                <label style="font-size: 16px;margin-bottom: 0">Phone Number<span class="text-warning">*</span></label>
                <input type="number"  class="form-control"id="phone" name="phone" placeholder="" style="padding: 0;margin: 0" required>
            </div>
            <p>

            <div class="form-group">
                <label style="font-size: 16px;margin-bottom: 0">Email Address <span class="text-warning">*</span></label>
                <input type="email" class="form-control" id="email" name="email" placeholder="" style="padding: 0;margin: 0" required>
                @if ($errors->has('email'))
                    <span class="text-warning">{{ $errors->first('email') }}</span>
                @endif
            </div>
            <p>
            <div class="form-group">
                <label style="font-size: 16px;margin-bottom: 0">Password<span class="text-warning">*</span></label>
                <input type="password" class="form-control" id="password" name="password" autocomplete="off" style="padding: 0;margin: 0" required>
            </div>
            <div class="check-site mt-4">
                <input type="checkbox" class="input_class_checkbox-two" style="display: none;" required><div class="class_checkbox_two"></div>
                <label class="governing-check-box" for="test1">{{__("I agree with the Kogas terms and conditions.")}}</label>
            </div>
            <div class="submit-btns mt-4">
                <input type="submit" name="" class="button-two" value="Register" id="register_button">
                <a href="{{url('/login')}}">{{__("Login Here")}}</a>
            </div>
        </form>
      </div>
    </div>
  </div>
</div>
</body>
<footer>
    <div class="footer-register-sec">
        <p>{{__("COPYRIGHT 2021 ALL RIGHT RESERVED | KOGI STATE GOVERNMENT")}}</p>
    </div>
</footer>

<!-- close-registration-sec -->

<!-- Option 1: Bootstrap Bundle with Popper -->
<script src="/frontend/js/bootstrap.bundle.min.js"></script>
<script src="/frontend/js/jquery.min.js"></script>
<script src="/frontend/js/marquee.js"></script>
<script src="/frontend/js/custom-script.js"></script>
@jquery
@toastr_js
@toastr_render
</html>
