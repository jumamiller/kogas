<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Bootstrap CSS -->
    <link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('fonts/popins.css')}}" rel="stylesheet">
    <link href="{{asset('css/style.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/css/dropify.css" />
    <title>Kogas</title>
</head>

<body>
<div class="kogas-enrolment-head">
    <div class="container-fluid">
        <div class="row justify-content-between">
            <div class="col-md-6">
                <div class="head-text-col">
                    <img src="{{asset('images/logo.png')}}"alt="">
                </div>
            </div>
            <div class="col-md-6 ">&nbsp;
                <h3 class="text-white text-uppercase">Welcome To KOGAS OFFICIALS PORTAL</h3>
            </div>
        </div>
    </div>
</div>
<!-- start-contact-form -->
<div class="contact-form padding-sec">
    <div class="container-fluid">
        <div class="formBox">

            <form method="post" action="{{route('login')}}" enctype="multipart/form-data">
                @csrf
                <div class="heading">
                    <h1>Sign In</h1>
                </div>

                <div class="row">
                    <div class="col-sm-12 col-lg-12">
                        <div class="inputBox ">
                            <label>Email address<span>*</span></label>
                            <input type="email" id="email_address" name="email_address" required>
                        </div>
                        <div class="inputBox">
                            <label>Password<span>*</span></label>
                            <input type="password" id="password" name="password" required>
                        </div>
                    </div>

                </div>

                <div class="row">
                    <div class="col-sm-12">
                        <input type="submit" name="" class="button-two" value="Login">
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- close-contact-form -->
<div class="footer">
    <div class="container-fluid">
        <p class="text-center">COPYRIGHT 2021 ALL RIGHTS RESERVED <span class="d-sm-inline d-block"><span class="d-sm-inline d-none">| </span>KOGI OPEN GOVERNANCE AND ACCOUNTABILITY SYSTEM</span> </p>
    </div>
</div>
<!-- Option 1: Bootstrap Bundle with Popper -->
<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script src="{{asset('js/bootstrap.bundle.min.js')}}"></script>
{{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script> --}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/js/dropify.min.js"></script>
</body>
</html>
