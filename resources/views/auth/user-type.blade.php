<!doctype html>
<html lang="en">
<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Bootstrap CSS -->
  <link href="/frontend/css/bootstrap.min.css" rel="stylesheet">
  <link rel="stylesheet" href="/frontend/css/marquee.css" />
  <link href="/frontend/fonts/popins.css" rel="stylesheet">
  <link href="/frontend/css/style.css" rel="stylesheet">
  <link href="/frontend/css/style-two.css" rel="stylesheet">
  <link href="/frontend/css/responsive.css" rel="stylesheet">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css">
  <title>{{__("Kogas Authentication")}}</title>
</head>

<body class="registration-sec">
<div class="container-fluid">
    <div class="row bg-white">
        <div class="col-md-4 text-left"><img src="frontend/image/logo.png" alt=""></div>
        <div class="col-md-4 text-center"><img src="frontend/image/logo-2.png"></div>
        <div class="col-md-4" style="text-align: end"><img src="frontend/image/logo-3.png" alt=""></div>
    </div>
    <div class="register-form">
        <h3 class="heading">{{__("Sign in As")}}</h3>
        <p class="heading">{{ trans('auth.please_select_your_user_category_to_sign_in') }}</p>
        <div class="container">
            <div class="row register-citizen justify-content-center">
                <div class="col-md-5">
                    <div class="citizen-user-sec active">
                        <div class="citizen-user-inner">
                            <img class="dotcs-none" src="/frontend/image/user-check.png" alt="">
                            <img class="check" src="/frontend/image/user-check-active.png" alt="">
                            <h3>{{__("Citizen")}}</h3>
                            <p>{{__("The general citizenry under the authority of the Kogi State Government.")}}</p>
                        </div>
                        <div class="dot-citizen-btn">
                            <a href="{{url('/citizen-login')}}"><img  class="dotcs-none" src="/frontend/image/dot-citizen.png" alt="">
                                <img class="check" src="/frontend/image/dot-citizen-check.png" alt=""></a>
                        </div>
                    </div>
                </div>

                <div class="col-md-5">
                    <div class="citizen-user-sec">
                        <div class="citizen-user-inner">
                            <img class="dotcs-none"  src="/frontend/image/user-check.png" alt="">
                            <img class="check" src="/frontend/image/user-check-active.png" alt="">
                            <h3 class="">Officials</h3>
                            <p>The government officials under the authority of the Kogi State Government</p>
                        </div>
                        <div class="dot-citizen-btn">
                            <a href="/login"><img class="dotcs-none" src="{{asset('/frontend/image/dot-citizen.png')}}" alt="">
                                <img class="check" src="/frontend/image/dot-citizen-check.png" alt=""></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- close-registration-sec -->
  <!-- Option 1: Bootstrap Bundle with Popper -->
  <script src="/frontend/js/bootstrap.bundle.min.js"></script>
  <script src="/frontend/js/jquery.min.js"></script>
  <script src="/frontend/js/marquee.js"></script>
  <script src="/frontend/js/custom-script.js"></script>

  <script>
   $('body').on('click', '.citizen-user-sec', function() {
      $('.citizen-user-sec.active').removeClass('active');
      $(this).addClass('active');
  });
  </script>
</body>
<footer class="justify-content-center">
    <div class="footer-register-sec">
        <p class="text-center mt-3">{{__("COPYRIGHT 2021 ALL RIGHT RESERVED | KOGI STATE GOVERNMENT")}}</p>
    </div>
</footer>
</html>
