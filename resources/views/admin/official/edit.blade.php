@extends('backend.master')
@section('content')
<div class="content-wrapper text-sm mt-3" style="font-family: Nunito;">
  <div class="col-md-12">
    <div class="card card-primary">
      <div class="card-header">
        <h3 class="card-title">Add Official</h3>
      </div>
      <form action="{{route('add.official')}}" method="post" enctype="multipart/form-data">
        @csrf
        <div class="card-body">
          <div class="form-group">
            <label for="exampleInputEmail1">Name</label>
            <input type="text" name="name" class="form-control" placeholder="">
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1">Email</label>
            <input type="email" name="email" class="form-control" placeholder="">
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1">Password</label>
            <input type="password" name="password" class="form-control" placeholder="">
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1">Mobile</label>
            <input type="text" name="mobile" class="form-control" placeholder="">
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1">Address</label>
            <textarea type="text" name="address" rows="4" class="form-control" placeholder=""></textarea>
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1">City</label>
            <input type="text" name="city" class="form-control" placeholder="">
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1">Pincode</label>
            <input type="text" name="pincode" class="form-control" placeholder="">
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1">Facebook</label>
            <input type="text" name="facebook" class="form-control" placeholder="">
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1">Twitter</label>
            <input type="text" name="twitter" class="form-control" placeholder="">
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1">Instagram</label>
            <input type="text" name="instagram" class="form-control" placeholder="">
          </div>
          <div class="form-group">
            <label for="exampleInputFile">Official's Image</label>
            <div class="input-group">
              <input type="file" name="profile_image" class="dropify" data-height="300" id="image1">
            </div>
          </div>
          <div class="card-body">
          <label class="block-title mb-2 mt-2">Permissions</label>
          <div class="form-check">
            <label class="row" style="margin-left: -18px;">Users</label>
            <div class="row">
              <div class="col-md-3">
                <input type="checkbox" name="viewuser" value="view user" class="form-check-input" id="">
                <label class="form-check-label" for="exampleCheck1">View</label>
              </div>
              <div class="col-md-3">
                <input type="checkbox" name="adduser" value="add user" class="form-check-input" id="">
                <label class="form-check-label" for="exampleCheck1">Add</label>
              </div>
              <div class="col-md-3">
                <input type="checkbox" name="edituser" value="edit user" class="form-check-input" id="">
                <label class="form-check-label" for="exampleCheck1">Edit</label>
              </div>
              <div class="col-md-3">
                <input type="checkbox" name="deleteuser" value="delete user" class="form-check-input" id="">
                <label class="form-check-label" for="exampleCheck1">Delete</label>
              </div>
            </div>
          </div>
          <div class="form-check">
            <label class="row" style="margin-left: -18px;padding-top: 10px;">News</label>
            <div class="row">
              <div class="col-md-3">
                <input type="checkbox" name="viewnews" value="view news" class="form-check-input" id="">
                <label class="form-check-label" for="exampleCheck1">View</label>
              </div>
              <div class="col-md-3">
                <input type="checkbox" name="addnews" value="add news" class="form-check-input" id="">
                <label class="form-check-label" for="exampleCheck1">Add</label>
              </div>
              <div class="col-md-3">
                <input type="checkbox" name="editnews" value="edit news" class="form-check-input" id="">
                <label class="form-check-label" for="exampleCheck1">Edit</label>
              </div>
              <div class="col-md-3">
                <input type="checkbox" name="deletenews" value="delete news" class="form-check-input" id="">
                <label class="form-check-label" for="exampleCheck1">Delete</label>
              </div>
            </div>
          </div>
          <div class="form-check">
            <label class="row" style="margin-left: -18px;padding-top: 10px;">Topic</label>
            <div class="row">
              <div class="col-md-3">
                <input type="checkbox" name="viewtopic" value="view topic" class="form-check-input" id="">
                <label class="form-check-label" for="exampleCheck1">View</label>
              </div>
              <div class="col-md-3">
                <input type="checkbox" name="addtopic" value="add topic" class="form-check-input" id="">
                <label class="form-check-label" for="exampleCheck1">Add</label>
              </div>
              <div class="col-md-3">
                <input type="checkbox" name="edittopic" value="edit topic" class="form-check-input" id="">
                <label class="form-check-label" for="exampleCheck1">Edit</label>
              </div>
              <div class="col-md-3">
                <input type="checkbox" name="deletetopic" value="delete topic" class="form-check-input" id="">
                <label class="form-check-label" for="exampleCheck1">Delete</label>
              </div>
            </div>
          </div>
          <div class="form-check">
            <label class="row" style="margin-left: -18px; padding-top: 10px;">Suggestions</label>
            <div class="row">
              <div class="col-md-3">
                <input type="checkbox" class="form-check-input" id="">
                <label class="form-check-label" for="exampleCheck1">View</label>
              </div>
              <div class="col-md-3">
                <input type="checkbox" class="form-check-input" id="">
                <label class="form-check-label">Approve</label>
              </div>
              <div class="col-md-3">
                <input type="checkbox" class="form-check-input" id="">
                <label class="form-check-label" for="exampleCheck1">Reject</label>
              </div>
            </div>
          </div>
          <div class="form-check">
            <label class="row" style="margin-left: -18px; padding-top: 10px;">Compaints</label>
            <div class="row">
              <div class="col-md-3">
                <input type="checkbox" name="viewcomplain" value="view complain" class="form-check-input" id="">
                <label class="form-check-label" for="exampleCheck1">View</label>
              </div>
              <div class="col-md-3">
                <input type="checkbox" name="approvecomplain" value="approve complain" class="form-check-input" id="">
                <label class="form-check-label">Approve</label>
              </div>
              <div class="col-md-3">
                <input type="checkbox" name="rejectcomplain" value="reject complain" class="form-check-input" id="">
                <label class="form-check-label" for="exampleCheck1">Reject</label>
              </div>
            </div>
          </div>
          <div class="form-check">
            <label class="row" style="margin-left: -18px; padding-top: 10px;">Appointments</label>
            <div class="row">
              <div class="col-md-3">
                <input type="checkbox" name="viewappointment" value="view appointment" class="form-check-input" id="">
                <label class="form-check-label" for="exampleCheck1">View</label>
              </div>
              <div class="col-md-3">
                <input type="checkbox" name="approveappointment" value="approve appointment" class="form-check-input" id="">
                <label class="form-check-label">Approve</label>
              </div>
              <div class="col-md-3">
                <input type="checkbox" name="rejectappointment" value="reject appointment" class="form-check-input" id="">
                <label class="form-check-label" for="exampleCheck1">Reject</label>
              </div>
              
            </div>
          </div>
        </div>
          
        </div>
        
        <!-- /.card-body -->
        <div class="card-footer">
          <button type="submit" class="btn btn-primary btn-block">Submit</button>
        </div>
      </form>
    </div>
  </div>
 
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$('#image1').dropify();
	});
</script>
@stop