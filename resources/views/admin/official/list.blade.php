@extends('backend.master')
@section('content')

<div class="content-wrapper text-sm mt-3" style="font-family: Nunito;">
	<div class="col-md-12">
		<div class="card-header">
	      <h3 class="card-title">Officials</h3>
	    </div>
	    <table class="table table-bordered table-hover" id="officialTable">
	    	<thead>
	    		<tr>
	    			{{-- <th>Name</th>
	    			<th>Mobile</th>
	    			<th>Email</th>
	    			<th>Alloted To</th>
	    			<th>Action</th> --}}
	    		</tr>
	    	</thead>
	    </table>
	</div>
	@include('admin.script.official-list-script')
</div>

@stop