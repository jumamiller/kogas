@extends('backend.master')
@section('content')
<div class="content-wrapper text-sm mt-3" style="font-family: Nunito;">
  <div class="col-md-12">
    <div class="card card-primary">
      <div class="card-header">
        <h3 class="card-title">Add News</h3>
      </div>
      <form action="{{route('add.news')}}" method="post" enctype="multipart/form-data">
        @csrf
        <div class="card-body">
          <div class="form-group">
            <label for="exampleInputEmail1">News Title</label>
            <input type="text" name="news_title" class="form-control" placeholder="Enter Name">
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1">News Body</label>
            <textarea type="text" name="news_body" rows="4" class="form-control" placeholder="Enter Name"></textarea>
          </div>
          
          <div class="form-group">
            <label for="exampleInputFile">News Image</label>
            <div class="input-group">
              <input type="file" name="news_image" class="dropify" data-height="300" id="image1">
            </div>
          </div>
          
        </div>
        
        <!-- /.card-body -->
        <div class="card-footer">
          <button type="submit" class="btn btn-primary btn-block">Submit</button>
        </div>
      </form>
    </div>
  </div>
 
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$('#image1').dropify();
	});
</script>
@stop