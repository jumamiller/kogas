@extends('backend.master')

@section('content')

<div class="content-wrapper text-sm mt-3" style="font-family: Nunito;">
 <div class="col-md-12">
<table class="table table-bordered table-hover" id="newsTable">
	<thead>
		<tr>
		</tr>
	</thead>
</table>
</div>
</div>
<div class="modal fade" id="newsDelete" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Confirm</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Are You Sure ?</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" id="confirmYes">Yes</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
  var url = {!! json_encode(url('/')) !!};
  function confirmDelete(event,id)
  {
    event.preventDefault();
    $('#newsDelete').modal('show');
    $('#confirmYes').click(function(){
        $.ajax({
          type:'GET',
          url: '{{url('admin/delete-news')}}' + '/' + id,
          success: function(data){
            location.reload();
          }
        });
    });
  }
  $(document).ready(function(){
    var newsTable = $('#newsTable').DataTable({
      "paging": true,
          "autoWidth": false,
          searching: true,
          "order": [[ 1, "desc" ]],
          "columnDefs": [
              { "orderable": false, "targets": [2] },
          ],
          "bAutoWidth": false ,
          "processing": true,
          "serverSide": true,
          "ajax": "{{route('table.news')}}",
          "rowCallback": function(row, data, index){
            var htmlStr = '<td><img src="'+url+'/public/uploads/news_image/'+data['news_image']+'" class="rounded-circle" height="35" width="35"></td><td>'+data['news_title']+'</td><td><a href="'+ '{{url('admin/edit-news')}}' +"/"+data['id']+'" class="btn btn-info btn-sm mr-1"><i class="far fa-edit"></i></a><a href="" onclick="confirmDelete(event,\''+data['id']+'\')" class="btn btn-danger btn-sm"><i class="fa fa-trash" aria-hidden="true"></i></a></td>';
            $(row).html(htmlStr);
          },
          columns:[
            { title: "News image", data: "news_image" },
            { title: "News Title", data: "news_title" },
            { title: "Action", data: "id" },
          ]
    })
  });

</script>
@stop