@extends('backend.master')

@section('content')

<div class="content-wrapper text-sm mt-3" style="font-family: Nunito;">
 <div class="col-md-12">
<table class="table table-bordered table-hover" id="userTable">
	<thead>
		<tr>
			{{-- <th>Name</th>
			<th>Mobile</th>
			<th>Role</th>
			<th>Action</th> --}}
		</tr>
	</thead>
</table>
</div>
</div>
<div class="modal fade" id="userDelete" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Confirm</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Are You Sure ?</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" id="confirmYes">Yes</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
	$(document).ready(function(){

		// User Table Start
		var userTable = $('#userTable').DataTable({
			"paging": true,
        	"autoWidth": false,
        	searching: true,
        	"order": [[ 1, "desc" ]],
        	"columnDefs": [
            	{ "orderable": false, "targets": [4] },
        	],
        	"bAutoWidth": false ,
        	"processing": true,
        	"serverSide": true,
        	"ajax": "{{route('table.user')}}",
        	"rowCallback": function(row, data, index){

        		var htmlStr = '<td>'+data['name']+'</td><td>'+data['email']+'</td><td>'+data['mobile']+'</td><td>'+data['role']+'</td><td><a href="'+ '{{url('admin/edit-user')}}' +"/"+data['id']+'" class="btn btn-info btn-sm mr-1"><i class="far fa-edit"></i></a><a href="" onclick="confirmDelete(event,\''+data['id']+'\')" class="btn btn-danger btn-sm"><i class="fa fa-trash" aria-hidden="true"></i></a></td>'
        		$(row).html(htmlStr);
        	},
        	columns:[
        		{ title: "Name", data: "name" },
        		{ title: "Email", data: "email" },
        		{ title: "Mobile", data: "mobile" },
        		{ title: "Role", data: "role" },
        		{ title: "Action", data: "id" },
        	]
		});

	});

	function confirmDelete(event, id)
		{
			event.preventDefault();
			$('#userDelete').modal('show');
			$('#confirmYes').click(function(e){
				$.ajax({
					type: 'GET',
					url: '{{url('/admin/delete-user')}}' +'/'+ id,
					success: function(data)
					{
						location.reload();
					}
				})
			});
		}
</script>
@stop
