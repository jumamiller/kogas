@extends('backend.master')
<style type="text/css">
    .ck.ck-editor__main>.ck-editor__editable {
    border-color: var(--ck-color-base-border);
    min-height: 400px;
}
</style>
@section('content')
@php
	$count = count($governer);
    $msg = '';
    if($gov_msg != null){
        $msg = base64_decode($gov_msg-> variable_value);    
    }
    
@endphp
<div class="content-wrapper text-sm mt-3" style="font-family: Nunito;">
    <div class="col-md-12">
    	@if($count > 0)

        <div class="col-md-12 mb-4">
    		<div class="row mt-4">
                <div class="col-md-3">
    				<label class="mr-3">Governer's Name</label>
                </div>
                <div class="col-md-8">
                    <p>{{$governer[0]-> name}}</p>
                </div>
                <div class="col-md-1 float-right">
                    <a href="{{route('remove.governer', $governer[0]-> id)}}" class="btn btn-danger btn-sm">Remove</a>
                </div>
    		</div>
            <div class="row">
                <div class="col-md-12">
                <form method="post" action="{{route('message.governer')}}">
                    @csrf
                    <textarea class="ckeeditor" value="{{$msg}}" name="gov_msg">{{$msg}}</textarea>
                    <input type="hidden" name="gov_id" value="{{$governer[0]-> id}}">
                    <button type="submit" class="btn btn-info btn-sm mt-3">Add/Upadate</button>
                </form>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                <form method="post" action="{{route('message.governer')}}">
                    @csrf
                    <table class="table table-bordered table-hover" id="officialTable">
                        <thead>
                            <tr>
                            </tr>
                        </thead>
                        <tbody>
                            
                        </tbody>
                    </table>
                </form>
                </div>
            </div>
            {{-- <button type="submit" class="btn btn-info btn-sm mt-3">Add/Upadate</button> --}}
        </div>
        @include('admin.script.allofficial-list-script')
    	@else
	    <div class="card-header">
	      <h3 class="card-title">Governer</h3>
	    </div>
        <table class="table table-bordered table-hover" id="govtTable">
        	<thead>
        		<tr>
        			<th>Name</th>
        			<th>Mobile</th>
        			<th>Role</th>
        			<th>Action</th>
        		</tr>
        	</thead>
        </table>
    </div>
    @endif
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$('#govtTable').DataTable({
			"paging": true,
        	"autoWidth": false,
        	searching: true,
        	"order": [[ 1, "desc" ]],
        	"columnDefs": [
            	{ "orderable": false, "targets": [] },
        	],
        	"bAutoWidth": false ,
        	"processing": true,
        	"serverSide": true,
        	"ajax": "{{route('gov.table')}}",
        	"rowCallback": function(row, data, index){
        		console.log(data);
        		var tempData = data;
            	var data = [];

            	data[0] = tempData['id'];
            	data[1] = tempData['name'];
            	data[2] = tempData['mobile'];
            	data[3] = tempData['role'];
            	

            	var htmlStr = '<td>'+data[1]+'</td><td>'+data[2]+'</td><td>'+data[3]+'</td><td><a href="'+ '{{url('admin/make-governer')}}' +"/"+data[0]+'" class="btn btn-info btn-sm">Make Governer</a></td>';
            	$(row).html(htmlStr);
        	},

        	columns:[
        		{ title: "Name", data: "name" },
        		{ title: "Mobile", data: "mobile" },
        		{ title: "Role", data: "role" },
        		{ title: "Action", data: "id" },
        	]

		});

        ClassicEditor.create(
        document.querySelector( '.ckeeditor' ) 
        );
	});
</script>
@stop