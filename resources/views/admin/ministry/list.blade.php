@extends('backend.master')
@section('content')

    <div class="content-wrapper text-sm mt-3" style="font-family: Nunito;">
        <div class="col-md-12">
            <table class="table table-bordered table-hover" id="minstry_table">
                <thead>
                <tr>
                </tr>
                </thead>
            </table>
        </div>
    </div>

    <div class="modal fade" id="projectDelete" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Confirm</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p>Are You Sure ?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="confirmYes">Yes</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        var url = {!! json_encode(url('/')) !!};
        function confirmDelete(event,id)
        {
            event.preventDefault();
            $('#projectDelete').modal('show');
            $('#confirmYes').click(function(){
                $.ajax({
                    type:'GET',
                    url: '{{url('admin/delete-project')}}' + '/' + id,
                    success: function(data){
                        location.reload();
                    }
                });
            });
        }
        $(document).ready(function(){
            var newsTable = $('#minstry_table').DataTable({
                "paging": true,
                "autoWidth": false,
                searching: true,
                "order": [[ 1, "desc" ]],
                "columnDefs": [
                    { "orderable": false, "targets": [4] },
                ],
                "bAutoWidth": false ,
                "processing": true,
                "serverSide": true,
                "ajax": "{{route('table.ministry')}}",
                "rowCallback": function(row, data, index){
                    console.log(data);
                    var htmlStr = '<td><img src="'+url+'/public/uploads/project_image/thumb_image/'+data['ministry_image']+'" class="rounded-circle" height="35" width="35"></td><td>'+data['name']+'</td><td>'+'</td><td><a href="'+ '{{url('admin/edit-project')}}' +"/"+data['id']+'" class="btn btn-info btn-sm mr-1"><i class="far fa-edit"></i></a><a href="" onclick="confirmDelete(event,\''+data['id']+'\')" class="btn btn-danger btn-sm"><i class="fa fa-trash" aria-hidden="true"></i></a></td>';
                    $(row).html(htmlStr);
                },
                columns:[
                    { title: "Ministry", data: "ministry_image" },
                    { title: "Name", data: "name" },
                    { title: "Action", data: "id" },
                ]
            })
        });

    </script>

@stop
