@extends('backend.master')
@section('content')
<div class="content-wrapper text-sm mt-3" style="font-family: Nunito;">
  <div class="col-md-12">
    <div class="card card-primary">
      <div class="card-header">
        <h3 class="card-title">Add Ministry</h3>
      </div>
      <form action="{{route('add.ministry')}}" method="post" enctype="multipart/form-data">
        @csrf
        <div class="card-body">
          <div class="form-group">
            <label for="exampleInputEmail1">Ministry Name</label>
            <input type="text" name="name" class="form-control" placeholder="">
          </div>
          
          <div class="form-group">
            <label for="exampleInputFile">Ministry Image</label>
            <div class="input-group">
              <input type="file" name="ministry_image" class="dropify" data-height="300" id="image1">
            </div>
          </div>
          
        </div>
        
        <!-- /.card-body -->
        <div class="card-footer">
          <button type="submit" class="btn btn-primary btn-block">Submit</button>
        </div>
      </form>
    </div>
  </div>
 
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$('#image1').dropify();
	});
</script>
@stop