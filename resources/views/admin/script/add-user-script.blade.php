<script type="text/javascript">
	$(document).ready(function(){
		$('#image1').dropify();

		var role = $('#role').val();

		if (role == 'Official') {
			$('#permission').show();
		}

		$('#role').change(function(e){

			var r = $('#role').val();
			console.log(r);
			if (r == 'Official') {
				$('#permission').show();
			} else {
				console.log('hide');
				$('#permission').hide();
			}
		})
	})
</script>