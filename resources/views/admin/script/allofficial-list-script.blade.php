<script type="text/javascript">
    function officialIdChange(e, id){
            if(e.checked == true)
            {
                insertToArray(id);
            } else {
                removeFromArray(id);
            }
    }
    var selectedIds = [];
    function insertToArray(id)
    {
        if(!isIdAlreadyAdded(selectedIds, id)){
            selectedIds.push(id);
        } else {
            removeIfExits(selectedIds, id)
        }
        console.log(selectedIds);
    }

    function removeIfExits(arry, id){
        for (var i = 0; i < arry.length; i++) {
            if (selectedIds[i] == id) {
                delete selectedIds[i];
                return true;
            }

            return false;
        }
    }

    function isIdAlreadyAdded(arry, id){
        for (var i = 0; i < arry.length; i++) {
            if(selectedIds[i] == id){
                return true;
            } 
            return false;
        }
    }

    function removeFromArray(id){
        for (var i = 0; i < selectedIds.length; i++) {
            if (selectedIds[i] == id) {
                delete selectedIds[i];
                return true;
            }
            return false;
        }
        console.log(selectedIds);
    }
    
	$(document).ready(function(){
        
		$('#officialTable').DataTable({
			"paging": true,
        	"autoWidth": false,
        	searching: true,
        	"order": [[ 1, "desc" ]],
        	"columnDefs": [
            	{ "orderable": false, "targets": [3] },
        	],
        	"bAutoWidth": false ,
        	"processing": true,
        	"serverSide": true,
        	"ajax": "{{route('table.officials')}}",
        	"rowCallback": function(row, data, index){
        		var tempData = data;
            	var data = [];

            	data[0] = tempData['id'];
                data[5] = tempData['user_id']
            	data[1] = tempData['name'];
            	data[2] = tempData['mobile'];
            	data[3] = tempData['email'];
            	data[4] = tempData['office'];
            	var office = data[4] == null ? 'Not Alloted' : data[4];
            	var allot = '';
            	if (data[4] == null) {
            		console.log('not alloted');
            		allot = '<a class="btn btn-info btn-sm" href="'+ '{{url('admin/allot-official')}}' +"/"+data[5]+'">Allot</a>';
            	}
            	console.log(allot);
            	var htmlStr = '<td><input type="checkbox" onchange="officialIdChange(this,\''+data[5]+'\')" class="form-check"></td><td>'+data[1]+'</td><td>'+data[2]+'</td><td>'+data[3]+'</td><td>'+office+'</td>';
            	$(row).html(htmlStr);
        	},
        	columns:[
                { title: "", data: "id" },
        		{ title: "Name", data: "name" },
        		{ title: "Mobile", data: "mobile" },
        		{ title: "Email", data: "email" },
        		{ title: "Alloted To", data: "office" },
        	]
		});
	});
</script>