<script type="text/javascript">
	$(document).ready(function(){
		$('#officialTable').DataTable({
			"paging": true,
        	"autoWidth": false,
        	searching: true,
        	"order": [[ 1, "desc" ]],
        	"columnDefs": [
            	{ "orderable": false, "targets": [4] },
        	],
        	"bAutoWidth": false ,
        	"processing": true,
        	"serverSide": true,
        	"ajax": "{{route('table.officials')}}",
        	"rowCallback": function(row, data, index){
        		var tempData = data;
            	var data = [];

            	data[0] = tempData['id'];
                data[5] = tempData['user_id']
            	data[1] = tempData['name'];
            	data[2] = tempData['mobile'];
            	data[3] = tempData['email'];
            	data[4] = tempData['office'];
            	var office = data[4] == null ? 'Not Alloted' : data[4];
            	var allot = '<a class="btn btn-secondary btn-sm" href="'+ '{{url('admin/allot-official')}}' +"/"+data[5]+'">Revoke</a>';
            	if (data[4] == null) {
            		console.log('not alloted');
            		allot = '<a class="btn btn-info btn-sm" href="'+ '{{url('admin/allot-official')}}' +"/"+data[5]+'">Allot</a>';
            	}
            	console.log(allot);
            	var htmlStr = '<td>'+data[1]+'</td><td>'+data[2]+'</td><td>'+data[3]+'</td><td>'+office+'</td><td><a class="btn btn-primary btn-sm mr-1"><i class="far fa-edit"></i></a><a class="btn btn-danger btn-sm mr-1"><i class="fa fa-trash"></i></a>'+allot+'</td>';
            	$(row).html(htmlStr);
        	},
        	columns:[
        		{ title: "Name", data: "name" },
        		{ title: "Mobile", data: "mobile" },
        		{ title: "Email", data: "email" },
        		{ title: "Alloted To", data: "office" },
        		{ title: "Action", data: "id" },
        	]
		});
	});
</script>