<script type="text/javascript">
	$(document).ready(function(){
		$('#govTable').DataTable({
			"paging": true,
        	"autoWidth": false,
        	searching: true,
        	"order": [[ 1, "desc" ]],
        	"columnDefs": [
            	{ "orderable": false, "targets": [1] },
        	],
        	"bAutoWidth": false ,
        	"processing": true,
        	"serverSide": true,
        	"ajax": "{{route('gov.table')}}",
        	"rowCallback": function(row, data, index){
        		var tempData = data;
            	var data = [];

            	data[0] = tempData['id'];
            	data[1] = tempData['name'];
            	data[2] = tempData['mobile'];
            	data[3] = tempData['role'];
            	

            	var htmlStr = '<td>'+data[1]+'</td><td>'+data[2]+'</td><td>'+data[3]+'</td><button class="btn btn-info btn-sm">Make Governer</button>';
            	$(row).html(htmlStr);
        	},
        	columns:[
        		{ title: "Name", data: "name" },
        		{ title: "Mobile", data: "mobile" },
        		{ title: "Role", data: "role" },
        		{ title: "Action", data: "id" },
        	]
		});
	});
</script>