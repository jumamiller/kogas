@extends('backend.master')
@section('content')
<div class="content-wrapper text-sm mt-3" style="font-family: Nunito;">
  <div class="col-md-12">
    <div class="card card-primary">
      <div class="card-header">
        <h3 class="card-title">Add Project</h3>
      </div>
      <form action="{{route('update.project', $project-> id)}}" method="post" enctype="multipart/form-data">
        @csrf
        <div class="card-body">
          <div class="form-group">
            <label for="exampleInputEmail1">Project Name</label>
            <input type="text" name="project_name" value="{{$project-> project_name}}" class="form-control" placeholder="">
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1">Project Detail</label>
            <textarea type="text" rows="4" name="project_detail" class="form-control" placeholder="">{{$project-> project_detail}}</textarea>
          </div>
          <div class="form-group">
            <label for="exampleInputFile">Thumb Image</label>
            <div class="input-group">
              <input type="file" name="thumb_image" data-default-file="{{asset('/uploads/project_image/thumb_image/'. $project-> thumb_image)}}" class="dropify" data-height="300" id="thumb">
            </div>
          </div>

          <div class="form-group">
            <label for="exampleInputFile">Banner Image</label>
            <div class="input-group">
              <input type="file" name="banner_image" data-default-file="{{asset('/uploads/project_image/banner_image/'. $project-> banner_image)}}" class="dropify" data-height="300" id="banner">
            </div>
          </div>

          <div class="form-group">
            <label for="exampleInputFile">Status</label>
            <div class="input-group">
              <select class="form-control" name="status">
                <option value="Upcomming" {{$project-> status == 'Upcomming' ? 'selected' : ''}}>Upcomming</option>
                <option value="In Progress" {{$project-> status == 'In Progress' ? 'selected' : ''}}>In Progress</option>
                <option value="Completed" {{$project-> status == 'Completed' ? 'selected' : ''}}>Completed</option>
              </select>
            </div>
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1">Client</label>
            <input type="text" name="client" value="{{$project-> client}}" class="form-control" placeholder="">
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1">Contractor</label>
            <input type="text" name="contractor" value="{{$project-> contractor}}" class="form-control" placeholder="">
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1">Facilitator</label>
            <input type="text" name="facilator" value="{{$project-> facilator}}" class="form-control" placeholder="">
          </div>
          <div class="form-group">
            <label for="exampleInputFile">Ministy</label>
            <div class="input-group">
              <select class="form-control" name="ministry">
                @foreach($ministries as $ministry)
                <option value="{{$ministry-> name}}" {{$project-> ministry == $ministry-> name ? 'selected' : ''}}>{{$ministry-> name}}</option>
                @endforeach
              </select>
            </div>
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1">Duration</label>
            <input type="text" name="duration" value="{{$project-> duration}}" class="form-control" placeholder="">
          </div>
        </div>

        <!-- /.card-body -->
        <div class="card-footer">
          <button type="submit" class="btn btn-primary btn-block">Submit</button>
        </div>
      </form>
    </div>
  </div>

</div>
<script type="text/javascript">
	$(document).ready(function(){
		$('#thumb').dropify();
    $('#banner').dropify();
	});
</script>
@stop
