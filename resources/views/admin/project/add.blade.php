@extends('backend.master')
@section('content')
<div class="content-wrapper text-sm mt-3" style="font-family: Nunito;">
  <div class="col-md-12">
    <div class="card card-primary">
      <div class="card-header">
        <h3 class="card-title">Add Project</h3>
      </div>
      <form action="{{route('add.project')}}" method="post" enctype="multipart/form-data">
        @csrf
        <div class="card-body">
          <div class="form-group">
            <label for="exampleInputEmail1">Project Name</label>
            <input type="text" name="project_name" class="form-control" placeholder="">
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1">Project Description</label>
            <textarea type="text" rows="4" name="project_detail" class="form-control" placeholder=""></textarea>
          </div>
          <div class="form-group">
            <label for="exampleInputFile">Thumb Image</label>
            <div class="input-group">
              <input type="file" name="thumb_image" class="dropify" data-height="300" id="thumb">
            </div>
          </div>

          <div class="form-group">
            <label for="exampleInputFile">Banner Image</label>
            <div class="input-group">
              <input type="file" name="banner_image" class="dropify" data-height="300" id="banner">
            </div>
          </div>

          <div class="form-group">
            <label for="exampleInputFile">Status</label>
            <div class="input-group">
              <select class="form-control" name="status">
                <option value="stageone">Upcoming</option>
                <option value="stagetwo">In Progress</option>
                <option value="stagethree">Completed</option>
              </select>
            </div>
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1">Client</label>
            <input type="text" name="client" class="form-control" placeholder="">
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1">Contractor</label>
            <input type="text" name="contractor" class="form-control" placeholder="">
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1">Facilitator</label>
            <input type="text" name="facilator" class="form-control" placeholder="">
          </div>
          <div class="form-group">
            <label for="exampleInputFile">Ministy</label>
            <div class="input-group">
              <select class="form-control" name="ministry">
                @foreach($ministries as $ministry)
                <option value="{{$ministry-> name}}">{{$ministry-> name}}</option>
                @endforeach
              </select>
            </div>
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1">Duration</label>
            <input type="text" name="duration" class="form-control" placeholder="">
          </div>
        </div>

        <!-- /.card-body -->
        <div class="card-footer">
          <button type="submit" class="btn btn-primary btn-block">Submit</button>
        </div>
      </form>
    </div>
  </div>

</div>
<script type="text/javascript">
	$(document).ready(function(){
		$('#thumb').dropify();
    $('#banner').dropify();
	});
</script>
@stop
