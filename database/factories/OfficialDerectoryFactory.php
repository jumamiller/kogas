<?php

namespace Database\Factories;

use App\Models\OfficialDerectory;
use Illuminate\Database\Eloquent\Factories\Factory;

class OfficialDerectoryFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = OfficialDerectory::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
