<?php

namespace Database\Factories;

use App\Models\ExeOfficial;
use Illuminate\Database\Eloquent\Factories\Factory;

class ExeOfficialFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = ExeOfficial::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
