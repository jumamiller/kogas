<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLocalgovermentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('localgoverments', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->integer('commi_id');
            $table->string('localgov_image');
            $table->longText('history');
            $table->string('state');
            $table->string('population');
            $table->string('area');
            $table->longText('message');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('localgoverments');
    }
}
