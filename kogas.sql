-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 07, 2021 at 12:33 PM
-- Server version: 10.4.19-MariaDB
-- PHP Version: 8.0.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `africde1_kogas`
--

-- --------------------------------------------------------

--
-- Table structure for table `appointments`
--

CREATE TABLE `appointments` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `first_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `state` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `ministry_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `department` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `official_id` int(11) NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'received',
  `user_id` int(11) NOT NULL,
  `message` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `appointment_date` varchar(1000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `appointments`
--

INSERT INTO `appointments` (`id`, `first_name`, `last_name`, `phone`, `email`, `state`, `city`, `type`, `ministry_name`, `department`, `official_id`, `status`, `user_id`, `message`, `appointment_date`, `created_at`, `updated_at`) VALUES
(1, 'Felix', 'Wanzetse', '1234567889', 'admin@admin.com', 'Kogi', 'Kogi', '1', '1', '1', 1, '1', 6, 'Place <em>some</em> <u>text</u> <strong>here</strong>', '06/22/2021 6:42 PM', '2021-06-22 12:42:33', '2021-06-22 12:42:33'),
(2, 'Felix', 'Odhiambo', '0712345678', 'james@gmail.com', 'Kogi', 'Kogi', '1', '1', '1', 1, '1', 6, 'Humbly Requesting to See the Minister For Energy', '06/23/2021 8:47 PM', '2021-06-23 14:48:18', '2021-06-23 14:48:18'),
(3, 'Felix', 'Odhiambo', '0712345678', 'james@gmail.com', 'Kogi', 'Kogi', '1', '1', '1', 1, '1', 6, 'Humbly Requesting to See the Minister For Energy', '06/23/2021 8:47 PM', '2021-06-23 14:48:25', '2021-06-23 14:48:25'),
(4, 'Felix', 'Odhiambo', '0712345678', 'james@gmail.com', 'Kogi', 'Kogi', '1', '1', '1', 1, '1', 6, 'Humbly Requesting to See the Minister For Energy', '06/23/2021 8:47 PM', '2021-06-23 14:48:26', '2021-06-23 14:48:26'),
(5, 'Felix', 'Odhiambo', '0712345678', 'james@gmail.com', 'Kogi', 'Kogi', '1', '1', '1', 1, '1', 6, 'Humbly Requesting to See the Minister For Energy', '06/23/2021 8:47 PM', '2021-06-23 14:48:32', '2021-06-23 14:48:32'),
(6, 'Felix', 'Odhiambo', '0712345678', 'james@gmail.com', 'Kogi', 'Kogi', '1', '1', '1', 1, '1', 6, 'Humbly Requesting to See the Minister For Energy', '06/23/2021 8:47 PM', '2021-06-23 14:48:33', '2021-06-23 14:48:33'),
(7, 'Felix', 'Odhiambo', '0712345678', 'james@gmail.com', 'Kogi', 'Kogi', '1', '1', '1', 1, '1', 6, 'Humbly Requesting to See the Minister For Energy', '06/23/2021 8:47 PM', '2021-06-23 14:48:33', '2021-06-23 14:48:33'),
(8, 'Felix', 'Odhiambo', '0712345678', 'james@gmail.com', 'Kogi', 'Kogi', '1', '1', '1', 1, '1', 6, 'Humbly Requesting to See the Minister For Energy', '06/23/2021 8:47 PM', '2021-06-23 14:48:33', '2021-06-23 14:48:33'),
(9, 'Felix', 'Odhiambo', '0712345678', 'james@gmail.com', 'Kogi', 'Kogi', '1', '1', '1', 1, '1', 6, 'Humbly Requesting to See the Minister For Energy', '06/23/2021 8:47 PM', '2021-06-23 14:48:34', '2021-06-23 14:48:34'),
(10, 'Alex', 'Awilo', '071000000', 'felixwanzetse@gmail.com', 'Kogi', 'Kogi', '1', '1', '1', 1, '1', 6, 'I would like to see You', '07/07/2021 10:55 AM', '2021-07-07 04:55:41', '2021-07-07 04:55:41');

-- --------------------------------------------------------

--
-- Table structure for table `complaints`
--

CREATE TABLE `complaints` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `complaint_title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `complaint_body` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `ministry_id` int(11) NOT NULL,
  `ministry_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `assigned_to` int(11) DEFAULT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'received',
  `citizen_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `conversations`
--

CREATE TABLE `conversations` (
  `id` int(10) UNSIGNED NOT NULL,
  `message` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `group_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `exe_officials`
--

CREATE TABLE `exe_officials` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `official_id` int(11) NOT NULL,
  `department_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `department_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE `groups` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `admin_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `name`, `code`, `admin_id`, `created_at`, `updated_at`) VALUES
(1, 'James Ouko', 'aGpBqHd', 6, '2021-07-05 02:52:17', '2021-07-05 02:52:17'),
(3, 'KOGAS', 'mEep3X8', 6, '2021-07-05 02:54:48', '2021-07-07 05:00:24'),
(5, 'KOGAS', 'QI94LeF', 6, '2021-07-07 04:56:28', '2021-07-07 04:56:28');

-- --------------------------------------------------------

--
-- Table structure for table `group_participants`
--

CREATE TABLE `group_participants` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `group_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `group_participants`
--

INSERT INTO `group_participants` (`id`, `group_id`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 1, 6, NULL, NULL),
(3, 3, 6, NULL, NULL),
(8, 5, 6, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `localgoverments`
--

CREATE TABLE `localgoverments` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `commi_id` int(11) NOT NULL,
  `localgov_image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `history` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `state` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `population` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `area` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `group_id` bigint(20) UNSIGNED NOT NULL,
  `message` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `messages`
--

INSERT INTO `messages` (`id`, `user_id`, `group_id`, `message`, `created_at`, `updated_at`) VALUES
(1, 6, 4, 'Hey', '2021-07-05 07:02:54', '2021-07-05 07:02:54'),
(2, 6, 4, 'Hey', '2021-07-05 07:03:25', '2021-07-05 07:03:25'),
(3, 6, 4, 'Hey', '2021-07-05 07:22:16', '2021-07-05 07:22:16'),
(4, 6, 4, 'Hey', '2021-07-05 07:23:11', '2021-07-05 07:23:11'),
(5, 6, 4, 'Hey', '2021-07-05 07:24:20', '2021-07-05 07:24:20'),
(6, 6, 4, 'Hey', '2021-07-05 07:24:28', '2021-07-05 07:24:28'),
(7, 6, 4, 'Hi', '2021-07-05 08:02:01', '2021-07-05 08:02:01'),
(8, 6, 4, 'Hi', '2021-07-05 08:06:29', '2021-07-05 08:06:29'),
(9, 6, 4, 'James', '2021-07-05 08:16:18', '2021-07-05 08:16:18'),
(10, 6, 4, 'Test Messa', '2021-07-05 08:42:44', '2021-07-05 08:42:44'),
(11, 6, 4, 'Test Messa', '2021-07-05 08:43:06', '2021-07-05 08:43:06'),
(12, 6, 4, 'Test Messa', '2021-07-05 08:46:29', '2021-07-05 08:46:29'),
(13, 6, 4, 'Good evening', '2021-07-05 08:47:06', '2021-07-05 08:47:06'),
(14, 6, 4, 'Good evening', '2021-07-05 08:49:30', '2021-07-05 08:49:30'),
(15, 6, 4, 'Good evening', '2021-07-05 08:49:34', '2021-07-05 08:49:34'),
(16, 6, 4, 'Good evening', '2021-07-05 08:49:56', '2021-07-05 08:49:56'),
(17, 6, 4, 'Good evening', '2021-07-05 08:50:48', '2021-07-05 08:50:48'),
(18, 6, 4, 'Message', '2021-07-05 09:18:52', '2021-07-05 09:18:52'),
(19, 6, 4, 'Hey', '2021-07-05 09:57:51', '2021-07-05 09:57:51'),
(20, 6, 3, 'Hey', '2021-07-06 05:34:19', '2021-07-06 05:34:19'),
(21, 6, 3, 'James', '2021-07-06 05:35:35', '2021-07-06 05:35:35'),
(22, 6, 3, 'Hey', '2021-07-06 14:25:56', '2021-07-06 14:25:56'),
(23, 6, 3, 'Hey', '2021-07-06 14:26:03', '2021-07-06 14:26:03'),
(24, 6, 3, 'Good evening', '2021-07-06 14:34:13', '2021-07-06 14:34:13'),
(25, 6, 3, 'Hello This is my Submission.', '2021-07-06 15:21:29', '2021-07-06 15:21:29'),
(26, 6, 3, 'Hello There Mr. Ebuka', '2021-07-07 04:57:04', '2021-07-07 04:57:04');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2021_01_06_103959_create_news_table', 2),
(5, '2021_01_06_104457_create_localgoverments_table', 3),
(6, '2021_01_06_104922_create_variables_table', 4),
(7, '2021_01_06_105138_create_exe_officials_table', 5),
(8, '2021_01_06_105425_create_otps_table', 6),
(9, '2021_01_06_105639_create_ministries_table', 7),
(10, '2021_01_06_105846_create_projects_table', 8),
(11, '2021_01_06_110319_create_product_images_table', 9),
(12, '2021_01_06_110515_create_official_derectories_table', 10),
(13, '2021_01_06_110817_create_appointments_table', 11),
(14, '2021_01_06_111344_create_complaints_table', 12),
(15, '2021_01_06_111730_create_suggestions_table', 13),
(16, '2021_01_06_120137_create_permission_tables', 14),
(17, '2021_07_01_205507_create_conversations_table', 15),
(18, '2021_07_01_205745_create_groups_table', 15),
(19, '2021_07_01_205810_create_group_user_table', 15),
(20, '2021_04_04_134613_create_groups_table', 16),
(21, '2021_04_04_154352_create_group_participants_table', 16),
(22, '2021_04_06_120621_create_messages_table', 16);

-- --------------------------------------------------------

--
-- Table structure for table `ministries`
--

CREATE TABLE `ministries` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ministry_image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ministries`
--

INSERT INTO `ministries` (`id`, `name`, `ministry_image`, `created_at`, `updated_at`) VALUES
(1, 'Ministry 1', 'Ministry-Image1610099641bakery.jpg', '2021-01-08 04:24:01', '2021-01-08 04:24:01');

-- --------------------------------------------------------

--
-- Table structure for table `model_has_permissions`
--

CREATE TABLE `model_has_permissions` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `model_has_permissions`
--

INSERT INTO `model_has_permissions` (`permission_id`, `model_type`, `model_id`) VALUES
(1, 'App\\Models\\User', 4),
(1, 'App\\Models\\User', 5),
(1, 'App\\Models\\User', 8),
(1, 'App\\Models\\User', 9),
(1, 'App\\Models\\User', 10),
(3, 'App\\Models\\User', 9),
(3, 'App\\Models\\User', 10),
(4, 'App\\Models\\User', 10),
(5, 'App\\Models\\User', 4),
(5, 'App\\Models\\User', 5),
(5, 'App\\Models\\User', 10),
(8, 'App\\Models\\User', 5),
(9, 'App\\Models\\User', 4),
(12, 'App\\Models\\User', 5),
(14, 'App\\Models\\User', 5),
(15, 'App\\Models\\User', 4),
(18, 'App\\Models\\User', 4);

-- --------------------------------------------------------

--
-- Table structure for table `model_has_roles`
--

CREATE TABLE `model_has_roles` (
  `role_id` bigint(20) UNSIGNED NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `model_has_roles`
--

INSERT INTO `model_has_roles` (`role_id`, `model_type`, `model_id`) VALUES
(1, 'App\\Models\\User', 1),
(2, 'App\\Models\\User', 1),
(4, 'App\\Models\\User', 3),
(4, 'App\\Models\\User', 4),
(4, 'App\\Models\\User', 5),
(4, 'App\\Models\\User', 8),
(4, 'App\\Models\\User', 9),
(4, 'App\\Models\\User', 10);

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE `news` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `news_title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `news_body` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `news_image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `news`
--

INSERT INTO `news` (`id`, `news_title`, `news_body`, `news_image`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 'First News', 'HHHHHHHHHHHBBBBBBBBBBBBBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDFGFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFHGHHHHHHHHHHHHHHHHHHHHHHHHHHRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR', 'News-Image-1610043192bakery.jpg', NULL, NULL, '2021-01-07 12:43:12', '2021-01-07 12:43:12');

-- --------------------------------------------------------

--
-- Table structure for table `official_derectories`
--

CREATE TABLE `official_derectories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `office` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `official_image` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `alloted_to` int(11) DEFAULT NULL,
  `facebook` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `twitter` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `instagram` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `official_derectories`
--

INSERT INTO `official_derectories` (`id`, `user_id`, `name`, `office`, `email`, `mobile`, `official_image`, `alloted_to`, `facebook`, `twitter`, `instagram`, `created_at`, `updated_at`) VALUES
(1, 5, 'Pharm. Jamiu Abdulkareem', NULL, 'official1@kogas.com', '99999999', 'Profile-Image-1610105920official-1.jpg', NULL, 'facebook.com', 'twitter.com', 'instagram.com', '2021-01-08 06:08:41', '2021-01-08 06:08:41'),
(2, 9, 'Alex Mwaura', NULL, 'alex@gmail.com', '070000000', 'Profile-Image-1624468398WIN_20210616_23_23_13_Pro.jpg', NULL, 'FaceBook', 'Twitter', 'Insta', '2021-06-23 14:13:20', '2021-06-23 14:13:20');

-- --------------------------------------------------------

--
-- Table structure for table `otps`
--

CREATE TABLE `otps` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `otp_doce` int(11) NOT NULL,
  `exp_at` datetime NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
(1, 'view user', 'web', '2021-01-06 10:59:13', '2021-01-06 10:59:13'),
(2, 'delete user', 'web', '2021-01-06 10:59:54', '2021-01-06 10:59:54'),
(3, 'edit user', 'web', '2021-01-06 11:00:19', '2021-01-06 11:00:19'),
(4, 'add user', 'web', '2021-01-06 11:00:43', '2021-01-06 11:00:43'),
(5, 'view news', 'web', '2021-01-06 11:01:53', '2021-01-06 11:01:53'),
(6, 'delete news', 'web', '2021-01-06 11:02:22', '2021-01-06 11:02:22'),
(7, 'edit news', 'web', '2021-01-06 11:02:45', '2021-01-06 11:02:45'),
(8, 'add news', 'web', '2021-01-06 11:03:04', '2021-01-06 11:03:04'),
(9, 'view topic', 'web', '2021-01-06 11:04:25', '2021-01-06 11:04:25'),
(10, 'delete topic', 'web', '2021-01-06 11:04:45', '2021-01-06 11:04:45'),
(11, 'edit topic', 'web', '2021-01-06 11:05:06', '2021-01-06 11:05:06'),
(12, 'add topic', 'web', '2021-01-06 11:05:47', '2021-01-06 11:05:47'),
(13, 'reject complain', 'web', '2021-01-06 11:07:15', '2021-01-06 11:07:15'),
(14, 'approve complain', 'web', '2021-01-06 11:07:34', '2021-01-06 11:07:34'),
(15, 'view complain', 'web', '2021-01-06 11:08:00', '2021-01-06 11:08:00'),
(16, 'reject appointment', 'web', '2021-01-06 11:09:42', '2021-01-06 11:09:42'),
(17, 'approve appointment', 'web', '2021-01-06 11:10:13', '2021-01-06 11:10:13'),
(18, 'view appointment', 'web', '2021-01-06 11:10:32', '2021-01-06 11:10:32');

-- --------------------------------------------------------

--
-- Table structure for table `product_images`
--

CREATE TABLE `product_images` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `project_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `projects`
--

CREATE TABLE `projects` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `project_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `project_detail` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `thumb_image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `banner_image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `client` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contractor` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `facilator` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ministry` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `duration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `projects`
--

INSERT INTO `projects` (`id`, `project_name`, `project_detail`, `thumb_image`, `banner_image`, `status`, `client`, `contractor`, `facilator`, `ministry`, `duration`, `created_at`, `updated_at`) VALUES
(1, 'First Project', 'This is First Project', 'Thumb-Project-Image1610098940dawat-super.jpeg', 'Banner-Project-Image1610098940dairy.jpg', 'Upcomming', 'ABC', 'XYZ', 'PQR', 'Ministry 1', '10 Months', '2021-01-08 04:12:20', '2021-01-08 04:12:20'),
(2, 'Second Project', 'This is Second Project.', 'Thumb-Project-Image1610100699chocklet.jpg', 'Banner-Project-Image1610100699care2.jpg', 'Upcomming', 'ABC', 'XYZ', 'PQR', 'Ministry 1', '12 Months', '2021-01-08 04:41:39', '2021-01-08 04:41:39'),
(4, 'Forth Project', 'THis is forth project', 'Thumb-Project-Image1610101125care.png', 'Banner-Project-Image1610101125chocklet.jpg', 'In Progress', 'ABC', 'XYZ', 'PQR', 'Ministry 1', '10 Months', '2021-01-08 04:48:45', '2021-01-08 04:48:45'),
(5, 'Bridge Construction', 'This project is funded by the government of Kenya.', 'Thumb-Project-Image1623875124WIN_20210616_23_23_17_Pro.jpg', 'Banner-Project-Image1623875124WIN_20210616_23_23_13_Pro.jpg', 'stagetwo', 'Government of Kenya', 'Prime Arc', 'GOK', 'Ministry 1', '5 Yeasrs', '2021-06-16 17:25:24', '2021-06-16 17:25:24'),
(6, 'Road Constfruction In Abuja', 'This is a Road Construction Project', 'Thumb-Project-Image1624468255Capture.PNG', 'Banner-Project-Image1624468255WIN_20210616_23_23_13_Pro.jpg', 'stagethree', 'Felix', 'Prime Arc Limited', 'GOK', 'Ministry 1', '2 Years', '2021-06-23 14:10:55', '2021-06-23 14:10:55');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
(1, 'SuperAdmin', 'web', '2021-01-06 10:54:07', '2021-01-06 10:54:07'),
(2, 'Governer', 'web', '2021-01-06 10:55:22', '2021-01-06 10:55:22'),
(3, 'Citizen', 'web', '2021-01-06 10:56:00', '2021-01-06 10:56:00'),
(4, 'Official', 'web', '2021-01-06 10:56:25', '2021-01-06 10:56:25');

-- --------------------------------------------------------

--
-- Table structure for table `role_has_permissions`
--

CREATE TABLE `role_has_permissions` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_has_permissions`
--

INSERT INTO `role_has_permissions` (`permission_id`, `role_id`) VALUES
(1, 1),
(1, 2),
(2, 1),
(2, 2),
(3, 1),
(3, 2),
(4, 1),
(4, 2),
(5, 1),
(5, 2),
(6, 1),
(6, 2),
(7, 1),
(7, 2),
(8, 1),
(8, 2),
(9, 1),
(9, 2),
(10, 1),
(10, 2),
(11, 1),
(11, 2),
(12, 1),
(12, 2),
(13, 1),
(13, 2),
(14, 1),
(14, 2),
(15, 1),
(15, 2),
(16, 1),
(16, 2),
(17, 1),
(17, 2),
(18, 1),
(18, 2);

-- --------------------------------------------------------

--
-- Table structure for table `suggestions`
--

CREATE TABLE `suggestions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `suggestion_title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'SUGGESTION',
  `suggestion_body` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `ministry_id` int(11) NOT NULL,
  `citizen_id` int(11) NOT NULL,
  `assigned_to` int(11) DEFAULT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'received',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `suggestions`
--

INSERT INTO `suggestions` (`id`, `suggestion_title`, `suggestion_body`, `ministry_id`, `citizen_id`, `assigned_to`, `status`, `created_at`, `updated_at`) VALUES
(1, 'SUGGESTION', 'This is my best suggestion.', 1, 6, 1, '1', '2021-06-25 20:57:22', '2021-06-25 20:57:22'),
(2, 'SUGGESTION', 'I suggest we meet again', 1, 6, 1, '1', '2021-07-07 05:04:01', '2021-07-07 05:04:01');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pincode` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `otp_code` int(11) DEFAULT NULL,
  `is_varified` int(11) NOT NULL DEFAULT 0,
  `profile_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `role` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'citizen',
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `mobile`, `address`, `city`, `pincode`, `otp_code`, `is_varified`, `profile_image`, `role`, `created_by`, `updated_by`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'admin@admin.com', NULL, NULL, NULL, NULL, NULL, 0, 'governor-img', 'Governer', NULL, NULL, NULL, '1234', NULL, '2021-01-06 06:00:59', '2021-01-08 12:15:42'),
(5, 'Pharm. Jamiu Abdulkareem', 'official1@kogas.com', '99999999', 'abc', 'hellow', NULL, NULL, 0, 'Profile-Image1610198532governor-img.jpg', 'Official', NULL, NULL, NULL, '$2y$10$jGKhBU496Q/7nvQQpOH1ZO0H7xLPeQ/ow5UijdaWOLb/dq76W6gQC', NULL, '2021-01-08 06:08:41', '2021-01-09 07:52:12'),
(6, 'James Ouko', 'felixwanzetse@gmail.com', '0712345678', 'NAirobi', 'Nairobi', NULL, NULL, 0, NULL, 'citizen', NULL, NULL, NULL, '$2y$10$8CJQyZ4/KvPncxNtUAVsUuhBUQjyIFG5RBQCzOowBcZq1M8CzkgQi', NULL, '2021-06-16 11:08:46', '2021-06-16 11:08:46'),
(8, 'James Ouko', 'james@gmail.com', NULL, 'Kenya', NULL, '00100', 3973, 0, NULL, 'Official', NULL, NULL, NULL, '$2y$10$lFoSi2nBTWuLulVWAA5sZOMstcYNtXif58LioCqn8L2PZxh2We6nG', NULL, '2021-06-23 11:46:03', '2021-06-23 11:46:03'),
(9, 'Alex Mwaura', 'alex@gmail.com', '070000000', 'Nairobi, Kenya', 'Nairobi', '00100', NULL, 0, 'Profile-Image-1624468398WIN_20210616_23_23_13_Pro.jpg', 'Official', NULL, NULL, NULL, '$2y$10$cIbVAWNoXAOZOpT5IEVeyuQHcz3dq2BLjFfIA0.JhQFqN57MHuNA6', NULL, '2021-06-23 14:13:18', '2021-06-23 14:13:18'),
(10, 'Ebuka', 'ebuka@gmail.com', NULL, 'Kenya', NULL, '00100', 9584, 0, NULL, 'Official', NULL, NULL, NULL, '$2y$10$9gPCEw/ZL5ZmuMtO30Z4K.bngOOxn/OYrlWbZoNSaVHFeGRbh/1hC', NULL, '2021-07-07 05:14:15', '2021-07-07 05:14:15');

-- --------------------------------------------------------

--
-- Table structure for table `variables`
--

CREATE TABLE `variables` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `variable_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `variable_value` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `variables`
--

INSERT INTO `variables` (`id`, `variable_name`, `variable_value`, `created_at`, `updated_at`) VALUES
(3, '2', 'PHA+U2VkIGVnZXN0YXMsIGFudGUgZXQgdnVscHV0YXRlIHZvbHV0cGF0LCBlcm9zIHBlZGUgc2VtcGVyIGVzdCwgdml0YWUgbHVjdHVzIG1ldHVzIGxpYmVybyBldSBhdWd1ZS4gTW9yYmkgcHVydXMgbGliZXJvLCBmYXVjaWJ1cyBhZGlwaXNjaW5nLCBjb21tb2RvIHF1aXMsIGdyYXZpZGEgaWQsIGVzdC4gU2VkIGxlY3R1cy4gUHJhZXNlbnQgZWxlbWVudHVtIGhlbmRyZXJpdCB0b3J0b3IuIFNlZCBzZW1wZXIgbG9yZW0gYXQgZmVsaXMuIFZlc3RpYnVsdW0gdm9sdXRwYXQuPC9wPjxwPlNlZCBlZ2VzdGFzLCBhbnRlIGV0IHZ1bHB1dGF0ZSB2b2x1dHBhdCwgZXJvcyBwZWRlIHNlbXBlciBlc3QsIHZpdGFlIGx1Y3R1cyBtZXR1cyBsaWJlcm8gZXUgYXVndWUuIE1vcmJpIHB1cnVzIGxpYmVybywgZmF1Y2lidXMgYWRpcGlzY2luZywgY29tbW9kbyBxdWlzLCBncmF2aWRhIGlkLCBlc3QuIFNlZCBsZWN0dXMuPC9wPg==', '2021-01-08 06:24:25', '2021-01-08 06:32:09'),
(4, '1', 'PHA+RGVhciBLb2dpdGVzLDwvcD48cD5TZWQgZWdlc3RhcywgYW50ZSBldCB2dWxwdXRhdGUgdm9sdXRwYXQsIGVyb3MgcGVkZSBzZW1wZXIgZXN0LCB2aXRhZSBsdWN0dXMgbWV0dXMgbGliZXJvIGV1IGF1Z3VlLiBNb3JiaSBwdXJ1cyBsaWJlcm8sIGZhdWNpYnVzIGFkaXBpc2NpbmcsIGNvbW1vZG8gcXVpcywgZ3JhdmlkYSBpZCwgZXN0LiBTZWQgbGVjdHVzLiBQcmFlc2VudCBlbGVtZW50dW0gaGVuZHJlcml0IHRvcnRvci4gU2VkIHNlbXBlciBsb3JlbSBhdCBmZWxpcy4gVmVzdGlidWx1bSB2b2x1dHBhdCwgbGFjdXMgYSB1bHRyaWNlcyBzYWdpdHRpcywgbWkgbmVxdWUgZXVpc21vZC48L3A+PHA+U2VkIGVnZXN0YXMsIGFudGUgZXQgdnVscHV0YXRlIHZvbHV0cGF0LCBlcm9zIHBlZGUgc2VtcGVyIGVzdCwgdml0YWUgbHVjdHVzIG1ldHVzIGxpYmVybyBldSBhdWd1ZS4gTW9yYmkgcHVydXMgbGliZXJvLCBmYXVjaWJ1cyBhZGlwaXNjaW5nLCBjb21tb2RvIHF1aXMsIGdyYXZpZGEgaWQsIGVzdC4gU2VkIGxlY3R1cy48L3A+PHA+U2VkIGVnZXN0YXMsIGFudGUgZXQgdnVscHV0YXRlIHZvbHV0cGF0LCBlcm9zIHBlZGUgc2VtcGVyIGVzdCwgdml0YWUgbHVjdHVzIG1ldHVzIGxpYmVybyBldSBhdWd1ZS4gTW9yYmkgcHVydXMgbGliZXJvLCBmYXVjaWJ1cyBhZGlwaXNjaW5nLCBjb21tb2RvIHF1aXMsIGdyYXZpZGEgaWQsIGVzdC4gU2VkIGxlY3R1cy48L3A+PHA+WW91IGFyZSBtb3N0IHdlbGNvbWUuPC9wPjxwPllvdXJzIHNpbmNlcmVseSw8c3Ryb25nPkFsaC4gWWFoYXlhIEJlbGxvRXhlY3V0aXZlIEdvdmVybm9yPC9zdHJvbmc+PC9wPg==', '2021-01-08 22:51:11', '2021-01-08 22:51:11');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `appointments`
--
ALTER TABLE `appointments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `complaints`
--
ALTER TABLE `complaints`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `conversations`
--
ALTER TABLE `conversations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `exe_officials`
--
ALTER TABLE `exe_officials`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `groups_code_unique` (`code`),
  ADD KEY `groups_admin_id_foreign` (`admin_id`);

--
-- Indexes for table `group_participants`
--
ALTER TABLE `group_participants`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `group_participants_group_id_user_id_unique` (`group_id`,`user_id`);

--
-- Indexes for table `localgoverments`
--
ALTER TABLE `localgoverments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ministries`
--
ALTER TABLE `ministries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`model_id`,`model_type`),
  ADD KEY `model_has_permissions_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indexes for table `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD PRIMARY KEY (`role_id`,`model_id`,`model_type`),
  ADD KEY `model_has_roles_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indexes for table `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `official_derectories`
--
ALTER TABLE `official_derectories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `otps`
--
ALTER TABLE `otps`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_images`
--
ALTER TABLE `product_images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `projects`
--
ALTER TABLE `projects`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `role_has_permissions_role_id_foreign` (`role_id`);

--
-- Indexes for table `suggestions`
--
ALTER TABLE `suggestions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `variables`
--
ALTER TABLE `variables`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `appointments`
--
ALTER TABLE `appointments`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `complaints`
--
ALTER TABLE `complaints`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `conversations`
--
ALTER TABLE `conversations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `exe_officials`
--
ALTER TABLE `exe_officials`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `group_participants`
--
ALTER TABLE `group_participants`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `localgoverments`
--
ALTER TABLE `localgoverments`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `ministries`
--
ALTER TABLE `ministries`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `news`
--
ALTER TABLE `news`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `official_derectories`
--
ALTER TABLE `official_derectories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `otps`
--
ALTER TABLE `otps`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `product_images`
--
ALTER TABLE `product_images`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `projects`
--
ALTER TABLE `projects`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `suggestions`
--
ALTER TABLE `suggestions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `variables`
--
ALTER TABLE `variables`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `groups`
--
ALTER TABLE `groups`
  ADD CONSTRAINT `groups_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD CONSTRAINT `model_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD CONSTRAINT `model_has_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD CONSTRAINT `role_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_has_permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
