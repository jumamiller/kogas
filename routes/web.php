<?php

use App\Http\Controllers\AppointmentController;
use App\Http\Controllers\ChatHomeController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\LocalgovermentController;
use App\Http\Controllers\OfficialsController;
use App\Http\Controllers\SuggestionController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\NewsController;
use App\Http\Controllers\Frontend\FrontendController;
use App\Http\Controllers\ProjectController;
use App\Http\Controllers\MinistryController;
use App\Http\Controllers\OfficialDerectoryController;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\Models\User;
use Illuminate\Auth\Events\Login;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [FrontendController::class, 'news'])->name('news');

Route::get('/projects', [FrontendController::class, 'projects'])->name('projects');
Route::get('/projects', [FrontendController::class, 'projects'])->name('projects');
Route::get('/single_Project_view/{id}', [ProjectController::class, 'single_Project_view'])->name('single_Project_view');
Route::get('home', [HomeController::class, 'index'])->name('home');
Route::get('local_government', [LocalgovermentController::class, 'index'])->name('local_government');
Route::get('view_officials', [OfficialsController::class, 'index'])->name('view_officials');
Route::get('ministries', [MinistryController::class, 'ministry_home'])->name('ministries');
Route::get('directories_home', [OfficialDerectoryController::class, 'directories_home'])->name('directories_home');

Route::get('admin/login', function () {
    return view('auth.login');
})->name('login');

// Route::get('role', function(){
// 	$user = User::find(5);
// 	$p = $user-> permissions ->toArray();
// 	dd($p);
// });

Auth::routes();

Route::get('logout', [AdminController::class, 'logout'])->name('logout');
Route::get('user_logout', [UserController::class, 'logout'])->name('user_logout');
//Route::get('/chat_home', [App\Http\Controllers\ChatHomeController::class, 'index'])->name('chat_home');
Route::get('/user-sign-in', [UserController::class, 'login'])->name('user-sign-in');
Route::post('/user_login', [UserController::class, 'user_login'])->name('user_login');
Route::get('/citizen-login', [UserController::class, 'citizen_login'])->name('citizen_login');
Route::get('/register', [UserController::class, 'registerView'])->name('register');
Route::get('/registerType', [UserController::class, 'registerType'])->name('registerType');
Route::post('/save_user', [UserController::class, 'store'])->name('save_user');

//Route::get('/dashboard',[UserController::class,'dashboard'])->name('dashboard');

Route::group(['prefix' => 'user', 'middleware' => 'auth'], function () {

    Route::get('/dashboard', [UserController::class, 'dashboard'])->name('dashboard');
    Route::get('/user_account_settings', [UserController::class, 'user_account_settings'])->name('user_account_settings');
    Route::get('/user_appointment', [UserController::class, 'user_appointment'])->name('user_appointment');
    Route::post('/save_appointment', [AppointmentController::class, 'store'])->name('save_appointment');
    Route::get('idea_suggestion', [SuggestionController::class, 'index'])->name('idea_suggestion');
    Route::post('/save_idea_suggestion', [SuggestionController::class, 'store'])->name('save_idea_suggestion');
    Route::get('/appointment', [FrontendController::class, 'appointment'])->name('appontment');
    Route::get('/book-appointment', [FrontendController::class, 'bookAppointment'])->name('book.appointment');
    //////////////////////////////////////////////////CHATTING ROUTES////////////////////////
    Route::get('/group-create', 'App\Http\Controllers\GroupController@create_form');
    Route::post('/group-create', 'App\Http\Controllers\GroupController@create');
    Route::get('/group-join', 'App\Http\Controllers\GroupController@join_form');
    Route::post('/group-join', 'App\Http\Controllers\GroupController@join');
    Route::get('/group/{id}', 'App\Http\Controllers\GroupController@show_group');
    Route::get('/group/edit/{id}', 'App\Http\Controllers\GroupController@edit');
    Route::post('/group/update/{id}', 'App\Http\Controllers\GroupController@update');
    Route::delete('/group/delete/{id}', 'App\Http\Controllers\GroupController@delete');
    Route::get('/group/members_list/{id}', 'App\Http\Controllers\GroupController@members_list');
    Route::get('/remove_user/{id}/{user_id}', 'App\Http\Controllers\GroupController@remove_user');
    Route::post('/send_message/{id}', 'App\Http\Controllers\MessageController@send_message');
    Route::get('/show_messages/{id}', 'App\Http\Controllers\MessageController@show_messages');
    Route::get('/chat_home', [ChatHomeController::class, 'index'])->name('chat_home');
    Route::get('idea_suggestion_list', [SuggestionController::class, 'idea_suggestion_list'])->name('idea_suggestion_list');
    Route::get('submit_report', [OfficialsController::class, 'submit_report'])->name('submit_report');
    Route::post('submit_report', [OfficialsController::class, 'save_report'])->name('submit_report');
    Route::get('single_ministry_home/{id}', [MinistryController::class, 'single_ministry_home'])->name('single_ministry_home');
    Route::get('/single_ministry_home/{id}/projects', [MinistryController::class, 'projects'])->name('single_ministry_projects');

});

Route::group(['prefix' => 'admin', 'middleware' => 'auth'], function () {
    Route::get('new-user', [UserController::class, 'index'])->name('new.user');
    Route::post('add-user', [UserController::class, 'store'])->name('add.user');
    Route::get('gov-list', [UserController::class, 'govList'])->name('gov.list');
    Route::get('gov-table', [UserController::class, 'govTable'])->name('gov.table');
    Route::get('make-governer/{id}', [UserController::class, 'makeGoverner'])->name('make.governer');
    Route::get('remove-governer/{id}', [UserController::class, 'removeGoverner'])->name('remove.governer');
    Route::post('message-governer', [UserController::class, 'messageGoverner'])->name('message.governer');
    Route::get('user-table', [UserController::class, 'userTable'])->name('table.user');
    Route::get('user-list', [UserController::class, 'listUser'])->name('list.user');
    Route::get('delete-user/{id}', [UserController::class, 'deleteUser'])->name('delete.user');
    Route::get('edit-user/{id}', [UserController::class, 'edit'])->name('edit.user');
    Route::post('update-user/{id}', [UserController::class, 'update'])->name('update.user');
    Route::get('home', [HomeController::class, 'index'])->name('home');

    //////////////////////////////////////////////////CHATTING ROUTES////////////////////////
    Route::get('/group-create', 'App\Http\Controllers\GroupController@create_form');
    Route::post('/group-create', 'App\Http\Controllers\GroupController@create');
    Route::get('/group-join', 'App\Http\Controllers\GroupController@join_form');
    Route::post('/group-join', 'App\Http\Controllers\GroupController@join');
    Route::get('/group/{id}', 'App\Http\Controllers\GroupController@show_group');
    Route::get('/group/edit/{id}', 'App\Http\Controllers\GroupController@edit');
    Route::post('/group/update/{id}', 'App\Http\Controllers\GroupController@update');
    Route::delete('/group/delete/{id}', 'App\Http\Controllers\GroupController@delete');
    Route::get('/group/members_list/{id}', 'App\Http\Controllers\GroupController@members_list');
    Route::get('/remove_user/{id}/{user_id}', 'App\Http\Controllers\GroupController@remove_user');
    Route::post('/send_message/{id}', 'App\Http\Controllers\MessageController@send_message');
    Route::get('/show_messages/{id}', 'App\Http\Controllers\MessageController@show_messages');
    Route::get('/chat_home', [ChatHomeController::class, 'index'])->name('chat_home');

    //News Route
    Route::get('news', [NewsController::class, 'index'])->name('news');
    Route::post('add-news', [NewsController::class, 'store'])->name('add.news');
    Route::get('list-news', [NewsController::class, 'listNews'])->name('list.news');
    Route::get('table-news', [NewsController::class, 'tableNews'])->name('table.news');
    Route::get('edit-news/{id}', [NewsController::class, 'edit'])->name('edit.news');
    Route::post('update-news/{id}', [NewsController::class, 'update'])->name('update.news');
    Route::get('delete-news/{id}', [NewsController::class, 'destroy'])->name('delete.news');

    //Project Route
    Route::get('project', [ProjectController::class, 'index'])->name('project');
    Route::post('add-project', [ProjectController::class, 'store'])->name('add.project');
    Route::get('edit-project/{id}', [ProjectController::class, 'edit'])->name('edit.project');
    Route::get('list-project', [ProjectController::class, 'listProject'])->name('list.project');
    Route::get('list-ministry', [MinistryController::class, 'listMinistry'])->name('list.ministries');
    Route::get('table-project', [ProjectController::class, 'tableProject'])->name('table.project');
    Route::get('table-ministry', [MinistryController::class, 'tableMinistry'])->name('table.ministry');
    Route::post('update-project/{id}', [ProjectController::class, 'update'])->name('update.project');
    Route::get('delete-project/{id}', [ProjectController::class, 'destroy'])->name('delete.project');

    //Ministry Route
    Route::get('ministry', [MinistryController::class, 'index'])->name('ministry');
    Route::post('add-ministy', [MinistryController::class, 'store'])->name('add.ministry');

    //Official Route
    Route::get('official', [OfficialDerectoryController::class, 'index'])->name('official');
    Route::post('add-official', [OfficialDerectoryController::class, 'store'])->name('add.official');
    Route::get('list-officials', [OfficialDerectoryController::class, 'listOfficials'])->name('list.officials');
    Route::get('table-officials', [OfficialDerectoryController::class, 'tableOfficials'])->name('table.officials');
    Route::get('allot-official', [OfficialDerectoryController::class, 'allot']);
});
