<?php

namespace App\Http\Controllers;

use App\Models\Appointment;
use App\Models\Ministry;
use App\Models\OfficialDerectory;
use App\Models\Suggestion;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class SuggestionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function index()
    {
        $ministry=Ministry::all();
        $officials=OfficialDerectory::all();

        return view('frontend.make-suggestion')
            ->with(
                [
                    "ministries"=>$ministry,
                    "officials"=>$officials,

                ]
            );
    }
    public function idea_suggestion_list()
    {
        $my_suggestions = Suggestion::where('citizen_id', '=', Auth::user()->id)
            ->orderBy('created_at','desc')
            ->get();

        return view('frontend.my_ideas_suggestions',compact('my_suggestions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'suggestion_message' => 'required',
            'ministry' => 'required',
            'official' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json( ['message'=>$validator->errors()->first()], 200);
        }
        $suggestion = new Suggestion();

        $suggestion-> suggestion_body = $request-> input('suggestion_message');
        $suggestion-> ministry_id = $request-> input('ministry');
        $suggestion-> assigned_to = $request-> input('official');
        $suggestion-> citizen_id = Auth::user()->id;
        $suggestion-> status = 1;

        $suggestion->save();

        return response()->json( ['message'=>'Suggestion Saved Successfully'], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Suggestion  $suggestion
     * @return \Illuminate\Http\Response
     */
    public function show(Suggestion $suggestion)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Suggestion  $suggestion
     * @return \Illuminate\Http\Response
     */
    public function edit(Suggestion $suggestion)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Suggestion  $suggestion
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Suggestion $suggestion)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Suggestion  $suggestion
     * @return \Illuminate\Http\Response
     */
    public function destroy(Suggestion $suggestion)
    {
        //
    }
}
