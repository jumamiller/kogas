<?php

namespace App\Http\Controllers;

use App\Models\Ministry;
use App\Models\OfficialDerectory;
use App\Models\User;
use Illuminate\Http\Request;
use Hash;
use DataTables;
use Spatie\Permission\Models\Role;

class OfficialDerectoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $ministries = Ministry::pluck('name', 'id');
        $roles = Role::pluck('name', 'id');
        $selectedID = 2;

        return view('admin.official.add',compact( 'ministries','selectedID','roles'));
    }

    public function directories_home()
    {
        $officials= OfficialDerectory::select('user_id','name','office','email','mobile',
            'official_image','alloted_to','facebook','twitter','instagram')->get();

        return view('frontend.directories',compact('officials'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $fileName = '';
        if($request-> hasFile('profile_image'))
        {
            $img = $request-> profile_image;
            $fileName = 'Profile-Image-'.time().$img-> getClientOriginalName();
            $img-> move('uploads/profile_images/', $fileName);
        }

        $official = new User;
        $official-> name = $request-> name;
        $official-> email = $request-> email;
        $official-> profile_image = $fileName;
        $official-> password = Hash::make($request-> password);
        $official-> address = $request-> address;
        $official-> role = 'Official';
        $official-> pincode = $request-> pincode;
        $official-> city = $request-> city;
        $official-> mobile = $request-> mobile;
        $official-> save();

        $user_id = $official-> id;

        $official-> assignRole('Official');
        $official-> givePermissionTo(
            $request-> viewuser,
            $request-> edituser,
            $request-> adduser,
            $request-> deleteuser,

            $request-> viewnews,
            $request-> editnews,
            $request-> addnews,
            $request-> deletenews,

            $request-> viewtopic,
            $request-> edittopic,
            $request-> addtopic,
            $request-> deletetopic,

            $request-> viewcomplain,
            $request-> approvecomplain,
            $request-> rejectcomplain,

            $request-> viewappointment,
            $request-> approveappointment,
            $request-> rejectappointment,
        );

        $directory = new OfficialDerectory;
        $directory-> name = $request-> name;
        $directory-> email = $request-> email;
        $directory-> mobile = $request-> mobile;
        $directory-> official_image = $fileName;
        $directory-> ministry = $request->ministry;
        $directory-> facebook = $request-> facebook;
        $directory-> twitter = $request-> twitter;
        $directory-> instagram = $request-> instagram;
        $directory-> user_id = $user_id;
        $directory-> save();

        return back();
    }

    public function listOfficials()
    {
        return view('admin.official.list');
    }

    public function tableOfficials()
    {
        $officials = OfficialDerectory::select('id', 'name', 'user_id', 'email', 'mobile', 'office')->get();
        return DataTables::of($officials)->make(true);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\OfficialDerectory  $officialDerectory
     * @return \Illuminate\Http\Response
     */
    public function show(OfficialDerectory $officialDerectory)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\OfficialDerectory  $officialDerectory
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        $permissions = $user-> permissions;
        $official = OfficialDerectory::where('user_id', $id)->first();

        return view('admin.official.edit')->with('official', $official)->with('permissions', $permissions);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\OfficialDerectory  $officialDerectory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, OfficialDerectory $officialDerectory)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\OfficialDerectory  $officialDerectory
     * @return \Illuminate\Http\Response
     */
    public function destroy(OfficialDerectory $officialDerectory)
    {
        //
    }
}
