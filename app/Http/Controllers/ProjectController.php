<?php

namespace App\Http\Controllers;

use App\Models\Appointment;
use App\Models\Project;
use App\Models\Ministry;
use DataTables;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $ministries = Ministry::select('id','name')-> get();
        return view('admin.project.add')->with('ministries', $ministries);
    }

    public function listProject()
    {
        return view('admin.project.list');
    }

    public function single_Project_view($id)
    {

        $single_project = Project::where('id', '=', $id)->first();


        return view('frontend.single-project')->with(["project"=>$single_project]);
    }

    public function tableProject()
    {
        $projects = Project::select('id', 'project_name', 'thumb_image', 'status', 'ministry')->get();
        return DataTables::of($projects)->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $banner_name = '';
        $thumb_name = '';
        if($request-> hasFile('thumb_image'))
        {
            $thumb = $request-> thumb_image;
            $thumb_name = 'Thumb-Project-Image'.time().$thumb-> getClientOriginalName();
            $thumb->move('uploads/project_image/thumb_image/', $thumb_name);
        }

        if($request-> hasFile('banner_image'))
        {
            $banner = $request-> banner_image;
            $banner_name = 'Banner-Project-Image'.time().$banner-> getClientOriginalName();
            $banner->move('uploads/project_image/banner_image/', $banner_name);
        }

        $project = new Project;
        $project-> project_name = $request-> project_name;
        $project-> project_detail = $request-> project_detail;
        $project-> status = $request-> status;
        $project-> client = $request-> client;
        $project-> contractor = $request-> contractor;
        $project-> facilator = $request-> facilator;
        $project-> ministry = $request-> ministry;
        $project-> duration = $request-> duration;
        $project-> thumb_image = $thumb_name;
        $project-> banner_image = $banner_name;
        $project-> save();

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function show(Project $project)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $project = Project::find($id);
        $ministries = Ministry::select('id','name')-> get();
        return view('admin.project.edit')->with('project', $project)->with('ministries', $ministries);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $project = Project::find($id);
        $banner_name = '';
        $thumb_name = '';
        if($request-> hasFile('thumb_image'))
        {
            $thumb = $request-> thumb_image;
            $thumb_name = 'Thumb-Project-Image'.time().$thumb-> getClientOriginalName();
            $thumb->move('public/uploads/project_image/thumb_image/', $thumb_name);
            $project-> thumb_image = $thumb_name;
        }

        if($request-> hasFile('banner_image'))
        {
            $banner = $request-> banner_image;
            $banner_name = 'Banner-Project-Image'.time().$banner-> getClientOriginalName();
            $banner->move('public/uploads/project_image/banner_image/', $banner_name);
            $project-> banner_image = $banner_name;
        }


        $project-> project_name = $request-> project_name;
        $project-> project_detail = $request-> project_detail;
        $project-> status = $request-> status;
        $project-> client = $request-> client;
        $project-> contractor = $request-> contractor;
        $project-> facilator = $request-> facilator;
        $project-> ministry = $request-> ministry;
        $project-> duration = $request-> duration;
        $project-> save();

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Project::find($id)->delete();
        return response()->json('done');
    }
}
