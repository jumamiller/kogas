<?php

namespace App\Http\Controllers;

use App\Models\Appointment;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class AppointmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'firstName' => 'required',
            'lastName' => 'required',
            'city' => 'required',
            'email' => 'required|email',
            'state' => 'required',
            'office' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json( ['message'=>$validator->errors()->first()], 200);
        }
        $appointment = new Appointment();
        $appointment->user_id =  Auth::user()-> id;
        $appointment->first_name =  $request->input('firstName');
        $appointment->last_name = $request-> input('lastName');
        $appointment-> phone = $request-> input('phone');
        $appointment-> email = $request-> input('email');
        $appointment-> state = $request-> input('state');
        $appointment-> city = $request-> input('city');
        $appointment-> ministry_name = $request-> input('office');
        $appointment-> status =  1;
        $appointment-> department = $request-> input('office');
        $appointment-> official_id = $request-> input('office');
        $appointment-> appointment_date = $request-> input('appointment_date');
        $appointment-> message = $request-> input('message');

        $appointment->save();

        return response()->json( ['message'=>'Appointment Booked Successfully'], 200);

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Appointment  $appointment
     * @return \Illuminate\Http\Response
     */
    public function show(Appointment $appointment)
    {

        $my_appointments = User::get();

        return view('view', compact($my_appointments));
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Appointment  $appointment
     * @return \Illuminate\Http\Response
     */
    public function edit(Appointment $appointment)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Appointment  $appointment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Appointment $appointment)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Appointment  $appointment
     * @return \Illuminate\Http\Response
     */
    public function destroy(Appointment $appointment)
    {
        //
    }
}
