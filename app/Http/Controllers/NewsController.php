<?php

namespace App\Http\Controllers;

use App\Models\News;
use DataTables;
use Illuminate\Http\Request;

class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.news.add');
    }

    public function listNews()
    {
        return view('admin.news.list');
    }

    public function tableNews()
    {
        $news = News::select('id', 'news_title', 'news_body', 'news_image')->get();
        return DataTables::of($news)->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request-> hasFile('news_image'))
        {
            $fileName = '';
            $img = $request-> news_image;
            $fileName = 'News-Image-'.time().$img->getClientOriginalName();
            $img->move('uploads/news_image', $fileName);
        }

        $news = new News;
        $news-> news_title = $request-> news_title;
        $news-> news_body = $request-> news_body;
        $news-> news_image = $fileName;
        $news-> save();

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\News  $news
     * @return \Illuminate\Http\Response
     */
    public function show(News $news)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\News  $news
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $news = News::find($id);
        return view('admin.news.edit')->with('news', $news);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\News  $news
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $news = News::find($id);

        if($request-> hasFile('news_image'))
        {
            $img = $request-> news_image;
            $fileName = 'News-Image-'.time().$img->getClientOriginalName();
            $img->move('uploads/news_image', $fileName);
            $news-> news_image = $fileName;
        }

        $news-> news_title = $request-> news_title;
        $news-> news_body = $request-> news_body;
        $news-> save();

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\News  $news
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        News::find($id)->delete();
        return response()->json('done');
    }
}
