<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Appointment;
use App\Models\Ministry;
use App\Models\OfficialDerectory;
use App\User;
use Illuminate\Http\Request;
use App\Models\News;
use App\Models\Project;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class FrontendController extends Controller
{
    public function news()
    {
        $news = News::latest()->get();
        $projects = Project::latest()->take(10)->get();

        return view('frontend.news.news')
            ->with('news', $news)
            ->with('projects', $projects);
        dd($projects);
    }

    public function appointment()
    {
        $my_appointments = Appointment::where('id', '=', Auth::user()->id)
            ->select(DB::raw('CONCAT(appointments.first_name, " ", appointments.last_name) AS name'),
                'appointments.email',
                'appointments.state',
                'appointments.city',
                'appointments.type',
                'appointments.ministry_name',
                'appointments.department',
                'appointments.official_id',
                'appointments.user_id',
                'appointments.message',
                'appointments.created_at',
                'appointments.appointment_date')
            ->orderBy('appointment_date', 'desc')
            ->get();

        return view('frontend.appointment', compact('my_appointments'));
    }

    public function bookAppointment()
    {
        $ministry=Ministry::all();
        $officials=OfficialDerectory::all();
        return view('frontend.book-appointment')
            ->with(
                [
                    "ministries"=>$ministry,
                    "officials"=>$officials,

                ]
            );
    }

    public function projects()
    {
        $projects = Project::latest()->paginate(15);
        $project = Project::latest()->first();
//        dd($project->images()->first());

        $in_progress=Project::where("status","=","In Progress")->get();
        $upcoming=Project::where("status","=","Upcomming")->get();
        $completed=Project::where("status","=","Completed")->get();



        return view('frontend.projects')->with([
                'in_progress'=>$in_progress,
                'projects'=>$projects,
                "upcoming"=>$upcoming,
                "completed"=>$completed
            ]
        );
    }
}
