<?php

namespace App\Http\Controllers;

use App\Models\Localgoverment;
use Illuminate\Http\Request;

class LocalgovermentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
       return  view('frontend.local_government');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Localgoverment  $localgoverment
     * @return \Illuminate\Http\Response
     */
    public function show(Localgoverment $localgoverment)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Localgoverment  $localgoverment
     * @return \Illuminate\Http\Response
     */
    public function edit(Localgoverment $localgoverment)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Localgoverment  $localgoverment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Localgoverment $localgoverment)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Localgoverment  $localgoverment
     * @return \Illuminate\Http\Response
     */
    public function destroy(Localgoverment $localgoverment)
    {
        //
    }
}
