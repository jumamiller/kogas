<?php

namespace App\Http\Controllers;
use App\Models\UserRide;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Variable;
use GuzzleHttp\Client;
use DataTables;
use Log;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use PHPMailer\PHPMailer\PHPMailer;
use app\Models\Helper;
use Exception;
use Yoeunes\Toastr\Facades\Toastr;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.user.add');
    }


    public function user_login(Request $request){

        //Error messages
        $messages = [
            "email.required" => "Email is required",
            "email.email" => "Email is not valid",
            "email.exists" => "Email doesn't exists",
            "password.required" => "Password is required",
            "password.min" => "Password must be at least 6 characters"
        ];
        // validate the form data
        $validator = Validator::make($request->all(), [
            'email' => 'required|email|exists:users,email',
            'password' => 'required'
        ], $messages);
        $request->validate([
            'email' => 'required',
            'password' => 'required',
        ]);
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }
        $credentials = $request->only('email', 'password');
        if (Auth::attempt($credentials)) {
            toastr()->success('Welcome Back to Kogas Portal','Success');
            return redirect()->intended('user/dashboard');
        }else{
            toastr()->error('Wrong Login Credentials or this account not approved yet.','LOGIN FAILED');
            return redirect()->back();
        }
    }

    public function citizen_login(Request $loginRequest){

        return view('auth.citizen-login');

    }

    public function dashboard(){

      return view('frontend.user_home');
    }

    public function user_account_settings(){

        return view('frontend.user-account-settings');
    }

    public function user_appointment(){
        $user=Auth::user();
        $my_appointments=$user->appointments()
            ->orderBy('appointment_date','desc')
            ->get();
        return view('frontend.appointment',compact('my_appointments'));
    }

    public function login(){

        return view('auth.user-type');

    }


    public function logout(){

        return redirect()-> route('user-sign-in');
    }

    public function registerView()
    {

        return view('auth.sign-up');

    }

    public function registerType()
    {

        return view('auth.sign-up-type');

    }



    public function govList()
    {
        $governer = \DB::table('users')->where('role', 'Governer')->get();
        $gov_msg = '';
        if(count($governer)>0){
            $gov_msg = Variable::where('variable_name', $governer[0]-> id)->first();
            // dd($gov_msg);
        }
        return view('admin.user.govlist')->with('governer', $governer)->with('gov_msg', $gov_msg);
    }

    public function govTable()
    {
        $list = \DB::table('users')->where('role', 'SuperAdmin')->select('id', 'name', 'mobile', 'role')->get();
        return DataTables::of($list)->make(true);

    }

    public function makeGoverner($id)
    {
        $governer = User::where('id', $id);
        $governer->update(['role'=>'Governer']);
        $governer->first()->assignRole('Governer');
        return back();
    }

    public function removeGoverner($id)
    {
        $governer = User::where('id', $id);
        $governer->update(['role'=>'SuperAdmin']);
        $governer->first()->removeRole('Governer');
        $governer->first()->assignRole('SuperAdmin');
        Variable::where('variable_name', $id)->delete();
        return back();
    }

    public function messageGoverner(Request $request)
    {
        $gov_msg = $request-> gov_msg;
        $gov_id = $request-> gov_id;
        $gov_msg = preg_replace('<script>', '',$gov_msg);
        $gov_msg = base64_encode($gov_msg);
        $is_governer = Variable::where('variable_name',$gov_id)->get();
        if(count($is_governer) > 0)
        {
            Variable::where('variable_name', $gov_id)->update(['variable_value'=> $gov_msg]);
            return back();
        } else {
            $variable = new Variable;
            $variable-> variable_name = $gov_id;
            $variable-> variable_value = $gov_msg;
            $variable-> save();
            return back();
        }

    }

    public function userTable()
    {
        $users = User::select('id', 'name', 'email', 'mobile', 'role')->get();
        return DataTables::of($users)->make(true);
    }

    public function listUser()
    {
        return view('admin.user.list');
    }

    public function deleteUser($id)
    {
        User::find($id)->delete();
        return response()->json('done');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function sms($mobile, $otp_code)
    {
        $url = "http://www.estoresms.com/smsapi.php";
        $client = new Client();
        $recipient = $mobile;
        try {

            $response = $client->request('GET', $url,
                ['query' => ['username' => 'DeGrey',
                    'password' => '3805963m9523',
                    'sender' => "Kogas",
                    'recipient' => implode( ',' , $recipient),
                    'message' => "Your OTP code is " + $otp_code + 'Please verify',
                    'dnd' => false,
                ]]);
            return 'Success';
        }  catch (\Exceptions $e) {
            //$response = $this->statusCodeHandling($e);
            return 'Failed';
        }

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        try{
            $user = new User;
            $user->name =  $request->input('name');
            $user->email = $request-> input('email');
            $user-> password = bcrypt($request-> input('password'));
            $user-> mobile = $request-> input('phone');
            $user-> address = $request->input("");
            $user-> city = $request-> input('town');
            $user-> pincode = $request->input("");
            $user-> otp_code = mt_rand(1000, 9999);
            $user-> role = "user";
            $user->save();
            toastr()->success("You have been successfully registered on Kogas",'Error');
            return redirect()->to('citizen-login');
        }catch (Exception $exception){
            toastr()->error($exception->getMessage(),'Error');
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        return view('admin.user.edit')->with('user', $user);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::find($id);
        if($request-> hasFile('profileimage'))
        {
            $img = $request-> profileimage;
            $fileName = 'Profile-Image'.time().$img-> getClientOriginalName();
            $img-> move('public/uploads/profile_images/',$fileName);
            $user-> profile_image = $fileName;
        }

        $user-> name = $request-> name;
        $user-> address = $request-> address;
        $user-> city = $request-> city;
        $user-> pincode = $request-> pincode;
        $user-> mobile = $request-> mobile;
        $user-> email = $request-> email;
        $user-> save();

        return back();

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
