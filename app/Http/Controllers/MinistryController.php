<?php

namespace App\Http\Controllers;

use App\Models\Ministry;
use App\Models\Project;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class MinistryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function ministry_home()
    {
        $ministries = Ministry::select('id','name','ministry_image')-> get();
        return view('frontend.ministries')->with('ministries', $ministries);
    }

    public function single_ministry_home($id)
    {
        $single_ministry = Ministry::select('id','name','ministry_image')
            ->where('id','=',$id)
            -> first();
        $projects=Project::where("ministry","=",$single_ministry->name)
            ->where("status","=","Upcomming")
            ->get();


        return view('frontend.single-ministry')->with(["ministry"=>$single_ministry,"projects"=>$projects]);
    }

    public function index()
    {
        return view('admin.ministry.add');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $fileName = '';
        if($request-> hasFile('ministry_image'))
        {
            $img = $request-> ministry_image;
            $fileName = 'Ministry-Image'.time().$img-> getClientOriginalName();
            $img-> move('uploads/ministry_image/', $fileName);
        }

        $mini = new Ministry;
        $mini-> name = $request-> name;
        $mini-> ministry_image = $fileName;
        $mini-> save();

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Ministry  $ministry
     * @return \Illuminate\Http\Response
     */
    public function show(Ministry $ministry)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Ministry  $ministry
     * @return \Illuminate\Http\Response
     */
    public function edit(Ministry $ministry)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Ministry  $ministry
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Ministry $ministry)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Ministry  $ministry
     * @return \Illuminate\Http\Response
     */
    public function destroy(Ministry $ministry)
    {
        //
    }
    public function projects($id){
        $ministry=Ministry::where('id',"=",$id)->first();
        $projects=Project::where("ministry","=","$ministry->name")->get();

        $in_progress=Project::where("status","=","In Progress")
            ->where("ministry","=","$ministry->name")->get();
        $upcoming=Project::where("status","=","Upcomming")->where("ministry","=","$ministry->name")->get();
        $completed=Project::where("status","=","Completed")->where("ministry","=","$ministry->name")->get();



        return view('frontend.ministry_projects')->with([
                'in_progress'=>$in_progress,
                'projects'=>$projects,
                "upcoming"=>$upcoming,
                "completed"=>$completed,
                "ministry"=>$ministry
            ]
        );
    }

    public function tableMinistry()
    {
        $ministries = Ministry::select('id', 'name', 'ministry_image')->get();
        return DataTables::of($ministries)->make(true);
    }

    public function listMinistry(){
        return view("admin.ministry.list");
    }
}
