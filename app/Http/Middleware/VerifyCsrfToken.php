<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as Middleware;

class VerifyCsrfToken extends Middleware
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        '/user-sign-in',
        '/user_login',
        'user/save_appointment',
        'user/save_idea_suggestion',
        '/save_user',
        '/user/submit_report'
        //
    ];
}
