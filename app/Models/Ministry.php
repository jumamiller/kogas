<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ministry extends Model
{
    use HasFactory;

   public function official_derectories(){
       return $this->hasMany(OfficialDerectory::class,"ministry");
   }

}
