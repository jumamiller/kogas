<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Appointment extends Model
{
    use HasFactory;
    public function user(){
        return $this->belongsTo(User::class,"user_id");
    }
    public function official(){
        return $this->belongsTo(OfficialDerectory::class,"official_id");
    }
    public function ministry(){
        return $this->belongsTo(Ministry::class,"ministry_name");
    }
}
